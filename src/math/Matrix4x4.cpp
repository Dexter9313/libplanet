/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../../include/math/Matrix4x4.hpp"

#include "exceptions/Exceptions.hpp"

Matrix4x4::Matrix4x4()
{
	data[0][0] = 1.0;
	data[0][1] = 0.0;
	data[0][2] = 0.0;
	data[0][3] = 0.0;

	data[1][0] = 0.0;
	data[1][1] = 1.0;
	data[1][2] = 0.0;
	data[1][3] = 0.0;

	data[2][0] = 0.0;
	data[2][1] = 0.0;
	data[2][2] = 1.0;
	data[2][3] = 0.0;

	data[3][0] = 0.0;
	data[3][1] = 0.0;
	data[3][2] = 0.0;
	data[3][3] = 1.0;
}

Matrix4x4::Matrix4x4(double fromScalar)
    : Matrix4x4()
{
	data[0][0] = fromScalar;
	data[0][1] = 0.0;
	data[0][2] = 0.0;
	data[0][3] = 0.0;

	data[1][0] = 0.0;
	data[1][1] = fromScalar;
	data[1][2] = 0.0;
	data[1][3] = 0.0;

	data[2][0] = 0.0;
	data[2][1] = 0.0;
	data[2][2] = fromScalar;
	data[2][3] = 0.0;

	data[3][0] = 0.0;
	data[3][1] = 0.0;
	data[3][2] = 0.0;
	data[3][3] = 1.0;
}

Matrix4x4::Matrix4x4(Vector4 const& fromTranslation)
    : Matrix4x4()
{
	data[3][0] = fromTranslation[0];
	data[3][0] = fromTranslation[1];
	data[3][0] = fromTranslation[2];
}

Matrix4x4::Matrix4x4(double fromRotationAngle, Vector4 const& aroundAxis)
    : Matrix4x4()
{
	Vector4 unitAxis = aroundAxis.getUnitForm();
	const double cosAngle(cos(fromRotationAngle)),
	    sinAngle(sin(fromRotationAngle));

	const double x(unitAxis[0]), y(unitAxis[1]), z(unitAxis[2]);

	data[0][0] = cosAngle + x * x * (1.0 - cosAngle);
	data[0][1] = x * y * (1.0 - cosAngle) - z * sinAngle;
	data[0][2] = x * z * (1.0 - cosAngle) + y * sinAngle;

	data[1][0] = x * y * (1.0 - cosAngle) + z * sinAngle;
	data[1][1] = cosAngle + y * y * (1.0 - cosAngle);
	data[1][2] = y * z * (1.0 - cosAngle) - x * sinAngle;

	data[2][0] = x * z * (1.0 - cosAngle) - y * sinAngle;
	data[2][1] = y * z * (1.0 - cosAngle) + x * sinAngle;
	data[2][2] = cosAngle + z * z * (1.0 - cosAngle);
}

Matrix4x4::Matrix4x4(std::array<std::array<double, 4>, 3> data)
{
	this->data[0] = data[0];
	this->data[1] = data[1];
	this->data[2] = data[2];

	this->data[3][0] = 0.0;
	this->data[3][1] = 0.0;
	this->data[3][2] = 0.0;
	this->data[3][3] = 1.0;
}

Matrix4x4::Matrix4x4(std::array<std::array<double, 4>, 4> data)
    : data(data)
{
	if(data[3][0] != 0.0 || data[3][1] != 0.0 || data[3][2] != 0.0
	   || data[3][3] != 1.0)
	{
		CRITICAL("Invalid matrix : last line isn't [0; 0; 0; 1].");
	}
}

Matrix4x4& Matrix4x4::operator*=(Matrix4x4 const& matrixToMultiply)
{
	const std::array<std::array<double, 4>, 4> oldData(data);

	for(unsigned int i(0); i < 4; ++i)
	{
		for(unsigned int j(0); j < 4; ++j)
		{
			data.at(i).at(j) = oldData.at(i)[0] * matrixToMultiply[0].at(j)
			                   + oldData.at(i)[1] * matrixToMultiply[1].at(j)
			                   + oldData.at(i)[2] * matrixToMultiply[2].at(j)
			                   + oldData.at(i)[3] * matrixToMultiply[3].at(j);
		}
	}

	return *this;
}

Matrix4x4& Matrix4x4::operator*=(double scalar)
{
	data[0][0] *= scalar;
	data[0][1] *= scalar;
	data[0][2] *= scalar;

	data[1][0] *= scalar;
	data[1][1] *= scalar;
	data[1][2] *= scalar;

	data[2][0] *= scalar;
	data[2][1] *= scalar;
	data[2][2] *= scalar;

	return *this;
}

Matrix4x4& Matrix4x4::operator/=(double scalar)
{
	data[0][0] /= scalar;
	data[0][1] /= scalar;
	data[0][2] /= scalar;

	data[1][0] /= scalar;
	data[1][1] /= scalar;
	data[1][2] /= scalar;

	data[2][0] /= scalar;
	data[2][1] /= scalar;
	data[2][2] /= scalar;

	return *this;
}

std::array<double, 4>& Matrix4x4::operator[](unsigned int index)
{
	if(index > 3)
	{
		CRITICAL("Index out of bounds.");
	}
	return data.at(index);
}

std::array<double, 4> Matrix4x4::operator[](unsigned int index) const
{
	if(index > 3)
	{
		CRITICAL("Index out of bounds.");
	}
	return data.at(index);
}

std::array<double, 4> Matrix4x4::getColumn(unsigned int index) const
{
	if(index > 3)
	{
		CRITICAL("Index out of bounds.");
	}
	return {{data[0].at(index), data[1].at(index), data[2].at(index),
	         data[3].at(index)}};
}

Matrix4x4 Matrix4x4::inverted() const
{
	// Implementation from easiliy parsable/replaceable :
	// https://stackoverflow.com/a/60374938
	// There is a better implementation in another answer.
	const double A2323 = data[2][2] * data[3][3] - data[2][3] * data[3][2];
	const double A1323 = data[2][1] * data[3][3] - data[2][3] * data[3][1];
	const double A1223 = data[2][1] * data[3][2] - data[2][2] * data[3][1];
	const double A0323 = data[2][0] * data[3][3] - data[2][3] * data[3][0];
	const double A0223 = data[2][0] * data[3][2] - data[2][2] * data[3][0];
	const double A0123 = data[2][0] * data[3][1] - data[2][1] * data[3][0];
	const double A2313 = data[1][2] * data[3][3] - data[1][3] * data[3][2];
	const double A1313 = data[1][1] * data[3][3] - data[1][3] * data[3][1];
	const double A1213 = data[1][1] * data[3][2] - data[1][2] * data[3][1];
	const double A2312 = data[1][2] * data[2][3] - data[1][3] * data[2][2];
	const double A1312 = data[1][1] * data[2][3] - data[1][3] * data[2][1];
	const double A1212 = data[1][1] * data[2][2] - data[1][2] * data[2][1];
	const double A0313 = data[1][0] * data[3][3] - data[1][3] * data[3][0];
	const double A0213 = data[1][0] * data[3][2] - data[1][2] * data[3][0];
	const double A0312 = data[1][0] * data[2][3] - data[1][3] * data[2][0];
	const double A0212 = data[1][0] * data[2][2] - data[1][2] * data[2][0];
	const double A0113 = data[1][0] * data[3][1] - data[1][1] * data[3][0];
	const double A0112 = data[1][0] * data[2][1] - data[1][1] * data[2][0];

	double det
	    = data[0][0]
	          * (data[1][1] * A2323 - data[1][2] * A1323 + data[1][3] * A1223)
	      - data[0][1]
	            * (data[1][0] * A2323 - data[1][2] * A0323 + data[1][3] * A0223)
	      + data[0][2]
	            * (data[1][0] * A1323 - data[1][1] * A0323 + data[1][3] * A0123)
	      - data[0][3]
	            * (data[1][0] * A1223 - data[1][1] * A0223
	               + data[1][2] * A0123);
	det = 1 / det;

	Matrix4x4 result;

	result[0][0]
	    = det * (data[1][1] * A2323 - data[1][2] * A1323 + data[1][3] * A1223);
	result[0][1]
	    = det * -(data[0][1] * A2323 - data[0][2] * A1323 + data[0][3] * A1223);
	result[0][2]
	    = det * (data[0][1] * A2313 - data[0][2] * A1313 + data[0][3] * A1213);
	result[0][3]
	    = det * -(data[0][1] * A2312 - data[0][2] * A1312 + data[0][3] * A1212);
	result[1][0]
	    = det * -(data[1][0] * A2323 - data[1][2] * A0323 + data[1][3] * A0223);
	result[1][1]
	    = det * (data[0][0] * A2323 - data[0][2] * A0323 + data[0][3] * A0223);
	result[1][2]
	    = det * -(data[0][0] * A2313 - data[0][2] * A0313 + data[0][3] * A0213);
	result[1][3]
	    = det * (data[0][0] * A2312 - data[0][2] * A0312 + data[0][3] * A0212);
	result[2][0]
	    = det * (data[1][0] * A1323 - data[1][1] * A0323 + data[1][3] * A0123);
	result[2][1]
	    = det * -(data[0][0] * A1323 - data[0][1] * A0323 + data[0][3] * A0123);
	result[2][2]
	    = det * (data[0][0] * A1313 - data[0][1] * A0313 + data[0][3] * A0113);
	result[2][3]
	    = det * -(data[0][0] * A1312 - data[0][1] * A0312 + data[0][3] * A0112);
	result[3][0]
	    = det * -(data[1][0] * A1223 - data[1][1] * A0223 + data[1][2] * A0123);
	result[3][1]
	    = det * (data[0][0] * A1223 - data[0][1] * A0223 + data[0][2] * A0123);
	result[3][2]
	    = det * -(data[0][0] * A1213 - data[0][1] * A0213 + data[0][2] * A0113);
	result[3][3]
	    = det * (data[0][0] * A1212 - data[0][1] * A0212 + data[0][2] * A0112);

	return result;
}

std::ostream& Matrix4x4::printInStream(std::ostream& stream) const
{
	stream << "[ " << data[0][0] << ", " << data[0][1] << ", " << data[0][2]
	       << ", " << data[0][3] << std::endl;
	stream << data[1][0] << ", " << data[1][1] << ", " << data[1][2] << ", "
	       << data[1][3] << std::endl;
	stream << data[2][0] << ", " << data[2][1] << ", " << data[2][2] << ", "
	       << data[2][3] << std::endl;
	stream << data[3][0] << ", " << data[3][1] << ", " << data[3][2] << ", "
	       << data[3][3] << "]";
	return stream;
}

Matrix4x4 operator*(Matrix4x4 const& a, Matrix4x4 const& b)
{
	Matrix4x4 c(a);
	c *= b;
	return c;
}

Vector4 operator*(Matrix4x4 const& matrix, Vector4 const& vector)
{
	Vector4 result(vector);

	// w is invariant
	for(unsigned int i(0); i < 3; ++i)
	{
		result[i] = matrix[i][0] * vector[0] + matrix[i][1] * vector[1]
		            + matrix[i][2] * vector[2] + matrix[i][3] * vector[3];
	}

	return result;
}

Vector3 operator*(Matrix4x4 const& matrix, Vector3 const& vector)
{
	Vector4 result(vector, 1.0);

	result = matrix * result;

	return {result[0], result[1], result[2]};
}

Matrix4x4 operator*(double scalar, Matrix4x4 const& matrix)
{
	return Matrix4x4(scalar) * matrix;
}

Matrix4x4 operator*(Matrix4x4 const& matrix, double scalar)
{
	Matrix4x4 copy(matrix);
	copy *= scalar;
	return copy;
}

Matrix4x4 operator/(Matrix4x4 const& matrix, double scalar)
{
	Matrix4x4 copy(matrix);
	copy /= scalar;
	return copy;
}

bool operator==(Matrix4x4 const& a, Matrix4x4 const& b)
{
	for(unsigned int i(0); i < 4; ++i)
	{
		for(unsigned int j(0); j < 4; ++j)
		{
			if(a[i].at(j) != b[i].at(j))
			{
				return false;
			}
		}
	}

	return true;
}

bool operator!=(Matrix4x4 const& a, Matrix4x4 const& b)
{
	return !(a == b);
}

std::ostream& operator<<(std::ostream& stream, Matrix4x4 const& matrix)
{
	return matrix.printInStream(stream);
}
