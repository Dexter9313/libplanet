/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/SpacecraftRenderer.hpp"
#include "graphics/renderers/planet/PlanetRenderer.hpp"

#include "scene/AssimpNode.hpp"
#include "scene/GLTFNode.hpp"

bool SpacecraftRenderer::globalIllumination = true;

double computeTotalNeighborsOcclusion(
    QVector3D const& pos, QVector4D const& lightposradius,
    std::array<QVector4D, 5> const& neighborsPosRadius,
    std::array<QVector3D, 5> const& neighborsOblateness);

SpacecraftRenderer::SpacecraftRenderer(Spacecraft const& drawnSpacecraft,
                                       std::map<QString, Node*>& nodesDict)
    : CelestialBodyRenderer(
          drawnSpacecraft,
          GLHandler::sRGBToLinear(
              Utils::toQt(drawnSpacecraft.getCelestialBodyParameters().color)),
          nodesDict)
{
}

void SpacecraftRenderer::setPointShaderParams(
    UniversalTime const& uT, OrbitalSystemCamera const& /*camera*/,
    GLShaderProgram const& pointShader)
{
	updateNeighborsData(uT);

	pointShader.setUniform("lightposradius", lightsPosRadius[0]);
	pointShader.setUniform("neighData.posRadius", 5, neighborsPosRadius.data());
	pointShader.setUniform("neighData.oblateness", 5,
	                       neighborsOblateness.data());
}

void SpacecraftRenderer::updateMesh(UniversalTime const& uT,
                                    OrbitalSystemCamera const& camera)
{
	if(drawnBody.getOrbit() != nullptr && !drawnBody.getOrbit()->isInRange(uT))
	{
		culled = true;
		destructModel3D();
		return;
	}
	culled = false;
	CelestialBodyRenderer::updateMesh(uT, camera);

	const double camDist((camRelPos - camera.getHeadShift()).length());
	if(drawnBody.getCelestialBodyParameters().radius / camDist < 0.0007)
	{
		// 3D model won't be drawn anyway
		destructModel3D();
		return;
	}

	auto const& drawnSpacecraft = dynamic_cast<Spacecraft const&>(drawnBody);
	if(getChild(0) != nullptr
	   && globalIllumination != getChild(0)->usesGlobalIllumination())
	{
		// only switch gltf
		const auto path = drawnSpacecraft.getSpacecraftParameters().modelPath;
		if((path.endsWith(".gltf") || path.endsWith(".glb"))
		   && !(drawnBody.getName() == "ISS" && path.endsWith("/scene.gltf")))
		{
			destructModel3D();
		}
	}

	constructModel3D();

	updateNeighborsData(uT);

	// apply model scale
	model.scale(drawnSpacecraft.getSpacecraftParameters().modelScale
	            / drawnBody.getCelestialBodyParameters().radius);

	Node* model3D = getChild(0);
	for(unsigned int i(0); i < 2; ++i)
	{
		lights.at(i)->setBoundingSphereRadius(
		    scale * drawnSpacecraft.getSpacecraftParameters().modelScale
		    * model3D->getBoundingSphere().radius);
		lights.at(i)->setCenter(position);
		lights.at(i)->color = lightsIllum.at(i);
		lights.at(i)->setDirection(
		    -lightsPosRadius.at(i).toVector3D().normalized());
	}
	model3D->setModel(model * properRotation);
	model3D->computeVisibility(camera);
	std::vector<GLMesh const*> shadowCastMeshes;
	std::vector<QMatrix4x4> shadowCastModels;
	for(auto const& pair : model3D->getShadowCastingMeshes())
	{
		shadowCastMeshes.push_back(&pair.first);
		shadowCastModels.push_back(pair.second);
	}
	for(auto const& light : lights)
	{
		light->generateShadowMap(shadowCastMeshes, shadowCastModels);
	}

	// update solar panels orientation
	if(drawnBody.getName() == "ISS")
	{
		QVector3D sunDirISSspace
		    = Utils::toQt(Orbitable::getRelativePositionAtUt(
		                      drawnBody, *(drawnBody.getSystem()["Sun"]), uT)
		                      .getUnitForm());

		sunDirISSspace
		    = utils::transformDirection((model * properRotation).inverted(),
		                                sunDirISSspace)
		          .normalized();

		float mainAngle
		    = (180.f / M_PI) * atan2(sunDirISSspace.z(), sunDirISSspace.y());
		mainAngle += -60.f;
		QMatrix4x4 rotateMain;
		rotateMain.rotate(mainAngle, QVector3D(1.f, 0.f, 0.f));
		model3D->setTransform("20 P4 Truss", rotateMain); // port
		model3D->setTransform("23 S4 Truss", rotateMain); // starboard

		const float projectedZY(
		    QVector2D(sunDirISSspace.z(), sunDirISSspace.y())
		        .normalized()
		        .length());
		const float secondaryAngle
		    = (180.f / M_PI) * atan2(-sunDirISSspace.x(), projectedZY);
		QMatrix4x4 rotateSecondary;
		rotateSecondary.rotate(secondaryAngle, QVector3D(0.f, 0.f, 1.f));
		// port
		model3D->setTransform("20 P4 Truss_01", rotateSecondary);
		model3D->setTransform("20 P4 Truss_02", rotateSecondary);
		model3D->setTransform("08 P6 Truss_01", rotateSecondary);
		model3D->setTransform("08 P6 Truss_02", rotateSecondary);
		// starboard
		model3D->setTransform("32 S6 Truss_01", rotateSecondary);
		model3D->setTransform("32 S6 Truss_02", rotateSecondary);
		model3D->setTransform("23 S4 Truss_01", rotateSecondary);
		model3D->setTransform("23 S4 Truss_02", rotateSecondary);

		// radiators
		model3D->setTransform("17 P1 Truss_02", rotateSecondary);
		model3D->setTransform("16 S1 Truss_02", rotateSecondary);
	}
}

void SpacecraftRenderer::doRender(BasicCamera const& camera,
                                  std::vector<Light const*> const& lights,
                                  GLTexture const& brdfLUT, bool environment)
{
	if(culled)
	{
		return;
	}
	transparencyRendered = false;
	Node* model3D        = getChild(0);
	if(model3D == nullptr)
	{
		CelestialBodyRenderer::doRender(camera, lights, brdfLUT, environment);
	}
	else
	{
		std::vector<Light const*> lights_;
		for(auto const& light : this->lights)
		{
			lights_.emplace_back(light.get());
			if(model3D->usesGlobalIllumination())
			{
				break;
			}
		}
		model3D->render(camera, lights_, brdfLUT, environment);
	}
	if(depthWillBeCleared())
	{
		CelestialBodyRenderer::doRenderTransparent(camera, lights, brdfLUT,
		                                           environment);
	}
	handleDepth();
}

void SpacecraftRenderer::doRenderTransparent(
    BasicCamera const& camera, std::vector<Light const*> const& lights,
    GLTexture const& brdfLUT, bool environment)
{
	if(culled)
	{
		return;
	}
	CelestialBodyRenderer::doRenderTransparent(camera, lights, brdfLUT,
	                                           environment);
}

void SpacecraftRenderer::updateNeighborsData(UniversalTime const& uT)
{
	auto sortedOccluders(drawnBody.getSortedRelevantNeighboringOccluders(uT));
	for(unsigned int i(0); i < neighborsPosRadius.size(); ++i)
	{
		if(i >= sortedOccluders.size())
		{
			neighborsPosRadius.at(i)  = QVector4D(0.f, 0.f, 0.f, 0.f);
			neighborsOblateness.at(i) = QVector3D(1.f, 1.f, 1.f);
			continue;
		}
		auto const& pair(sortedOccluders.at(i));
		neighborsPosRadius.at(i)
		    = QVector4D(Utils::toQt(pair.second),
		                pair.first->getCelestialBodyParameters().radius);
		neighborsOblateness.at(i)
		    = Utils::toQt(pair.first->getCelestialBodyParameters().oblateness);
	}

	auto sortedEmitters(drawnBody.getSortedRelevantNeighboringEmitters(uT));
	for(unsigned int i(0); i < lightsPosRadius.size(); ++i)
	{
		if(i >= sortedEmitters.size())
		{
			lightsPosRadius.at(i) = QVector4D(0.f, 0.f, 0.f, 0.f);
			lightsIllum.at(i)     = QVector3D(0.f, 0.f, 0.f);
			continue;
		}
		auto const& pair(sortedEmitters.at(i));
		const QVector3D lightpos(pair.second[0], pair.second[1],
		                         pair.second[2]);
		lightsPosRadius.at(i) = QVector4D(
		    lightpos, pair.first->getCelestialBodyParameters().radius);
		const float illum(pow(10.0, 0.4 * (-pair.second[3] - 14.0)));
		const QColor c(
		    Utils::toQt(pair.first->getCelestialBodyParameters().color));
		lightsIllum.at(i) = illum * QVector3D(c.redF(), c.greenF(), c.blueF());
	}

	// embbed everything in lightsIllum if model3D exists
	Node* model3D = getChild(0);
	if(model3D != nullptr)
	{
		for(unsigned int i(0); i < 2; ++i)
		{
			lightsIllum.at(i) *= computeTotalNeighborsOcclusion(
			    {}, lightsPosRadius.at(i), neighborsPosRadius,
			    neighborsOblateness);
		}
	}
}

void SpacecraftRenderer::constructModel3D()
{
	if(getChild(0) != nullptr)
	{
		return;
	}

	QMap<QString, QString> defines;
	defines["MAX_LIGHTS"] = QString::number(
	    QSettings().value("graphics/maxlightcasters").toUInt());

	auto const& drawnSpacecraft(dynamic_cast<Spacecraft const&>(drawnBody));
	const auto path(PlanetRenderer::currentSystemDir()
	                + drawnSpacecraft.getSpacecraftParameters().modelPath);

	if((path.endsWith(".gltf") || path.endsWith(".glb")) && globalIllumination
	   && !(drawnBody.getName() == "ISS" && path.endsWith("/scene.gltf")))
	{
		auto child = std::make_unique<GLTFNode>(path, nodesDict);
		child->setIgnorePreMultiply(true);
		addChild(std::move(child));
	}
	else
	{
		auto child = std::make_unique<AssimpNode>(
		    nodesDict, path, GLShaderProgram("model", "spacecraft", defines),
		    Utils::toQt(drawnSpacecraft.getCelestialBodyParameters().color));
		child->setIgnorePreMultiply(true);
		addChild(std::move(child));
	}

	for(auto& lightptr : lights)
	{
		lightptr                = std::make_unique<Light>();
		lightptr->ambiantFactor = 1e-3f;
	}
}

void SpacecraftRenderer::destructModel3D()
{
	eraseChild(0);
	for(auto& lightptr : lights)
	{
		lightptr.reset();
	}
}

// Disk on disk occlusion proportion (see notes/disk-occlusion.ggb in Geogebra
// geometry)
double getNeighborOcclusionFactor(double rLight, double rNeighbor, double dist)
{
	// both disks don't meet
	if(dist >= rLight + rNeighbor)
	{
		return 0.0;
	}

	// they meet and light is a point so occlusion == 1.0
	// early return because of divisions by sLight == 0.0
	if(rLight == 0.0)
	{
		return 1.0;
	}

	const double rLightSq    = rLight * rLight;
	const double rNeighborSq = rNeighbor * rNeighbor;

	// surfaces of light disk and neighbor disk
	const double sLight    = M_PI * rLightSq;
	const double sNeighbor = M_PI * rNeighborSq;

	// disks intersection surface
	double sX = 0.0;

	// one disk is included in the other
	if(dist <= abs(rLight - rNeighbor))
	{
		sX = fmin(sLight, sNeighbor);
	}
	else
	{
		const double alpha
		    = (rLightSq - rNeighborSq + (dist * dist)) / (2.0 * dist);
		const double x = sqrt(fmax(0.0, rLightSq - (alpha * alpha)));

		double gammaLight = asin(x / rLight);
		if(alpha < 0.0)
		{
			gammaLight = M_PI - gammaLight;
		}
		double gammaNeighbor = asin(x / rNeighbor);
		if(alpha > dist)
		{
			gammaNeighbor = M_PI - gammaNeighbor;
		}

		sX = (rLightSq * gammaLight) + (rNeighborSq * gammaNeighbor)
		     - (dist * x);
	}

	// return clamped between 0.0 and 1.0
	const double res(sX / sLight);
	return res < 0.0 ? 0.0 : (res > 1.0 ? 1.0 : res);
}

double computeTotalNeighborsOcclusion(
    QVector3D const& pos, QVector4D const& lightposradius,
    std::array<QVector4D, 5> const& neighborsPosRadius,
    std::array<QVector3D, 5> const& neighborsOblateness)
{
	const QVector3D posRelFromLight = lightposradius.toVector3D() - pos;
	const QVector3D lightdir        = posRelFromLight.normalized();

	double globalCoeffNeighbor = 1.0;

	for(unsigned int i(0); i < 5; ++i)
	{
		// if neighbor is the light caster
		if((neighborsPosRadius.at(i).toVector3D() - lightposradius.toVector3D())
		       .length()
		   < neighborsPosRadius.at(i).w())
		{
			continue;
		}
		const QVector3D posRelFromNeighbor
		    = neighborsPosRadius.at(i).toVector3D() - pos;

		// if neighbor behind light caster
		if((posRelFromNeighbor).length()
		   > (lightposradius.toVector3D() - pos).length())
		{
			continue;
		}
		// if neighbor and light are not on the same side from us
		if(QVector3D::dotProduct(lightdir, posRelFromNeighbor) < 0.0)
		{
			continue;
		}

		const double neighborRadius = neighborsPosRadius.at(i).w();

		QVector3D closestPoint
		    = QVector3D::dotProduct(lightdir, posRelFromNeighbor) * lightdir
		      - posRelFromNeighbor;

		closestPoint /= neighborsOblateness.at(i);

		globalCoeffNeighbor
		    *= (1.0
		        - getNeighborOcclusionFactor(
		            lightposradius.w() * posRelFromNeighbor.length(),
		            neighborRadius * posRelFromLight.length(),
		            posRelFromLight.length() * closestPoint.length()));
	}

	return globalCoeffNeighbor;
}
