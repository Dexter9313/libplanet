/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/StarRenderer.hpp"

#include "math/MathUtils.hpp"
#include "physics/blackbody.hpp"

StarRenderer::StarRenderer(Star const& drawnStar,
                           std::map<QString, Node*>& nodesDict)
    : CelestialBodyRenderer(drawnStar,
                            GLHandler::sRGBToLinear(Utils::toQt(
                                drawnStar.getCelestialBodyParameters().color)),
                            nodesDict)
    , billboardOriginalEdgeSize(drawnStar.getCelestialBodyParameters().radius
                                * 39.253) // eyeballed
    , glareShader("billboard", "sunglare")
    , billboard(getBillboardImage(drawnStar), std::move(glareShader))
    , detailedShader("detailedstar")
    , blackbodyTex(GLTexture::Tex1DProperties(blackbody::arrays_size()))
{
	// detailed
	detailedShader.setUniform(
	    "blackbodyBoundaries",
	    QVector2D(blackbody::min_temp(), blackbody::max_temp()));
	detailedShader.setUniform(
	    "temperature",
	    static_cast<float>(drawnStar.getStarParameters().temperature));
	detailedShader.setUniform("seed", drawnStar.getPseudoRandomSeed() / 2100.f);
	detailedShader.setUniform(
	    "oblateness",
	    Utils::toQt(drawnStar.getCelestialBodyParameters().oblateness));
	detailedShader.setUniform(
	    "starRadius",
	    static_cast<float>(drawnStar.getCelestialBodyParameters().radius));

	Primitives::setAsUnitSphere(detailedMesh, detailedShader, 50, 50);

	// NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
	blackbodyTex.setData(blackbody::red().data(), blackbody::green().data(),
	                     blackbody::blue().data());
}

void StarRenderer::updateMesh(UniversalTime const& uT,
                              OrbitalSystemCamera const& camera)
{
	CelestialBodyRenderer::updateMesh(uT, camera);

	const double transitionExposure = camera.dynamicrange
	                                  / blackbody::luminanceFromTemperature(
	                                      dynamic_cast<Star const&>(drawnBody)
	                                          .getStarParameters()
	                                          .temperature
	                                      * 2.0 / 3.0);
	double exposureScale(1.0);
	if(camera.exposure < transitionExposure)
	{
		exposureScale = camera.exposure / transitionExposure;
	}

	billboard.position = position;
	billboard.width    = billboardOriginalEdgeSize * scale * exposureScale;
	glareShader.setUniform("exposure", camera.exposure);
	glareShader.setUniform("dynamicrange", camera.dynamicrange);

	detailedShader.setUniform("time", static_cast<float>(uT % 1.0e9));

	const QVector3D campos(Utils::toQt(camera.getHeadShift() - camRelPos));
	detailedShader.setUniform("campos", campos);
}

void StarRenderer::doRender(BasicCamera const& camera,
                            std::vector<Light const*> const& lights,
                            GLTexture const& brdfLUT, bool environment)
{
	transparencyRendered = false;
	auto const& cam(dynamic_cast<OrbitalSystemCamera const&>(camera));
	const double camDist((camRelPos - cam.getHeadShift()).length());
	if(drawnBody.getCelestialBodyParameters().radius / camDist < 0.0007)
	{
		CelestialBodyRenderer::doRender(camera, lights, brdfLUT, environment);
	}
	else
	{
		detailedShader.setUniform("exposure", cam.exposure);
		detailedShader.setUniform("dynamicrange", cam.dynamicrange);
		GLHandler::setUpRender(detailedShader, CelestialBodyRenderer::model);
		GLHandler::useTextures({&blackbodyTex});
		detailedMesh.render();
	}
	if(depthWillBeCleared())
	{
		doRenderTransparent(camera, lights, brdfLUT, environment);
	}
	handleDepth();
}

void StarRenderer::doRenderTransparent(BasicCamera const& camera,
                                       std::vector<Light const*> const& lights,
                                       GLTexture const& brdfLUT,
                                       bool environment)
{
	if(transparencyRendered)
	{
		return;
	}
	auto const& cam(dynamic_cast<OrbitalSystemCamera const&>(camera));
	const double camDist((camRelPos - cam.getHeadShift()).length());
	if(drawnBody.getCelestialBodyParameters().radius / camDist >= 0.0007)
	{
		billboard.render(camera);
	}

	CelestialBodyRenderer::doRenderTransparent(camera, lights, brdfLUT,
	                                           environment);
}

QImage StarRenderer::getBillboardImage(Star const& star)
{
	const QColor starCol(Utils::toQt(star.getCelestialBodyParameters().color));
	QImage img(utils::getAbsoluteDataPath("images/star.png"));
	for(int i(0); i < img.height(); ++i)
	{
		for(int j(0); j < img.width(); ++j)
		{
			const QRgb colRGB(img.pixel(i, j));
			QColor col(colRGB);
			col.setAlpha(colRGB >> 24u);
			col.setRedF(col.redF() * starCol.redF());
			col.setGreenF(col.greenF() * starCol.greenF());
			col.setBlueF(col.blueF() * starCol.blueF());
			img.setPixel(i, j, col.rgba());
		}
	}
	return img;
}
