/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/OrbitRenderer.hpp"

#include "graphics/GraphicsUtils.hpp"

double OrbitRenderer::overridenScale = 0.0;
bool OrbitRenderer::forceRedraw      = false;
unsigned int OrbitRenderer::nextID   = 0;

GLShaderProgram& OrbitRenderer::orbitShader()
{
	static GLShaderProgram orbitShader("orbit");
	return orbitShader;
}

OrbitRenderer::OrbitRenderer(Orbitable const& drawnOrbitable,
                             unsigned int nDivisions)
    : drawnOrbitable(drawnOrbitable)
    , orbitMesh(std::make_unique<GLMesh>())
    , nDivisions(nDivisions)
    , id(nextID)
{
	++nextID;
	if(drawnOrbitable.getOrbitableType() != Orbitable::Type::BINARY)
	{
		auto const& drawnCelestialBody
		    = dynamic_cast<CelestialBody const&>(drawnOrbitable);
		color = Utils::toQt(
		    drawnCelestialBody.getCelestialBodyParameters().color);
	}
}

void OrbitRenderer::updateMesh(UniversalTime const& uT,
                               OrbitalSystemCamera const& camera)
{
	++timeSinceLastRedraw;

	currentCamRelPos = Utils::toQt(
	    camera.getRelativePositionTo(*drawnOrbitable.getParent(), uT));

	if(!forceRedraw && timeSinceLastRedraw < 5 + id)
	{
		return;
	}

	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
	auto& orbit(const_cast<Orbit&>(*drawnOrbitable.getOrbit()));
	const std::string newPatch(drawnOrbitable.getOrbit()->getCurrentPatch());

	const bool redraw(needsRedraw(orbit.getParameters()));
	if(!vertices.empty() && currentPatch == newPatch && !redraw)
	{
		return;
	}

	timeSinceLastRedraw = 0;

	if(!vertices.empty())
	{
		vertices.clear();
		orbitMesh = std::make_unique<GLMesh>();
	}

	const double maxTrueAn(orbit.getTrueAnomalyAtUT(uT));
	double minTrueAn(maxTrueAn - 2.0 * constant::pi);

	hyperbolic = false;
	// change limits for hyperbolic orbits
	if(orbit.getParameters().eccentricity > 1.0
	   // test for NaN period
	   || orbit.getPeriod() != orbit.getPeriod())
	{
		// https://en.wikipedia.org/wiki/Hyperbolic_trajectory#Eccentricity_and_angle_between_approach_and_departure
		minTrueAn  = 0.0;
		hyperbolic = true;
	}

	std::vector<unsigned int> elements;
	for(unsigned int current(0); current < nDivisions; ++current)
	{
		const double t = current / static_cast<double>(nDivisions - 1);
		const double trueAnomaly = minTrueAn + t * (maxTrueAn - minTrueAn);
		elements.push_back(current);
		elements.push_back(current + 1);
		Vector3 pos(orbit.getPositionFromTrueAn(trueAnomaly));
		vertices.push_back(pos[0]);
		vertices.push_back(pos[1]);
		vertices.push_back(pos[2]);
	}
	elements.pop_back();
	if(!hyperbolic)
	{
		elements.push_back(0);
	}
	orbitMesh->setVertexShaderMapping(orbitShader(), {{"position", 3}});
	orbitMesh->setVertices(vertices, elements);

	currentPatch      = newPatch;
	currentParameters = orbit.getParameters();
}

void OrbitRenderer::renderTransparent(BasicCamera const& camera, float alpha)
{
	auto const& cam = dynamic_cast<OrbitalSystemCamera const&>(camera);
	orbitShader().setUniform("color", color);
	orbitShader().setUniform("hyperbolic", hyperbolic ? 1.f : 0.f);
	orbitShader().setUniform("alpha", alpha);
	orbitShader().setUniform("cameraRelPos", currentCamRelPos);
	orbitShader().setUniform("overridenScale",
	                         static_cast<float>(overridenScale));
	orbitShader().setUniform("exposure", cam.exposure);
	orbitShader().setUniform("dynamicrange", cam.dynamicrange);
	const GLBlendSet glBlend(GLBlendSet::BlendState{});
	GLHandler::setUpRender(orbitShader(), QMatrix4x4());
	GLHandler::glf().glLineWidth(2.f);
	orbitMesh->render(PrimitiveType::LINES);
	GLHandler::glf().glLineWidth(1.f);
}

bool OrbitRenderer::needsRedraw(Orbit::Parameters newParams)
{
	if(newParams.semiMajorAxis / currentParameters.semiMajorAxis < 0.9
	   || currentParameters.semiMajorAxis / newParams.semiMajorAxis < 0.9)
	{
		return true;
	}

	if(newParams.eccentricity / currentParameters.eccentricity < 0.9
	   || currentParameters.eccentricity / newParams.eccentricity < 0.9)
	{
		return true;
	}

	double incDiff(newParams.inclination - currentParameters.inclination);
	int q(floor(incDiff / (2.0 * constant::pi)));
	incDiff -= (q * 2.0 + 1.0) * constant::pi;
	if(abs(incDiff) > 0.2)
	{
		return true;
	}

	double anlDiff(newParams.ascendingNodeLongitude
	               - currentParameters.ascendingNodeLongitude);
	q = floor(anlDiff / (2.0 * constant::pi));
	anlDiff -= (q * 2.0 + 1.0) * constant::pi;
	if(abs(anlDiff) > 0.3)
	{
		return true;
	}

	double paDiff(newParams.periapsisArgument
	              - currentParameters.periapsisArgument);
	q = floor(paDiff / (2.0 * constant::pi));
	paDiff -= (q * 2.0 + 1.0) * constant::pi;

	return abs(paDiff) > 0.3;
}
