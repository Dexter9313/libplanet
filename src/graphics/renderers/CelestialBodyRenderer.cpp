/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/CelestialBodyRenderer.hpp"

double& CelestialBodyRenderer::overridenScale()
{
	static double overridenScale = 0.0;
	return overridenScale;
}

float& CelestialBodyRenderer::renderLabels()
{
	static float renderLabels = 0.f;
	return renderLabels;
}

float& CelestialBodyRenderer::renderOrbits()
{
	static float renderOrbits = 0.f;
	return renderOrbits;
}

QStringList& CelestialBodyRenderer::renderLabelsOrbitsOnly()
{
	static QStringList renderLabelsOrbitsOnly;
	return renderLabelsOrbitsOnly;
}

GLShaderProgram& CelestialBodyRenderer::pointShader()
{
	static GLShaderProgram pointShader("pointcelestbody");
	return pointShader;
}

CelestialBodyRenderer::CelestialBodyRenderer(
    CelestialBody const& drawnBody, QColor const& pointColor,
    std::map<QString, Node*>& nodesDict)
    : Node(nodesDict, drawnBody.getName().c_str())
    , drawnBody(drawnBody)
    , label(drawnBody.getDisplayName().c_str(),
            Utils::toQt(drawnBody.getCelestialBodyParameters().color))
{
	// POINT
	float r = NAN, g = NAN, b = NAN;
	r = pointColor.redF();
	g = pointColor.greenF();
	b = pointColor.blueF();
	pointMesh.setVertexShaderMapping(pointShader(), {{"color", 3}});
	pointMesh.setVertices({r, g, b});

	if(DebrisRenderer::filePathExistsForBody(drawnBody.getName().c_str()))
	{
		debrisRenderer = std::make_unique<DebrisRenderer>(drawnBody);
	}
}

void CelestialBodyRenderer::updateMesh(UniversalTime const& uT,
                                       OrbitalSystemCamera const& camera)
{
	OrbitRenderer::overridenScale = overridenScale();
	if(orbitRenderer == nullptr && drawnBody.getOrbit() != nullptr)
	{
		orbitRenderer = std::make_unique<OrbitRenderer>(drawnBody);
	}
	if(renderOrbits() > 0.f && orbitRenderer != nullptr)
	{
		orbitRenderer->updateMesh(uT, camera);
	}
	if(debrisRenderer != nullptr)
	{
		debrisRenderer->update(uT);
	}

	camRelPos    = camera.getRelativePositionTo(drawnBody, uT);
	lastUpdateUt = uT;

	const double centerPosition = 5000.f;

	const double camDist(camRelPos.length());
	scale = centerPosition / camDist;

	if(scale > 1.0)
	{
		scale = 1.0;
	}

	if(overridenScale() != 0.0 && overridenScale() < scale)
	{
		scale            = overridenScale();
		clearDepthBuffer = false;
	}
	else
	{
		clearDepthBuffer = true;
	}

	const double radiusScale(drawnBody.getCelestialBodyParameters().radius
	                         * scale);
	position = Utils::toQt(scale * camRelPos);
	const double camDistVR((camRelPos - camera.getHeadShift()).length());
	apparentAngle
	    = 2.0 * atan(drawnBody.getCelestialBodyParameters().radius / camDistVR);
	if(drawnBody.getOrbit() != nullptr)
	{
		if(drawnBody.getOrbit()->getParameters().semiMajorAxis < 0.0)
		{
			orbitApparentAngle = DBL_MAX;
		}
		else
		{
			orbitApparentAngle
			    = abs(2.0
			          * atan(drawnBody.getOrbit()->getParameters().semiMajorAxis
			                 / camDistVR));
		}
	}
	// for rootOrbitable
	else
	{
		orbitApparentAngle = DBL_MAX;
	}

	model = QMatrix4x4();
	model.translate(position);
	model.scale(radiusScale);

	properRotation = Utils::toQt(drawnBody.getProperRotationAtUT(uT));

	const Vector3 labelPos(
	    camRelPos
	    + Vector3(0.0, 0.0,
	              2.0 * drawnBody.getCelestialBodyParameters().radius));
	// Vector3 camRelPosVR(camera.getHeadShift() - camRelPos - labelPos);
	const Vector3 unitRelPos((camera.getHeadShift() - labelPos).getUnitForm());

	const float yaw(atan2(unitRelPos[1], unitRelPos[0]));
	const float pitch(-1.0 * asin(unitRelPos[2]));
	QMatrix4x4 model;
	model.translate(Utils::toQt(scale * labelPos));
	model.scale(camDistVR * scale / 3.f);
	model.rotate(yaw * 180.f / M_PI + 90.f, 0.0, 0.0, 1.0);
	model.rotate(pitch * 180.f / M_PI + 90.f, 1.0, 0.0, 0.0);
	label.updateModel(model);
	label.setAlpha(renderLabels());
}

void CelestialBodyRenderer::doRender(BasicCamera const& camera,
                                     std::vector<Light const*> const& lights,
                                     GLTexture const& brdfLUT, bool environment)
{
	transparencyRendered = false;
	renderPoint(dynamic_cast<OrbitalSystemCamera const&>(camera));
	if(depthWillBeCleared())
	{
		doRenderTransparent(camera, lights, brdfLUT, environment);
	}
	handleDepth();
}

void CelestialBodyRenderer::doRenderTransparent(
    BasicCamera const& camera, std::vector<Light const*> const& /*lights*/,
    GLTexture const& /*brdfLUT*/, bool environment)
{
	if(transparencyRendered)
	{
		return;
	}
	transparencyRendered = true;
	if(environment
	   || (!renderLabelsOrbitsOnly().empty()
	       && !renderLabelsOrbitsOnly().contains(drawnBody.getName().c_str())))
	{
		return;
	}

	if(renderLabels() > 0.f && apparentAngle < 15.0 * constant::pi / 180.0
	   && orbitApparentAngle > 7.0 * constant::pi / 180.0)
	{
		auto const& cam(dynamic_cast<OrbitalSystemCamera const&>(camera));
		label.render(cam.exposure, cam.dynamicrange);
	}
	if(renderOrbits() > 0.f && orbitRenderer != nullptr
	   && apparentAngle < 5.0 * constant::pi / 180.0
	   && orbitApparentAngle > 7.0 * constant::pi / 180.0)
	{
		orbitRenderer->renderTransparent(camera, renderOrbits());
	}

	if(debrisRenderer != nullptr)
	{
		debrisRenderer->render(model,
		                       drawnBody.getCelestialBodyParameters().radius);
	}
}

void CelestialBodyRenderer::handleDepth() const
{
	if(clearDepthBuffer)
	{
		GLHandler::clearDepthBuffer();
	}
}

void CelestialBodyRenderer::renderPoint(OrbitalSystemCamera const& camera)
{
	pointShader().setUniform("mag",
	                         static_cast<float>(drawnBody.getApparentMagnitude(
	                             -1.0 * camRelPos, lastUpdateUt)));
	pointShader().setUniform("pixelSolidAngle", camera.pixelSolidAngle());
	setPointShaderParams(lastUpdateUt, camera, pointShader());

	const GLStateSet glState({{GL_MULTISAMPLE, false}});
	const GLBlendSet glBlend({GL_ONE, GL_ONE}); // sum points
	GLHandler::setUpRender(pointShader(), model);
	pointMesh.render();
}
