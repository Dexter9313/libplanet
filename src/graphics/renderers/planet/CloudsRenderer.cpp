/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/CloudsRenderer.hpp"

CloudsRenderer::CloudsRenderer(Planet const& drawnBody,
                               QString const& texturePath)
    : drawnBody(drawnBody)
    , shaderHandler("planet/clouds", drawnBody)
    , procedural(texturePath == "")
{
	Primitives::setAsUnitSphere(cloudSphere, shaderHandler.getShader(), 500,
	                            500, PrimitiveType::TRIANGLES);

	unsigned int texmaxsize(QSettings().value("graphics/texmaxsize").toUInt());
	if(procedural)
	{
		if(texmaxsize > 3)
		{
			texmaxsize = 3;
		}
		GLShaderProgram sclouds("planet/gentex/planet", "planet/gentex/clouds");
		sclouds.setUniform("seed", drawnBody.getPseudoRandomSeed());

		genTex = std::make_unique<GeneratedTexture>(
		    std::move(sclouds), texmaxsize * 2048, texmaxsize * 1024);
	}
	else
	{
		asyncTex = std::make_unique<AsyncTexture>(
		    texturePath, texmaxsize * 2048, texmaxsize * 1024,
		    QColor(0, 0, 0, 0));
	}

	auto const& shader(shaderHandler.getShader());
	shader.setUniform("tex", 0);
	shader.setUniform("atmData.preproc", 1);
	shader.setUniform("atmData.emissionLUT", 2);
	shader.setUniform("atmData.absorptionLUT", 3);
	shader.setUniform("night", 4);
}

void CloudsRenderer::update()
{
	if(procedural)
	{
		genTex->render();
	}
}

void CloudsRenderer::renderTransparent(
    PlanetShaderHandler::EvolvingParameters const& params,
    AtmosphereRenderer const& atmosphereRenderer, GLTexture const* night)
{
	shaderHandler.update(params);

	const GLBlendSet glBlend({GL_ONE, GL_SRC1_COLOR});

	std::vector<GLTexture const*> texs;
	texs.push_back(procedural ? &genTex->getTexture()
	                          : &asyncTex->getTexture());
	auto atmosTexs(atmosphereRenderer.computeLUTs(
	    params,
	    drawnBody.getCelestialBodyParameters().radius
	        + drawnBody.getAtmosphere()->clouds,
	    1));
	texs.push_back(atmosTexs[0]);
	texs.push_back(atmosTexs[1]);
	texs.push_back(atmosTexs[2]);
	texs.push_back(night);
	GLHandler::useTextures(texs);

	// render back face
	{
		const GLCullFaceSet glCullFace(GL_FRONT);
		GLHandler::setUpRender(shaderHandler.getShader(),
		                       params.model * params.properRotation);
		cloudSphere.render();
	}

	atmosTexs = atmosphereRenderer.computeLUTs(
	    params,
	    drawnBody.getCelestialBodyParameters().radius
	        + drawnBody.getAtmosphere()->clouds,
	    0);
	texs[1] = atmosTexs[0];
	texs[2] = atmosTexs[1];
	texs[3] = atmosTexs[2];
	GLHandler::useTextures(texs);

	bool camIsFarEnough(params.campos.length()
	                    > 1.1 * drawnBody.getCelestialBodyParameters().radius);

	const GLStateSet glState({{GL_POLYGON_OFFSET_FILL, camIsFarEnough}});
	// render front face
	if(camIsFarEnough)
	{
		GLHandler::glf().glPolygonOffset(-2.0, -2.0);
	}

	GLHandler::setUpRender(shaderHandler.getShader(),
	                       params.model * params.properRotation);
	cloudSphere.render();
}
