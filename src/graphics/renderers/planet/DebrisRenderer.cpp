/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/DebrisRenderer.hpp"
#include "graphics/renderers/planet/PlanetRenderer.hpp"

#include <QJsonDocument>

float DebrisRenderer::opacity     = 0.f;
float DebrisRenderer::asteroids   = 0.f;
unsigned int DebrisRenderer::size = 1;

DebrisRenderer::DebrisRenderer(CelestialBody const& drawnBody)
    : shader("planet/debris")
    , bodyName(drawnBody.getName().c_str())
{
	std::vector<float> vertices;
	if(filePathExistsForBody(bodyName))
	{
		for(auto const& filePath : getFilePathsForBody(bodyName))
		{
			QFile f(filePath);
			f.open(QIODevice::ReadOnly);
			const QJsonArray json = QJsonDocument::fromJson(f.readAll())
			                            .object()["data"]
			                            .toArray();
			for(auto elem : json)
			{
				const auto obt(elem.toObject()["orbit"].toObject());
				const float sma(obt["SMA"].toDouble());
				const float ecc(obt["ECC"].toDouble());
				const float inc(obt["INC"].toDouble());
				const float anl(obt["LAN"].toDouble());
				const float pArg(obt["pARG"].toDouble());
				const float MAepoch(obt["M"].toDouble());
				vertices.push_back(sma);
				vertices.push_back(ecc);
				vertices.push_back(inc);
				vertices.push_back(anl);
				vertices.push_back(pArg);
				vertices.push_back(MAepoch);
			}
		}
	}
	else
	{
		const float r(drawnBody.getCelestialBodyParameters().radius);
		vertices = {{2.f * r, 0.f, 0.1f, 0.f, 0.f, 0.f, 3.f * r, 0.f, 0.2f, 0.f,
		             0.f, 0.f}};
	}
	mesh.setVertexShaderMapping(shader, {{"sma", 1},
	                                     {"ecc", 1},
	                                     {"inc", 1},
	                                     {"anl", 1},
	                                     {"per", 1},
	                                     {"MAepoch", 1}});
	mesh.setVertices(vertices);

	shader.setUniform(
	    "gravParam",
	    static_cast<float>(constant::G
	                       * drawnBody.getCelestialBodyParameters().mass));
}

void DebrisRenderer::update(UniversalTime const& uT)
{
	if(bodyName != "Earth" && bodyName != "Sun")
	{
		return;
	}
	if(bodyName == "Earth" && opacity == 0.f)
	{
		return;
	}
	if(bodyName == "Sun" && asteroids == 0.f)
	{
		return;
	}
	const float high_ut = floor(static_cast<double>(uT) * 1e-6);
	const float low_ut  = static_cast<float>(uT - high_ut * 1e6);
	shader.setUniform("high_ut", high_ut);
	shader.setUniform("low_ut", low_ut);
	shader.setUniform("opacity", bodyName == "Earth" ? opacity : asteroids);
	QColor color;
	if(bodyName == "Sun")
	{
		color = QColor(20, 20, 20);
	}
	else
	{
		color = QColor(255, 91, 0);
	}
	shader.setUniform("color", color);
}

void DebrisRenderer::render(QMatrix4x4 const& model, float bodyRadius)
{
	if(bodyName != "Earth" && bodyName != "Sun")
	{
		return;
	}
	if(bodyName == "Earth" && opacity == 0.f)
	{
		return;
	}
	if(bodyName == "Sun" && asteroids == 0.f)
	{
		return;
	}
	shader.setUniform("planetRadius", bodyRadius);

	const GLStateSet glState({{GL_DEPTH_CLAMP, true}});
	GLHandler::glf().glDepthFunc(GL_LEQUAL);
	GLHandler::setPointSize(size);
	const GLBlendSet glBlend(GLBlendSet::BlendState{});
	GLHandler::setUpRender(shader, model);
	mesh.render();
	GLHandler::setPointSize(1);
	GLHandler::glf().glDepthFunc(GL_LESS);
}

bool DebrisRenderer::filePathExistsForBody(QString const& bodyName)
{
	return !getFilePathsForBody(bodyName).isEmpty();
}

QStringList DebrisRenderer::getFilePathsForBody(QString const& bodyName)
{
	const QString basename
	    = PlanetRenderer::currentSystemDir() + "/debris/" + bodyName;
	if(QFile::exists(basename))
	{
		return {basename};
	}
	unsigned int i = 1;
	QStringList result;
	while(QFile::exists(basename + "." + QString::number(i)))
	{
		result << basename + "." + QString::number(i);
		i += 1;
	}
	return result;
}
