/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include "graphics/renderers/planet/PlanetRenderer.hpp"

QString& PlanetRenderer::currentSystemDir()
{
	static QString currentSystemDir;
	return currentSystemDir;
}

PlanetRenderer::PlanetRenderer(Planet const& drawnBody,
                               std::map<QString, Node*>& nodesDict)
    : CelestialBodyRenderer(
          drawnBody, computePointColor(drawnBody, drawnBody.getHostStar()),
          nodesDict)
    , boundingSphere(drawnBody.getCelestialBodyParameters().radius)
    , unloadedShaderHandler("planet/planet", "planet/uniformplanet", drawnBody)
{
	// UNLOADED
	Primitives::setAsUnitSphere(unloadedMesh, unloadedShaderHandler.getShader(),
	                            50, 50);
	unloadedShaderHandler.getShader().setUniform(
	    "color", Utils::toQt(drawnBody.getCelestialBodyParameters().color));

	const QString name(drawnBody.getName().c_str());
	terrainPath = currentSystemDir() + "/terrains/" + name;
	if(!QFile::exists(terrainPath))
	{
		terrainPath = "";
	}

	const float outerRing(drawnBody.getPlanetParameters().outerRing);
	if(outerRing != 0.f)
	{
		QString rings;
		QString str = currentSystemDir() + "/images/" + name + "/rings.jpg";
		if(QFileInfo(str).exists())
		{
			rings = str;
		}
		str = currentSystemDir() + "/images/" + name + "/rings.png";
		if(QFileInfo(str).exists())
		{
			rings = str;
		}

		ringsRenderer = std::make_unique<RingsRenderer>(
		    drawnBody, drawnBody.getPseudoRandomSeed(), rings);
	}

	// Atmosphere
	Planet::Atmosphere* atmosphere = drawnBody.getAtmosphere();
	if(atmosphere != nullptr)
	{
		atmosphereRenderer = std::make_unique<AtmosphereRenderer>(drawnBody);
		if(atmosphere->clouds != 0.0)
		{
			QString str = currentSystemDir() + "/images/"
			              + drawnBody.getName().c_str() + "/clouds.jpg";
			if(!QFileInfo(str).exists())
			{
				str = currentSystemDir() + "/images/"
				      + drawnBody.getName().c_str() + "/clouds.png";
				if(!QFileInfo(str).exists())
				{
					str = "";
				}
			}
			cloudsRenderer = std::make_unique<CloudsRenderer>(drawnBody, str);
		}
	}
}

void PlanetRenderer::setPointShaderParams(UniversalTime const& uT,
                                          OrbitalSystemCamera const& /*camera*/,
                                          GLShaderProgram const& pointShader)
{
	updateNeighborsData(uT);
	pointShader.setUniform("lightposradius", lightsPosRadius[0]);
	pointShader.setUniform("neighData.posRadius", 5, neighborsPosRadius.data());
	pointShader.setUniform("neighData.oblateness", 5,
	                       neighborsOblateness.data());
}

void PlanetRenderer::updateMesh(UniversalTime const& uT,
                                OrbitalSystemCamera const& camera)
{
	CelestialBodyRenderer::updateMesh(uT, camera);
	auto const& drawnPlanet(dynamic_cast<Planet const&>(drawnBody));

	bool customModel = false;
	if(detailedPlanet != nullptr)
	{
		const float modelRadius(detailedPlanet->getCustomModelBoundingSphere());
		customModel = (modelRadius > 0.f);
		if(customModel)
		{
			boundingSphere = modelRadius * 1000.f;
		}
	}

	// custom models have km units, not radius units
	if(customModel)
	{
		model.scale(1000.0 / drawnBody.getCelestialBodyParameters().radius);
	}

	if(drawnPlanet.getPlanetParameters().outerRing == 0.f)
	{
		culled = OrbitalSystemCamera::shouldBeCulled(position,
		                                             boundingSphere * scale);
	}
	else
	{
		// take rings into account !
		const double cullingRadius(drawnPlanet.getPlanetParameters().outerRing
		                           * scale);
		culled = OrbitalSystemCamera::shouldBeCulled(position, cullingRadius);
	}

	const float pixFOV(camera.pixelVertFOV());
	apparentAngleAllowsUnloaded = apparentAngle >= 2.0 * pixFOV;
	apparentAngleAllowsDetailed = apparentAngle >= 12.0 * pixFOV;

	if(customModel
	   && (!apparentAngleAllowsDetailed || detailedPlanet == nullptr
	       || !detailedPlanet->getAsyncMesh().isLoaded()))
	{
		model.scale(drawnBody.getCelestialBodyParameters().radius / 1000.0);
	}

	if(!apparentAngleAllowsDetailed)
	{
		unloadPlanet();
		if(!apparentAngleAllowsUnloaded)
		{
			return;
		}
	}
	else if(pixFOV > 0.f) // on frame 0 pixFOV is 0.f and loads everything...
	{
		loadPlanet();
	}
	if(culled)
	{
		return;
	}

	updateNeighborsData(uT);

	if(detailedPlanet != nullptr)
	{
		// planet->updateTextureLoading();
		detailedPlanet->update(properRotation, lightsPosRadius[0].toVector3D(),
		                       lightsPosRadius[1].toVector3D());
		if(cloudsRenderer != nullptr)
		{
			cloudsRenderer->update();
		}
	}
}

void PlanetRenderer::doRender(BasicCamera const& camera,
                              std::vector<Light const*> const& lights,
                              GLTexture const& brdfLUT, bool environment)
{
	transparencyRendered = false;
	if(culled)
	{
		if(depthWillBeCleared())
		{
			CelestialBodyRenderer::doRenderTransparent(camera, lights, brdfLUT,
			                                           environment);
		}
		return;
	}

	auto const& cam(dynamic_cast<OrbitalSystemCamera const&>(camera));
	const QVector3D campos(Utils::toQt(cam.getHeadShift() - camRelPos));

	evolvingParams = {model,
	                  properRotation,
	                  campos,
	                  lightsPosRadius,
	                  lightsIllum,
	                  neighborsPosRadius,
	                  neighborsOblateness,
	                  cam.exposure,
	                  cam.dynamicrange};

	if(!apparentAngleAllowsUnloaded)
	{
		CelestialBodyRenderer::doRender(camera, lights, brdfLUT, environment);
	}
	else if(!apparentAngleAllowsDetailed
	        || (detailedPlanet == nullptr && terrain == nullptr)
	        || (terrain != nullptr && !terrain->isLoaded()))
	{
		if(terrain != nullptr)
		{
			const auto model
			    = evolvingParams.model * evolvingParams.properRotation;
			terrain->update(camera, model);
		}
		unloadedShaderHandler.update(evolvingParams);
		GLHandler::setUpRender(unloadedShaderHandler.getShader(),
		                       model * properRotation);
		unloadedMesh.render();
	}
	else
	{
		if(detailedPlanet != nullptr)
		{
			GLTexture const* ringsTex(nullptr);
			if(ringsRenderer != nullptr)
			{
				ringsTex = &ringsRenderer->getTexture();
			}

			detailedPlanet->render(evolvingParams, ringsTex,
			                       atmosphereRenderer.get());
		}
		else if(terrain != nullptr)
		{
			const auto model
			    = evolvingParams.model * evolvingParams.properRotation;
			terrain->update(camera, model);
			terrain->render();
		}
	}

	if(depthWillBeCleared())
	{
		doRenderTransparent(camera, lights, brdfLUT, environment);
	}
	handleDepth();
}

void PlanetRenderer::doRenderTransparent(
    BasicCamera const& camera, std::vector<Light const*> const& lights,
    GLTexture const& brdfLUT, bool environment)
{
	if(transparencyRendered)
	{
		return;
	}

	GLTexture const* night(nullptr);
	if(detailedPlanet != nullptr)
	{
		night = &detailedPlanet->getNightTexture();
	}

	/*if(false) // culled || !apparentAngleAllowsUnloaded)
	{
	    if(atmosphereRenderer != nullptr)
	    {
	        atmosphereRenderer->renderTransparent(evolvingParams);
	    }
	    if(cloudsRenderer != nullptr)
	    {
	        cloudsRenderer->renderTransparent(evolvingParams,
	                                          *atmosphereRenderer, night);
	    }
	    CelestialBodyRenderer::renderTransparent(camera);
	    return;
	}*/

	if(apparentAngleAllowsDetailed)
	{
		if(ringsRenderer != nullptr)
		{
			ringsRenderer->render(evolvingParams);
		}
		if(atmosphereRenderer != nullptr)
		{
			atmosphereRenderer->renderTransparent(evolvingParams);
		}
		if(cloudsRenderer != nullptr)
		{
			cloudsRenderer->renderTransparent(evolvingParams,
			                                  *atmosphereRenderer, night);
		}
	}

	CelestialBodyRenderer::doRenderTransparent(camera, lights, brdfLUT,
	                                           environment);
}

void PlanetRenderer::loadPlanet()
{
	if(detailedPlanet != nullptr || terrain != nullptr)
	{
		return;
	}

	auto const& drawnPlanet(dynamic_cast<Planet const&>(drawnBody));
	if(terrainPath.isEmpty())
	{
		detailedPlanet = std::make_unique<DetailedPlanetRenderer>(
		    drawnPlanet, currentSystemDir());
	}
	else
	{
		terrain = std::make_unique<trn::Terrain>(
		    terrainPath,
		    Utils::toQt(drawnBody.getCelestialBodyParameters().oblateness),
		    drawnPlanet.getName() == "Earth", "planet/custom_patch");
		terrainAlbedoCoeffLeftToApply = terrain->getRootPatchesNumber();
		;
		terrain->setStaticUniformsFunction(
		    [&drawnPlanet](GLShaderProgram const& shader)
		    {
			    PlanetShaderHandler::setConstantUniforms(shader, drawnPlanet);

			    shader.setUniform("planetData.albedoCoeff", 1.f);
			    shader.setUniform("planetData.roughness", 1.f);
			    // samplers
			    shader.setUniform("atmData.preproc", 0);
			    shader.setUniform("atmData.emissionLUT", 1);
			    shader.setUniform("atmData.absorptionLUT", 2);
			    shader.setUniform("ringsData.tex", 3);
		    });
		terrain->setDynamicUniformsFunction(
		    [this, &drawnPlanet](GLShaderProgram const& shader)
		    {
			    PlanetShaderHandler::update(
			        shader, getEvolvingParams(),
			        QSettings().value("graphics/maxlightcasters").toUInt());
			    if(terrainAlbedoCoeffLeftToApply > 0 && terrain->isLoaded())
			    {
				    auto avgLinear = terrain->getAverageDiffuseLinearColor();
				    if(avgLinear.isValid())
				    {
					    const float coeff((avgLinear.redF() + avgLinear.greenF()
					                       + avgLinear.blueF())
					                      / 3.f);
					    const float albedoCoeff
					        = drawnPlanet.getPlanetParameters().albedo / coeff;
					    shader.setUniform("planetData.albedoCoeff",
					                      albedoCoeff);
					    --terrainAlbedoCoeffLeftToApply;
				    }
			    }
		    });
		terrain->setPopulateTexturesFunction(
		    [this, &drawnPlanet](std::vector<GLTexture const*>& texs)
		    {
			    if(atmosphereRenderer != nullptr)
			    {
				    auto atmoTexs = atmosphereRenderer->computeLUTs(
				        getEvolvingParams(),
				        drawnPlanet.getCelestialBodyParameters().radius, 0);
				    texs.push_back(atmoTexs[0]);
				    texs.push_back(atmoTexs[1]);
				    texs.push_back(atmoTexs[2]);
			    }
			    else
			    {
				    texs.push_back(nullptr);
				    texs.push_back(nullptr);
				    texs.push_back(nullptr);
			    }
			    if(ringsRenderer != nullptr)
			    {
				    texs.push_back(&ringsRenderer->getTexture());
			    }
			    else
			    {
				    texs.push_back(nullptr);
			    }
		    });
	}
}

void PlanetRenderer::unloadPlanet(bool /*waitIfPlanetNotLoaded*/)
{
	if(detailedPlanet == nullptr && terrain == nullptr)
	{
		return;
	}

	/*if(planet->isLoading() && !waitIfPlanetNotLoaded)
	{
	    planet->updateTextureLoading();
	    return;
	}*/
	detailedPlanet.reset();
	terrain.reset();
}

QColor PlanetRenderer::computePointColor(Planet const& drawnPlanet,
                                         Star const* star)
{
	QColor lightcolor;
	if(star == nullptr)
	{
		lightcolor = QColor::fromRgbF(1.0, 1.0, 1.0, 1.0);
	}
	else
	{
		lightcolor = Utils::toQt(star->getCelestialBodyParameters().color);
	}
	Color color(drawnPlanet.getCelestialBodyParameters().color);
	color.r *= lightcolor.redF();
	color.g *= lightcolor.greenF();
	color.b *= lightcolor.blueF();
	double maxC(color.r);
	if(color.g > maxC)
	{
		maxC = color.g;
	}
	if(color.b > maxC)
	{
		maxC = color.b;
	}
	maxC /= 255.f;
	color.r /= maxC;
	color.g /= maxC;
	color.b /= maxC;
	return Utils::toQt(color);
}

void PlanetRenderer::updateNeighborsData(UniversalTime const& uT)
{
	const auto sortedOccluders(
	    drawnBody.getSortedRelevantNeighboringOccluders(uT));
	for(unsigned int i(0); i < neighborsPosRadius.size(); ++i)
	{
		if(i >= sortedOccluders.size())
		{
			neighborsPosRadius.at(i)  = QVector4D(0.f, 0.f, 0.f, 0.f);
			neighborsOblateness.at(i) = QVector3D(1.f, 1.f, 1.f);
			continue;
		}
		auto const& pair(sortedOccluders.at(i));
		neighborsPosRadius.at(i)
		    = QVector4D(Utils::toQt(pair.second),
		                pair.first->getCelestialBodyParameters().radius);
		neighborsOblateness.at(i)
		    = Utils::toQt(pair.first->getCelestialBodyParameters().oblateness);
	}

	const auto sortedEmitters(
	    drawnBody.getSortedRelevantNeighboringEmitters(uT));
	for(unsigned int i(0); i < lightsPosRadius.size(); ++i)
	{
		if(i >= sortedEmitters.size())
		{
			lightsPosRadius.at(i) = QVector4D(0.f, 0.f, 0.f, 0.f);
			lightsIllum.at(i)     = QVector3D(0.f, 0.f, 0.f);
			continue;
		}

		auto const& pair(sortedEmitters.at(i));
		lightsPosRadius.at(i)
		    = QVector4D(pair.second[0], pair.second[1], pair.second[2],
		                pair.first->getCelestialBodyParameters().radius);

		const float mag(pair.second[3]);
		const float illum(pow(10.0, 0.4 * (-mag - 14.0)));
		const QColor c(
		    Utils::toQt(pair.first->getCelestialBodyParameters().color));
		lightsIllum.at(i) = illum * QVector3D(c.redF(), c.greenF(), c.blueF());
	}
}
