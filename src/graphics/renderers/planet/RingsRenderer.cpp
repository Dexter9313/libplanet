/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/RingsRenderer.hpp"

#include <numbers>

RingsRenderer::RingsRenderer(Planet const& drawnBody, float seed,
                             QString const& texturePath)
    : shaderHandler("planet/ring", drawnBody)
{
	const double planetRadius(drawnBody.getCelestialBodyParameters().radius);
	const double innerRing(drawnBody.getPlanetParameters().innerRing
	                       / planetRadius);
	const double outerRing(drawnBody.getPlanetParameters().outerRing
	                       / planetRadius);

	const float coeff(1.f / cos(std::numbers::pi / 20.0));

	std::vector<float> ringVertices;
	std::vector<unsigned int> ringElements;
	for(unsigned int i(0); i < 20; ++i)
	{
		// 2 * i
		ringVertices.emplace_back(innerRing
		                          * cos(i * std::numbers::pi_v<float> / 10.f));
		ringVertices.emplace_back(innerRing
		                          * sin(i * std::numbers::pi_v<float> / 10.f));

		// 2 * i + 1
		ringVertices.emplace_back(outerRing * coeff
		                          * cos(i * std::numbers::pi_v<float> / 10.f));
		ringVertices.emplace_back(outerRing * coeff
		                          * sin(i * std::numbers::pi_v<float> / 10.f));

		if(i != 0)
		{
			ringElements.emplace_back(2 * (i - 1));
			ringElements.emplace_back(2 * (i - 1) + 1);
			ringElements.emplace_back(2 * i + 1);

			ringElements.emplace_back(2 * (i - 1));
			ringElements.emplace_back(2 * i + 1);
			ringElements.emplace_back(2 * i);
		}
	}
	ringElements.emplace_back(2 * (20 - 1));
	ringElements.emplace_back(2 * (20 - 1) + 1);
	ringElements.emplace_back(1);

	ringElements.emplace_back(2 * (20 - 1));
	ringElements.emplace_back(1);
	ringElements.emplace_back(0);

	mesh.setVertexShaderMapping(shaderHandler.getShader(), {{"position", 2}});
	mesh.setVertices(ringVertices, ringElements);

	if(texturePath == "")
	{
		texTarget = std::make_unique<GLFramebufferObject>(
		    GLTexture::Tex2DProperties(16000, 1));
		update(seed);
	}
	else
	{
		procedural = false;
		tex        = std::make_unique<AsyncTexture>(texturePath,
		                                            QColor(210, 180, 140));
	}
}

GLTexture const& RingsRenderer::getTexture()
{
	return procedural ? texTarget->getColorAttachmentTexture()
	                  : tex->getTexture();
}

void RingsRenderer::update(float seed)
{
	if(!procedural)
	{
		return;
	}

	const GLShaderProgram s("planet/gentex/ringtex");
	GLMesh tmpMesh;
	tmpMesh.setVertexShaderMapping(s, {{"position", 2}});
	tmpMesh.setVertices({-1.f, -1.f, 1.f, -1.f, -1.f, 1.f, 1.f, 1.f});

	s.setUniform("color", QColor(210, 180, 140));
	s.setUniform("seed", seed);

	GLHandler::beginRendering(*texTarget);
	s.use();
	tmpMesh.render(PrimitiveType::TRIANGLE_STRIP);
}

void RingsRenderer::render(
    PlanetShaderHandler::EvolvingParameters const& params)
{
	shaderHandler.update(params);

	const GLBlendSet glBlend(GLBlendSet::BlendState{});
	GLHandler::setUpRender(shaderHandler.getShader(),
	                       params.model * params.properRotation);
	GLHandler::useTextures({&getTexture()});
	const GLStateSet glState({{GL_CULL_FACE, false}});
	mesh.render();
}
