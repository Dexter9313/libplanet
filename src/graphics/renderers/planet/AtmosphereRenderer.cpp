/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/AtmosphereRenderer.hpp"

QMap<QString, QString> AtmosphereRenderer::constructDefines()
{
	QMap<QString, QString> defines;
	defines["NUM_IN_SCATTER"] = QString::number(
	    20 + 10 * QSettings().value("graphics/atmoquality").toUInt());
	defines["MAX_LIGHTS"] = QString::number(
	    QSettings().value("graphics/maxlightcasters").toUInt());
	return defines;
}

AtmosphereRenderer::AtmosphereRenderer(Planet const& drawnBody)
    : shaderHandler("planet/atmosphere", drawnBody)
    , preproc(getTextureConstructor())
    , emissionLUT(GLTexture::Tex2DProperties(size, size, GL_RGBA32F),
                  GLTexture::Sampler(GL_LINEAR, GL_REPEAT, GL_CLAMP_TO_EDGE))
    , absorptionLUT(GLTexture::Tex2DProperties(size, size, GL_RGBA32F),
                    GLTexture::Sampler(GL_LINEAR, GL_REPEAT, GL_CLAMP_TO_EDGE))
    , lutShader("planet/atmosphere_lut", constructDefines())
    , atmRadius(getRadius(drawnBody.getCelestialBodyParameters().radius,
                          *drawnBody.getAtmosphere()))
{
	Primitives::setAsUnitSphere(atmoSphere, shaderHandler.getShader(), 500,
	                            500);
	PlanetShaderHandler::setConstantUniforms(lutShader, drawnBody);

	setPrecomputedTextureData(preproc,
	                          drawnBody.getCelestialBodyParameters().radius,
	                          *drawnBody.getAtmosphere());
}

std::array<GLTexture const*, 3> AtmosphereRenderer::computeLUTs(
    PlanetShaderHandler::EvolvingParameters const& params, float renderRadius,
    int intersectIndex) const
{
	PlanetShaderHandler::update(
	    lutShader, params,
	    QSettings().value("graphics/maxlightcasters").toUInt());

	lutShader.setUniform("renderRadius", renderRadius);
	lutShader.setUniform("intersectIndex", intersectIndex);
	lutShader.setUniform("atmData.preproc", 2);
	GLHandler::glf().glMemoryBarrier(GL_UNIFORM_BARRIER_BIT);

	lutShader.exec({{emissionLUT, GLComputeShader::DataAccessMode::W},
	                {absorptionLUT, GLComputeShader::DataAccessMode::W},
	                {preproc, GLComputeShader::DataAccessMode::SAMPLER}},
	               {size, size, 1});

	return {&preproc, &emissionLUT, &absorptionLUT};
}

void AtmosphereRenderer::renderTransparent(
    PlanetShaderHandler::EvolvingParameters const& params)
{
	// LUT
	computeLUTs(params, atmRadius, 1);

	// RENDER
	shaderHandler.update(params);
	shaderHandler.getShader().setUniform("emissionLUT", 0);
	shaderHandler.getShader().setUniform("absorptionLUT", 1);

	const GLBlendSet glBlend(
	    {GL_ONE, GL_SRC1_COLOR}); // sum with absorbed background
	const GLCullFaceSet glCullFace(GL_FRONT);

	GLHandler::setUpRender(shaderHandler.getShader(),
	                       params.model * params.properRotation);
	GLHandler::useTextures({&emissionLUT, &absorptionLUT});
	atmoSphere.render();
}

float AtmosphereRenderer::getRadius(float planetRadius,
                                    Planet::Atmosphere const& atmosphere)
{
	return planetRadius
	       + (atmosphere.H0R > atmosphere.H0M ? atmosphere.H0R : atmosphere.H0M)
	             * 18.0;
}

GLTexture::Tex2DProperties AtmosphereRenderer::getTextureConstructor()
{
	return {256, 256, GL_RGBA32F};
}

void AtmosphereRenderer::setPrecomputedTextureData(
    GLTexture& tex, float planetRadius, Planet::Atmosphere const& atmosphere)
{
	const GLComputeShader preprocShader("planet/atmosphere_preproc");
	preprocShader.setUniform("planetRadius", planetRadius);
	preprocShader.setUniform("atmData.radius",
	                         getRadius(planetRadius, atmosphere));
	preprocShader.setUniform("atmData.H0",
	                         QVector2D(atmosphere.H0R, atmosphere.H0M));
	preprocShader.setUniform(
	    "atmData.O3layer", QVector2D(atmosphere.O3height, atmosphere.O3width));

	preprocShader.exec({{tex, GLComputeShader::DataAccessMode::W}},
	                   {256, 256, 1});
}
