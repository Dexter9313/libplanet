/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/DetailedPlanetRenderer.hpp"

DetailedPlanetRenderer::DetailedPlanetRenderer(Planet const& drawnPlanet,
                                               QString const& currentSystemDir)
    : drawnBody(drawnPlanet)
    , terrain(drawnPlanet)
    , shadowmap(GLTexture::Tex2DProperties(1, 1))

{
	const QString name(drawnPlanet.getName().c_str());

	QString diffuse(""), normal(""), specular(""), night("");
	QString str(currentSystemDir + "/images/" + name + "/diffuse.jpg");
	if(QFileInfo(str).exists())
	{
		diffuse = str;
	}
	str = currentSystemDir + "/images/" + name + "/diffuse.png";
	if(QFileInfo(str).exists())
	{
		diffuse = str;
	}
	str = currentSystemDir + "/images/" + name + "/normal.jpg";
	if(QFileInfo(str).exists())
	{
		normal = str;
	}
	str = currentSystemDir + "/images/" + name + "/normal.png";
	if(QFileInfo(str).exists())
	{
		normal = str;
	}
	str = currentSystemDir + "/images/" + name + "/specular.jpg";
	if(QFileInfo(str).exists())
	{
		specular = str;
	}
	str = currentSystemDir + "/images/" + name + "/specular.png";
	if(QFileInfo(str).exists())
	{
		specular = str;
	}

	str = currentSystemDir + "/images/" + name + "/night.jpg";
	if(QFileInfo(str).exists())
	{
		night = str;
	}
	str = currentSystemDir + "/images/" + name + "/night.png";
	if(QFileInfo(str).exists())
	{
		night = str;
	}

	shaderHandler = std::make_unique<PlanetShaderHandler>(
	    "planet/planet", drawnBody,
	    normal != ""
	        || drawnPlanet.getPlanetParameters().type
	               == Planet::Type::TERRESTRIAL);
	auto const& shader(shaderHandler->getShader());
	shader.setUniform("diff", 0);
	shader.setUniform("norm", 1);
	shader.setUniform("specular", 2);
	shader.setUniform("night", 3);
	shader.setUniform("ringsData.tex", 4);
	shader.setUniform("atmData.preproc", 5);
	shader.setUniform("atmData.emissionLUT", 6);
	shader.setUniform("atmData.absorptionLUT", 7);
	shader.setUniform("shadowmap0", 8);
	shader.setUniform("shadowmap1", 9);

	if(diffuse != "" || normal != "" || specular != "" || night != "")
	{
		initFromTex(diffuse, normal, specular, night);
		procedural = false;
	}
	else if(drawnPlanet.getPlanetParameters().type == Planet::Type::GASGIANT)
	{
		initGasGiant(
		    drawnPlanet.getPseudoRandomSeed(),
		    Utils::toQt(drawnPlanet.getCelestialBodyParameters().color));
		procedural = true;
	}
	else
	{
		initTerrestrial(
		    drawnPlanet.getPseudoRandomSeed(),
		    Utils::toQt(drawnPlanet.getCelestialBodyParameters().color), 90.f);
		procedural = true;
	}

	QString model("");
	if(QFileInfo(currentSystemDir + "/models/" + name + ".ply").exists())
	{
		model = currentSystemDir + "/models/" + name + ".ply";
	}

	GLMesh mesh;
	Primitives::setAsUnitSphere(mesh, shaderHandler->getShader(), 50, 50);
	asyncMesh = std::make_unique<AsyncMesh>(model, std::move(mesh));

	std::array<unsigned char, 4> data = {255, 255, 255, 255};
	shadowmap.setData({data.data()});
}

void DetailedPlanetRenderer::initTerrestrial(float seed, QColor const& color,
                                             float polarLatitude)
{
	unsigned int texmaxsize(QSettings().value("graphics/texmaxsize").toUInt());
	if(texmaxsize > 3)
	{
		texmaxsize = 3;
	}
	const unsigned int width(texmaxsize * 2048), height(texmaxsize * 1024);

	GLShaderProgram sdiff("planet/gentex/planet",
	                      "planet/gentex/terrestrialdiff");
	sdiff.setUniform("color", color);
	sdiff.setUniform("polarLatitude", polarLatitude);
	sdiff.setUniform("seed", seed);

	GLShaderProgram snorm("planet/gentex/planet",
	                      "planet/gentex/terrestrialnorm");
	snorm.setUniform("seed", seed);

	genTexs.at(0)
	    = std::make_unique<GeneratedTexture>(std::move(sdiff), width, height);
	genTexs.at(1)
	    = std::make_unique<GeneratedTexture>(std::move(snorm), width, height);
	genTexs.at(2) = std::make_unique<GeneratedTexture>(QColor(0, 0, 0));
	genTexs.at(3) = std::make_unique<GeneratedTexture>(QColor(0, 0, 0));
}

void DetailedPlanetRenderer::initGasGiant(float seed, QColor const& color)
{
	unsigned int texmaxsize(QSettings().value("graphics/texmaxsize").toUInt());
	if(texmaxsize > 3)
	{
		texmaxsize = 3;
	}
	const unsigned int width(texmaxsize * 2048), height(texmaxsize * 1024);

	GLShaderProgram sdiff("planet/gentex/planet", "planet/gentex/gasgiantdiff");
	sdiff.setUniform("color", color);
	sdiff.setUniform("bandsIntensity",
	                 DeterministicRandomFloat(seed).getRandomNumber(2));
	sdiff.setUniform("seed", seed);

	genTexs.at(0)
	    = std::make_unique<GeneratedTexture>(std::move(sdiff), width, height);
	genTexs.at(1)
	    = std::make_unique<GeneratedTexture>(QColor(128, 128, 255), false);
	genTexs.at(2) = std::make_unique<GeneratedTexture>(QColor(0, 0, 0));
	genTexs.at(3) = std::make_unique<GeneratedTexture>(QColor(0, 0, 0));
}

void DetailedPlanetRenderer::initFromTex(QString const& diffusePath,
                                         QString const& normalPath,
                                         QString const& specularPath,
                                         QString const& nightPath)
{
	const unsigned int texmaxsize(
	    QSettings().value("graphics/texmaxsize").toUInt());
	const unsigned int width(texmaxsize * 2048), height(texmaxsize * 1024);

	asyncTexs.at(0) = std::make_unique<AsyncTexture>(
	    diffusePath, width, height,
	    Utils::toQt(drawnBody.getCelestialBodyParameters().color));

	asyncTexs.at(1) = std::make_unique<AsyncTexture>(
	    normalPath, width, height, QColor(128, 128, 255), false);

	asyncTexs.at(2) = std::make_unique<AsyncTexture>(specularPath, width,
	                                                 height, QColor(0, 0, 0));
	asyncTexs.at(3) = std::make_unique<AsyncTexture>(nightPath, width, height,
	                                                 QColor(0, 0, 0));
}

GLTexture const& DetailedPlanetRenderer::getNightTexture() const
{
	return procedural ? genTexs.at(3)->getTexture()
	                  : asyncTexs.at(3)->getTexture();
}

void DetailedPlanetRenderer::update(QMatrix4x4 const& properRotation,
                                    QVector3D const& hoststarpos,
                                    QVector3D const& neighborLightCasterPos)
{
	asyncMesh->updateMesh(shaderHandler->getShader());
	if(procedural)
	{
		genTexs.at(0)->render();
		genTexs.at(1)->render();
		genTexs.at(2)->render();
		genTexs.at(3)->render();
	}
	if(light.at(0) != nullptr)
	{
		light.at(0)->setBoundingSphereRadius(
		    asyncMesh->getBoundingSphereRadius());
		light.at(0)->setDirection(
		    utils::transformPosition(properRotation.inverted(),
		                             -1.f * hoststarpos)
		        .normalized());
		light.at(0)->generateShadowMap({&asyncMesh->getMesh()}, {QMatrix4x4()});
	}
	if(light.at(1) != nullptr)
	{
		light.at(1)->setBoundingSphereRadius(
		    asyncMesh->getBoundingSphereRadius());
		light.at(1)->setDirection(
		    utils::transformPosition(properRotation.inverted(),
		                             -1.f * neighborLightCasterPos)
		        .normalized());
		light.at(1)->generateShadowMap({&asyncMesh->getMesh()}, {QMatrix4x4()});
	}
}

void DetailedPlanetRenderer::render(
    PlanetShaderHandler::EvolvingParameters const& params,
    GLTexture const* ringsTex, AtmosphereRenderer const* atmosphereRenderer)
{
	shaderHandler->update(params);

	float albedoCoeff(1.f);
	std::vector<GLTexture const*> textures;
	if(procedural)
	{
		textures = {&genTexs[0]->getTexture(), &genTexs[1]->getTexture(),
		            &genTexs[2]->getTexture(), &genTexs[3]->getTexture()};
	}
	else
	{
		const QColor avgLinear(
		    GLHandler::sRGBToLinear(asyncTexs[0]->getAverageColor()));
		float coeff(avgLinear.redF() + avgLinear.greenF() + avgLinear.blueF());
		coeff /= 3.f;
		albedoCoeff = drawnBody.getPlanetParameters().albedo / coeff;

		textures = {&asyncTexs[0]->getTexture(), &asyncTexs[1]->getTexture(),
		            &asyncTexs[2]->getTexture(), &asyncTexs[3]->getTexture()};
	}
	textures.push_back(ringsTex);
	if(atmosphereRenderer != nullptr)
	{
		auto atmosTexs(atmosphereRenderer->computeLUTs(
		    params, drawnBody.getCelestialBodyParameters().radius, 0));
		textures.push_back(atmosTexs[0]);
		textures.push_back(atmosTexs[1]);
		textures.push_back(atmosTexs[2]);
	}
	else
	{
		textures.push_back(nullptr);
		textures.push_back(nullptr);
		textures.push_back(nullptr);
	}

	if(asyncMesh->isLoaded())
	{
		for(auto& lightptr : light)
		{
			if(lightptr != nullptr)
			{
				continue;
			}
			lightptr                = std::make_unique<Light>();
			lightptr->ambiantFactor = 0.f;
		}
		for(auto& lightptr : light)
		{
			textures.push_back(&lightptr->getShadowMap());
		}

		shaderHandler->getShader().setUniform(
		    "lightspace0", light.at(0)->getTransformation(true));
		shaderHandler->getShader().setUniform(
		    "lightspace1", light.at(1)->getTransformation(true));
		shaderHandler->getShader().setUniform(
		    "boundingSphereRadius", asyncMesh->getBoundingSphereRadius());
		GLHandler::useTextures(textures);
		shaderHandler->getShader().setUniform("planetData.albedoCoeff",
		                                      albedoCoeff);
		shaderHandler->getShader().setUniform("planetData.roughness", 1.f);
		GLHandler::setUpRender(shaderHandler->getShader(),
		                       params.model * params.properRotation);
		asyncMesh->getMesh().render();
	}
	else
	{
		textures.push_back(&shadowmap);
		float roughness(0.0);
		if(!procedural && asyncTexs.at(1)->isLoaded())
		{
			roughness = 1.0;
		}
		terrain.render(params, textures, albedoCoeff, roughness);
	}
}
