/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/TerrainRenderer.hpp"

TerrainRenderer::TerrainRenderer(Planet const& drawnBody)
    : shaderHandler("planet/terrain", drawnBody)
{
	auto const& shader(shaderHandler.getShader());
	shader.setUniform("diff", 0);
	shader.setUniform("norm", 1);
	shader.setUniform("specular", 2);
	shader.setUniform("night", 3);
	shader.setUniform("ringsData.tex", 4);
	shader.setUniform("atmData.preproc", 5);
	shader.setUniform("atmData.emissionLUT", 6);
	shader.setUniform("atmData.absorptionLUT", 7);

	Primitives::setAsGrid(grid, shaderHandler.getShader(), 100,
	                      PrimitiveType::TRIANGLES);
}

void TerrainRenderer::render(
    PlanetShaderHandler::EvolvingParameters const& params,
    std::vector<GLTexture const*> const& texs, float albedoCoeff,
    float roughness)
{
	shaderHandler.getShader().setUniform("planetData.albedoCoeff", albedoCoeff);
	shaderHandler.getShader().setUniform("planetData.roughness", roughness);

	shaderHandler.update(params);
	GLHandler::setUpRender(shaderHandler.getShader(),
	                       params.model * params.properRotation);
	GLHandler::useTextures(texs);
	grid.render();
}
