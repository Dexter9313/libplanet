/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/GeneratedTexture.hpp"

GeneratedTexture::GeneratedTexture(QColor const& color, bool sRGB)
    : isDefault(true)
    , width(8)
    , height(8)
    , framebuffer(GLTexture::Tex2DProperties(width, height, GL_RGBA16F))
{
	GLHandler::setClearColor(sRGB ? GLHandler::sRGBToLinear(color) : color);
	GLHandler::beginRendering(framebuffer);
	GLHandler::setClearColor(QColor(0, 0, 0, 255));
}

GeneratedTexture::GeneratedTexture(GLShaderProgram&& shader, unsigned int width,
                                   unsigned int height)
    : isDefault(false)
    , currentLvl(width / patchsize > height / patchsize ? width / patchsize
                                                        : height / patchsize)
    , width(width)
    , height(height)
    , framebuffer(GLTexture::Tex2DProperties(width / currentLvl,
                                             height / currentLvl, GL_RGBA16F))
{
	this->shader = std::make_unique<GLShaderProgram>(std::move(shader));
	Primitives::setAsQuad(mesh, *this->shader);
}

void GeneratedTexture::render()
{
	if(isDefault || currentLvl == 0)
	{
		return;
	}
	// grid of patches
	const unsigned int gridWidth(width / (currentLvl * patchsize)),
	    gridHeight(height / (currentLvl * patchsize));
	if(progress >= gridWidth * gridHeight)
	{
		currentLvl /= 2;
		if(currentLvl == 0)
		{
			return;
		}
		progress = 0;

		GLFramebufferObject newFramebuffer(GLTexture::Tex2DProperties(
		    width / currentLvl, height / currentLvl, GL_RGBA16F));
		// REQUIRED FOR BLIT TO WORK... SEE WHAT SHOULD BE PART OF RENTER TARGET
		// CREATION MAYBE THE COLOR ATTACHMENT ASSOCIATION...
		GLHandler::beginRendering(newFramebuffer);
		framebuffer.blitColorBufferTo(newFramebuffer);
		framebuffer = std::move(newFramebuffer);
	}

	GLHandler::beginRendering(static_cast<GLbitfield>(0), framebuffer);
	GLHandler::glf().glScissor((progress % gridWidth) * patchsize,
	                           (progress / gridWidth) * patchsize, patchsize,
	                           patchsize);

	const GLStateSet glState({{GL_SCISSOR_TEST, true}});
	shader->use();
	mesh.render(PrimitiveType::TRIANGLE_STRIP);
	framebuffer.getColorAttachmentTexture().generateMipmap();
	++progress;
}

GLTexture const& GeneratedTexture::getTexture()
{
	return framebuffer.getColorAttachmentTexture();
}
