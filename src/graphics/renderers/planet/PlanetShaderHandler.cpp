/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/planet/PlanetShaderHandler.hpp"
#include "graphics/renderers/planet/AtmosphereRenderer.hpp"

QMap<QString, QString>
    PlanetShaderHandler::constructDefines(bool normal,
                                          std::string const& bodyName)
{
	QMap<QString, QString> defines;
	defines['_' + QString(bodyName.c_str()).toUpper().replace(' ', '_')] = "1";
	defines["NUM_IN_SCATTER"] = QString::number(
	    20 + 10 * QSettings().value("graphics/atmoquality").toUInt());
	defines["MAX_LIGHTS"] = QString::number(
	    QSettings().value("graphics/maxlightcasters").toUInt());
	if(normal)
	{
		defines["NORMAL"] = "";
	}
	const unsigned int smoothshadows(
	    QSettings().value("graphics/smoothshadows").toUInt());
	if(smoothshadows > 0)
	{
		defines["SMOOTHSHADOWS"] = QString::number(smoothshadows * 2 + 1);
	}
	return defines;
}

PlanetShaderHandler::PlanetShaderHandler(QString const& vertPath,
                                         QString const& fragPath,
                                         Planet const& drawnBody, bool normal)
    : shader(vertPath, fragPath, constructDefines(normal, drawnBody.getName()))
{
	setConstantUniforms(shader, drawnBody);
}

PlanetShaderHandler::PlanetShaderHandler(QString const& shaderPath,
                                         Planet const& drawnBody, bool normal)
    : PlanetShaderHandler(shaderPath, shaderPath, drawnBody, normal)
{
}

void PlanetShaderHandler::update(EvolvingParameters const& params)
{
	update(shader, params, maxlights);
}

void PlanetShaderHandler::setConstantUniforms(GLShaderProgram const& shader,
                                              Planet const& drawnBody)
{
	// parameters

	const double planetRadius(drawnBody.getCelestialBodyParameters().radius);

	shader.setUniform("planetData.radius", static_cast<float>(planetRadius));
	shader.setUniform(
	    "planetData.oblateness",
	    Utils::toQt(drawnBody.getCelestialBodyParameters().oblateness));

	shader.setUniform(
	    "ringsData.inner",
	    static_cast<float>(drawnBody.getPlanetParameters().innerRing));
	shader.setUniform(
	    "ringsData.outer",
	    static_cast<float>(drawnBody.getPlanetParameters().outerRing));

	// atmosphere

	Planet::Atmosphere const* atmosphere(drawnBody.getAtmosphere());
	if(atmosphere == nullptr)
	{
		return;
	}

	const float H0R       = atmosphere->H0R;
	const float H0M       = atmosphere->H0M;
	const QVector3D betaM = atmosphere->betaM * QVector3D(1.0, 1.0, 1.0);

	shader.setUniform("atmData.radius",
	                  AtmosphereRenderer::getRadius(planetRadius, *atmosphere));
	shader.setUniform("atmData.H0", QVector2D(H0R, H0M));
	shader.setUniform("atmData.O3layer",
	                  QVector2D(atmosphere->O3height, atmosphere->O3width));
	std::array<QVector3D, 3> beta0 = {Utils::toQt(atmosphere->betaR), betaM,
	                                  Utils::toQt(atmosphere->betaO3)};
	shader.setUniform("atmData.beta0", 3, beta0.data());

	shader.setUniform("cloudsHeight", static_cast<float>(atmosphere->clouds));
}

void PlanetShaderHandler::update(GLShaderProgram const& shader,
                                 EvolvingParameters const& params,
                                 unsigned int maxlights)
{
	std::array<QVector4D, 2> lightsPosRadius;

	for(unsigned int i(0); i < lightsPosRadius.size(); ++i)
	{
		auto lightpos(utils::transformPosition(
		    params.properRotation.inverted(),
		    params.lightsPosRadius.at(i).toVector3D()));
		lightsPosRadius.at(i)
		    = QVector4D(lightpos, params.lightsPosRadius.at(i).w());
	}

	auto campos(utils::transformPosition(params.properRotation.inverted(),
	                                     params.campos));

	std::array<QVector4D, 5> neighborsPosRadius;
	for(unsigned int i(0); i < neighborsPosRadius.size(); ++i)
	{
		QVector3D pos(params.neighborsPosRadius.at(i));
		pos = utils::transformPosition(params.properRotation.inverted(), pos);
		neighborsPosRadius.at(i)
		    = QVector4D(pos, params.neighborsPosRadius.at(i).w());
	}

	shader.setUniform("campos", campos);
	shader.setUniform("lightsPosRadius", maxlights, lightsPosRadius.data());
	shader.setUniform("lightsIllum", maxlights, params.lightsIllum.data());
	shader.setUniform("neighData.posRadius", 5, neighborsPosRadius.data());
	shader.setUniform("neighData.oblateness", 5,
	                  params.neighborsOblateness.data());
	shader.setUniform("exposure", params.exposure);
	shader.setUniform("dynamicrange", params.dynamicrange);
}
