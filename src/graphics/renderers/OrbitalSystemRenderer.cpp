/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include "graphics/renderers/OrbitalSystemRenderer.hpp"

#include "graphics/renderers/SpacecraftRenderer.hpp"
#include "graphics/renderers/StarRenderer.hpp"
#include "graphics/renderers/planet/PlanetRenderer.hpp"

bool OrbitalSystemRenderer::autoCameraTarget = true;

OrbitalSystemRenderer::OrbitalSystemRenderer(OrbitalSystem const& drawnSystem)
    : drawnSystem(drawnSystem)
{
	for(auto const* body : drawnSystem.getAllPlanetsPointers())
	{
		addNode(std::make_unique<PlanetRenderer>(*body, nodesDict));
	}
	for(auto const* star : drawnSystem.getAllStarsPointers())
	{
		addNode(std::make_unique<StarRenderer>(*star, nodesDict));
	}
	for(auto const* spacecraft : drawnSystem.getAllSpacecraftsPointers())
	{
		addNode(std::make_unique<SpacecraftRenderer>(*spacecraft, nodesDict));
	}
}

std::vector<Node*> OrbitalSystemRenderer::sortedNodes() const
{
	std::vector<Node*> result;
	result.reserve(sortedRenderers.size());

	auto it(sortedRenderers.end());
	while(it != sortedRenderers.begin())
	{
		--it;
		result.push_back(it->second);
	}

	return result;
}

void OrbitalSystemRenderer::updateNodes(BasicCamera& camera)
{
	auto& cam(dynamic_cast<OrbitalSystemCamera&>(camera));

	sortedRenderers.clear();
	for(auto& node : rootNodes)
	{
		auto* bodyRenderer(dynamic_cast<CelestialBodyRenderer*>(node.get()));
		sortedRenderers[cam.getRelativePositionTo(bodyRenderer->getDrawnBody(),
		                                          uT)
		                    .length()]
		    = bodyRenderer;
	}

	if(autoCameraTarget)
	{
		auto const* closest(&sortedRenderers.begin()->second->getDrawnBody());
		while(cam.getRelativePositionTo(*closest, uT).length()
		          > std::max(closest->getSphereOfInfluenceRadius(),
		                     8.0 * closest->getCelestialBodyParameters().radius)
		      && closest->getParent() != nullptr
		      && closest->getParent()->getOrbitableType()
		             != Orbitable::Type::BINARY)
		{
			closest = dynamic_cast<CelestialBody const*>(closest->getParent());
		}

		if(closest != cam.target)
		{
			cam.relativePosition
			    = -1.0 * cam.getRelativePositionTo(*closest, uT);
			cam.target = closest;
		}
	}

	for(auto& node : rootNodes)
	{
		auto* cbChild = dynamic_cast<CelestialBodyRenderer*>(node.get());
		cbChild->updateMesh(uT, cam);
	}
}
