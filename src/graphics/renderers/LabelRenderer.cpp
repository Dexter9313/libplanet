/*
    Copyright (C) 2020 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/renderers/LabelRenderer.hpp"

bool& LabelRenderer::fontAdded()
{
	static bool fontAdded = false;
	return fontAdded;
}

QString& LabelRenderer::fontFamily()
{
	static QString fontFamily;
	return fontFamily;
}

GLShaderProgram& LabelRenderer::lineShader()
{
	static GLShaderProgram lineShader("default", "tick");
	return lineShader;
}

LabelRenderer::LabelRenderer(QString const& text, QColor const& color,
                             bool renderTick)
    : label(200, 200, GLShaderProgram("billboard", "label"))
    , renderTick(renderTick)
    , numberOfLines(1)
    , color(color)
{
	if(!fontAdded())
	{
		const QString fontPath(
		    QSettings().value("graphics/customfont").toString());
		if(!fontPath.isEmpty())
		{
			auto id      = QFontDatabase::addApplicationFont(fontPath);
			fontFamily() = QFontDatabase::applicationFontFamilies(id)[0];
		}
		fontAdded() = true;
	}

	if(!fontFamily().isEmpty())
	{
		QFont f = label.getFont();
		f.setFamily(fontFamily());
		label.setFont(f);
	}

	numberOfLines += text.count('\n');
	label.setText(text);
	label.setColor(color);
	label.setSuperSampling(2.f);
	label.setFlags(Qt::AlignCenter);

	lineMesh.setVertexShaderMapping(lineShader(), {{"position", 3}});
	lineMesh.setVertices({{0.0f, 0.0f, 0.0f, 0.0f, 0.06f, 0.0f}});
}

void LabelRenderer::updateModel(QMatrix4x4 const& model)
{
	QMatrix4x4 text3DLocalModel;
	if(renderTick)
	{
		text3DLocalModel.translate(0.0f, 0.1f, 0.0f);
	}
	text3DLocalModel.translate(0.0f, 0.06f * (numberOfLines - 1), 0.0f);

	label.getModel() = model * text3DLocalModel;
	lineModel        = model;
}

void LabelRenderer::render(float exposure, float dynamicrange)
{
	label.getShader().setUniform("exposure", exposure);
	label.getShader().setUniform("dynamicrange", dynamicrange);
	label.render();

	if(renderTick)
	{
		const GLStateSet glState({{GL_MULTISAMPLE, true}});
		const GLBlendSet glBlend(GLBlendSet::BlendState{});
		lineShader().setUniform("color", color);
		lineShader().setUniform("alpha", alpha);
		lineShader().setUniform("exposure", exposure);
		lineShader().setUniform("dynamicrange", dynamicrange);
		GLHandler::setUpRender(lineShader(), lineModel);
		lineMesh.render(PrimitiveType::LINES);
	}
}
