#include "graphics/OrbitalSystemCamera.hpp"
#include "graphics/GraphicsUtils.hpp"
#include "graphics/renderers/CelestialBodyRenderer.hpp"

OrbitalSystemCamera::OrbitalSystemCamera(VRHandler const& vrHandler,
                                         float const& exposure,
                                         float const& dynamicrange)
    : BasicCamera(vrHandler)
    , exposure(exposure)
    , dynamicrange(dynamicrange)
{
	lookAt(QVector3D(0.f, 0.f, 0.f), QVector3D(1.f, 0.f, 0.f),
	       QVector3D(0.f, 0.f, 1.f));
}

void OrbitalSystemCamera::setLockedOnRotation(bool value,
                                              UniversalTime const& uT)
{
	if(target->getOrbitableType() == Orbitable::Type::BINARY
	   || value == lockedOnRotation)
	{
		return;
	}
	auto const* tgtBody(dynamic_cast<CelestialBody const*>(target));
	lockedOnRotation = value;
	relativePosition
	    = (lockedOnRotation
	           ? Utils::fromQt(
	                 Utils::toQt(tgtBody->getProperRotationAtUT(uT)).inverted())
	           : tgtBody->getProperRotationAtUT(uT))
	      * relativePosition;
}

void OrbitalSystemCamera::toggleLockedOnRotation(UniversalTime const& uT)
{
	setLockedOnRotation(!lockedOnRotation, uT);
}

double OrbitalSystemCamera::getActualTargetRadiusBelow() const
{
	if(target->getOrbitableType() == Orbitable::Type::BINARY)
	{
		return 0.0;
	}
	auto const* tgtBody(dynamic_cast<CelestialBody const*>(target));
	const double result(tgtBody->getCelestialBodyParameters().radius);

	Vector3 unitRelPos(relativePosition.getUnitForm());
	if(!lockedOnRotation)
	{
		unitRelPos = Utils::fromQt(
		                 Utils::toQt(tgtBody->getProperRotationAtUT(currentUT))
		                     .inverted())
		             * unitRelPos;
	}

	const Vector3 oblateness(tgtBody->getCelestialBodyParameters().oblateness);

	unitRelPos[0] *= oblateness[0];
	unitRelPos[1] *= oblateness[1];
	unitRelPos[2] *= oblateness[2];

	return result * unitRelPos.length();
}

double OrbitalSystemCamera::getAltitude() const
{
	return relativePosition.length() - getActualTargetRadiusBelow();
}

void OrbitalSystemCamera::setAltitude(double altitude)
{
	relativePosition = relativePosition.getUnitForm()
	                   * (getActualTargetRadiusBelow() + altitude);
}

Vector3 OrbitalSystemCamera::getHeadShift() const
{
	QMatrix4x4 eyeViewMatrix;
	if(vrHandler.isEnabled())
	{
		eyeViewMatrix
		    = vrHandler.getEyeViewMatrix(vrHandler.getCurrentRenderingEye());
	}
	return Utils::fromQt(QVector3D(
	           (hmdScaledToWorld * eyeViewMatrix.inverted()).column(3)))
	       / CelestialBodyRenderer::overridenScale();
}

Vector3
    OrbitalSystemCamera::getRelativePositionTo(Orbitable const& orbitable,
                                               UniversalTime const& uT) const
{
	const Vector3 targetRelPosToBody(
	    Orbitable::getRelativePositionAtUt(*target, orbitable, uT));

	Vector3 relPos(relativePosition);
	if(lockedOnRotation
	   && target->getOrbitableType() != Orbitable::Type::BINARY)
	{
		auto const* tgtBody(dynamic_cast<CelestialBody const*>(target));
		relPos = tgtBody->getProperRotationAtUT(uT) * relativePosition;
	}

	return targetRelPosToBody - relPos;
}

void OrbitalSystemCamera::updateUT(UniversalTime const& uT)
{
	currentUT = uT;
	while(yaw < 0.f)
	{
		yaw += 2.f * M_PI;
	}
	while(yaw >= 2.f * M_PI)
	{
		yaw -= 2.f * M_PI;
	}
	if(pitch > M_PI_2 - 0.01)
	{
		pitch = M_PI_2 - 0.01;
	}
	if(pitch < -1.f * M_PI_2 + 0.01)
	{
		pitch = -1.f * M_PI_2 + 0.01;
	}
	/*angleAboveXY = angleAboveXY > 3.1415f / 2.f ? 3.1415f / 2.f :
	angleAboveXY; angleAboveXY = angleAboveXY < -3.1415f / 2.f ? -3.1415f / 2.f
	: angleAboveXY; relativePosition.setXYZ( distance +
	target->getCelestialBodyParameters().radius, 0, 0);
	relativePosition.rotateAlongY(angleAboveXY);
	relativePosition.rotateAlongZ(angleAroundZ);*/

	lookDirection
	    = {-cosf(yaw) * cosf(pitch), -sinf(yaw) * cosf(pitch), sinf(pitch)};

	QVector3D lookDir(lookDirection);
	if(lockedOnRotation
	   && target->getOrbitableType() != Orbitable::Type::BINARY)
	{
		auto const* tgtBody(dynamic_cast<CelestialBody const*>(target));
		lookDir = utils::transformDirection(
		    Utils::toQt(tgtBody->getProperRotationAtUT(uT)), lookDirection);
	}

	QVector3D upDir(up);
	if(lockedOnRotation
	   && target->getOrbitableType() != Orbitable::Type::BINARY)
	{
		auto const* tgtBody(dynamic_cast<CelestialBody const*>(target));
		upDir = utils::transformDirection(
		    Utils::toQt(tgtBody->getProperRotationAtUT(uT)), up);
	}

	lookAt(QVector3D(0.f, 0.f, 0.f), lookDir, up);
}

// http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-testing-points-and-spheres/
bool OrbitalSystemCamera::shouldBeCulled(QVector3D const& /*spherePosition*/,
                                         float /*radius*/)
{
	return false;
	/*
	for(unsigned int i(0); i < 6; ++i)
	{
	    if(QVector4D::dotProduct(clippingPlanes[i],
	                             QVector4D(spherePosition, 1.f))
	       < -radius)
	    {
	        return true;
	    }
	}
	return false;
	*/
}
