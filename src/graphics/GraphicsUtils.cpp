/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "graphics/GraphicsUtils.hpp"

namespace Utils
{
QVector3D toQt(Vector3 const& vector)
{
	return {static_cast<float>(vector[0]), static_cast<float>(vector[1]),
	        static_cast<float>(vector[2])};
}

Vector3 fromQt(QVector3D const& vector)
{
	return {vector.x(), vector.y(), vector.z()};
}

QColor toQt(Color const& color)
{
	return {static_cast<int>(color.r), static_cast<int>(color.g),
	        static_cast<int>(color.b), static_cast<int>(color.alpha)};
}

Color fromQt(QColor const& color)
{
	return {static_cast<unsigned int>(color.alpha()),
	        static_cast<unsigned int>(color.red()),
	        static_cast<unsigned int>(color.green()),
	        static_cast<unsigned int>(color.blue())};
}

QMatrix4x4 toQt(Matrix4x4 const& matrix)
{
	return {static_cast<float>(matrix[0][0]), static_cast<float>(matrix[0][1]),
	        static_cast<float>(matrix[0][2]), static_cast<float>(matrix[0][3]),
	        static_cast<float>(matrix[1][0]), static_cast<float>(matrix[1][1]),
	        static_cast<float>(matrix[1][2]), static_cast<float>(matrix[1][3]),
	        static_cast<float>(matrix[2][0]), static_cast<float>(matrix[2][1]),
	        static_cast<float>(matrix[2][2]), static_cast<float>(matrix[2][3]),
	        static_cast<float>(matrix[3][0]), static_cast<float>(matrix[3][1]),
	        static_cast<float>(matrix[3][2]), static_cast<float>(matrix[3][3])};
}

Matrix4x4 fromQt(QMatrix4x4 const& matrix)
{
	return {{{{matrix.row(0)[0], matrix.row(0)[1], matrix.row(0)[2],
	           matrix.row(0)[3]},
	          {matrix.row(1)[0], matrix.row(1)[1], matrix.row(1)[2],
	           matrix.row(1)[3]},
	          {matrix.row(2)[0], matrix.row(2)[1], matrix.row(2)[2],
	           matrix.row(2)[3]},
	          {matrix.row(3)[0], matrix.row(3)[1], matrix.row(3)[2],
	           matrix.row(3)[3]}}}};
}
} // namespace Utils
