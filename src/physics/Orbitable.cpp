/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/Orbitable.hpp"

#include "physics/CSVOrbit.hpp"
#include "physics/OrbitalSystem.hpp"
#include "physics/Planet.hpp"
#include "physics/Spacecraft.hpp"
#include "physics/Star.hpp"

std::unique_ptr<QProgressDialog>& Orbitable::progress()
{
	static std::unique_ptr<QProgressDialog> progress;
	return progress;
}

float& Orbitable::value()
{
	static float value = 100.f;
	return value;
}

float& Orbitable::current()
{
	static float current = 0.f;
	return current;
}

Orbitable::Orbitable(Type type, QJsonObject const& json,
                     OrbitalSystem const& system)
    : system(system)
    , type(type)
{
	name          = json["name"].toString().toStdString();
	auto dispName = json["displayName"].toString();
	dispName.replace(QString{"\\n"}, QString{'\n'});
	displayName = dispName.toStdString();
	if(displayName.empty())
	{
		displayName = name;
	}
	jsonBack = std::make_unique<QJsonObject>(json);
}

Orbitable::Orbitable(Type type, QJsonObject const& json, Orbitable& parent)
    : system(parent.getSystem())
    , type(type)
    , parent(&parent)
{
	name          = json["name"].toString().toStdString();
	auto dispName = json["displayName"].toString();
	dispName.replace(QString{"\\n"}, QString{'\n'});
	displayName = dispName.toStdString();
	if(displayName.empty())
	{
		displayName = name;
	}
	jsonBack = std::make_unique<QJsonObject>(json);
}

Orbitable::Orbitable(Type type, std::string name, OrbitalSystem const& system,
                     Orbit* orbit)
    : system(system)
    , type(type)
    , name(std::move(name))
    , orbit(orbit)
{
}

Orbitable::Orbitable(Type type, std::string name, Orbitable& parent,
                     Orbit* orbit)
    : system(parent.getSystem())
    , type(type)
    , name(std::move(name))
    , parent(&parent)
    , orbit(orbit)
{
}

void Orbitable::generateBinariesNames()
{
	if(type != Type::BINARY)
	{
		return;
	}

	std::vector<Orbitable*> nonPlanetsChildren;
	nonPlanetsChildren.reserve(children.size());
	for(auto const& child : children)
	{
		nonPlanetsChildren.push_back(child.get());
	}

	for(auto it(nonPlanetsChildren.begin()); it != nonPlanetsChildren.end();)
	{
		if((*it)->type == Type::PLANET)
		{
			it = nonPlanetsChildren.erase(it);
		}
		else
		{
			++it;
		}
	}

	if(nonPlanetsChildren.size() != 2)
	{
		qCritical() << "Binary isn't binary...";
	}

	// generate childrens names first
	for(auto* o : nonPlanetsChildren)
	{
		o->generateBinariesNames();
	}

	std::string name0(nonPlanetsChildren[0]->getName()),
	    name1(nonPlanetsChildren[1]->getName());

	// find first point of divergence
	unsigned int i(0);
	while(name0[i] == name1[i])
	{
		++i;
	}

	if(name0[i] < name1[i])
	{
		setName(name0.substr(0, i) + name0.substr(i, std::string::npos)
		        + name1.substr(i, std::string::npos));
	}
	else
	{
		setName(name0.substr(0, i) + name1.substr(i, std::string::npos)
		        + name0.substr(i, std::string::npos));
	}
}

void Orbitable::generatePlanetsNames()
{
	// if no parent could set our own name
	if(type == Type::PLANET && parent == nullptr)
	{
		setName(getSystem().getName());
	}

	std::vector<Planet*> planetChildren;
	for(auto const& o : children)
	{
		if(o->type == Type::PLANET)
		{
			planetChildren.push_back(dynamic_cast<Planet*>(o.get()));
		}
	}

	// sort by SMA
	std::map<double, Planet*> sorted;
	for(auto* planet : planetChildren)
	{
		if(planet->getName().empty())
		{
			sorted[planet->getOrbit()->getParameters().semiMajorAxis] = planet;
		}
	}
	// set their name by increasing SMA
	char planetLetter(type == Type::PLANET ? 1 : 'b');

	for(auto& pair : sorted)
	{
		pair.second->setName(name + " "
		                     + (type == Type::PLANET
		                            ? std::to_string(planetLetter)
		                            : std::string() + planetLetter));
		++planetLetter;
	}

	for(auto const& c : children)
	{
		c->generatePlanetsNames();
	}
}

void Orbitable::parseChildren()
{
	auto& json(*jsonBack);
	bool createdProgress(false);
	if(progress() == nullptr)
	{
		createdProgress = true;
		progress()      = std::make_unique<QProgressDialog>(
            QObject::tr("Loading ") + getSystem().getName().c_str() + "...",
            QString(), 0, 100);
		value()   = 100.f;
		current() = 0.f;
	}

	if(!json["planets"].toArray().empty()
	   || !json["spacecrafts"].toArray().empty())
	{
		const unsigned int totalSize(json["planets"].toArray().size()
		                             + json["spacecrafts"].toArray().size());
		value() /= totalSize;
		for(auto val : json["planets"].toArray())
		{
			auto p = std::make_unique<Planet>(val.toObject(), *this);
			p->parseChildren();
			children.emplace_back(std::move(p));
		}
		for(auto val : json["spacecrafts"].toArray())
		{
			auto p = std::make_unique<Spacecraft>(val.toObject(), *this);
			p->parseChildren();
			children.emplace_back(std::move(p));
		}
		value() *= totalSize;
	}
	else
	{
		current() += value();
		QCoreApplication::processEvents();
		progress()->setValue(current());
	}

	if(createdProgress)
	{
		progress().reset();
	}

	// binaries are the only orbitables to have non-planet or non-spacecraft
	// children
	if(type != Type::BINARY)
	{
		return;
	}

	for(auto val : json["binaries"].toArray())
	{
		// binaries don't have their own class because they are pure orbitables
		auto b
		    = std::make_unique<Orbitable>(Type::BINARY, val.toObject(), *this);
		b->parseChildren();
		children.emplace_back(std::move(b));
	}

	for(auto val : json["stars"].toArray())
	{
		auto s = std::make_unique<Star>(val.toObject(), *this);
		s->parseChildren();
		children.emplace_back(std::move(s));
	}
}

void Orbitable::parseOrbit()
{
	{
		auto& json(*jsonBack);
		const Orbit::MassiveBodyMass parentMass(
		    parent == nullptr ? 0.0 : parent->getMass());
		if(CSVOrbit::csvExistsFor(name))
		{
			orbit = std::make_unique<CSVOrbit>(
			    parentMass, name, json["infiniteLifeSpan"].toBool(true));
		}
		else if(json.contains("orbit"))
		{
			const auto jsonObt = json["orbit"].toObject();
			if(json["orbit"].toObject().contains("separationArcsec")
			   && !json["orbit"].toObject().contains("separationMeters"))
			{
				const double sepRad(
				    json["orbit"].toObject()["separationArcsec"].toDouble()
				    * 4.8481368111e-6),
				    sysDistMeters(system.getDistanceToEarth() * 3.085678e16);
				jsonObt["separationMeters"]
				    = sysDistMeters
				      * sepRad; // ~= 2.0 * sysDistMeters * tan(sepRad / 2.0);
			}
			orbit = std::make_unique<Orbit>(parentMass, jsonObt);
		}
	}
	jsonBack.reset();
	for(auto const& child : children)
	{
		child->parseOrbit();
	}
}

float Orbitable::getPseudoRandomSeed() const
{
	const QString hash(
	    QCryptographicHash::hash(name.data(), QCryptographicHash::Md5).toHex());

	float seed(0.f);
	for(auto c : hash)
	{
		seed += c.toLatin1();
	}
	return seed;
}

double Orbitable::getMass() const
{
	if(type == Type::BINARY)
	{
		double result(0.0);
		for(auto const& child : children)
		{
			result += child->getMass();
		}
		return result;
	}
	return dynamic_cast<CelestialBody const*>(this)
	    ->getCelestialBodyParameters()
	    .mass;
}

std::vector<Orbitable*> Orbitable::getAllDescendants() const
{
	std::vector<Orbitable*> result;
	for(auto const& child : children)
	{
		result.push_back(child.get());
		std::vector<Orbitable*> childDescendants(child->getAllDescendants());
		result.insert(result.end(), childDescendants.begin(),
		              childDescendants.end());
	}
	return result;
}

// TODO(florian) : optimize (no need to seek stars as planet children for ex)
std::vector<Orbitable*> Orbitable::getAllDescendants(Type type) const
{
	std::vector<Orbitable*> result;
	for(auto const& child : children)
	{
		if(child->type == type)
		{
			result.push_back(child.get());
		}
		std::vector<Orbitable*> childDescendants(
		    child->getAllDescendants(type));
		result.insert(result.end(), childDescendants.begin(),
		              childDescendants.end());
	}
	return result;
}

void Orbitable::addChild(std::unique_ptr<Orbitable>&& child)
{
	children.push_back(std::move(child));
}

std::unique_ptr<Orbitable> Orbitable::removeChild(std::string const& name)
{
	for(unsigned int i(0); i < children.size(); ++i)
	{
		if(children[i]->getName() == name)
		{
			auto ptr = std::move(children[i]);
			children.erase(children.begin() + i);
			return ptr;
		}
	}
	return {};
}

Vector3 Orbitable::getRelativePositionAtUT(UniversalTime const& uT) const
{
	if(orbit == nullptr)
	{
		return {0.0, 0.0, 0.0};
	}

	return orbit->getPositionAtUT(uT);
}

Vector3 Orbitable::getAbsolutePositionAtUT(UniversalTime const& uT) const
{
	Vector3 result(getRelativePositionAtUT(uT));

	if(parent != nullptr)
	{
		// At least works for JPL Horizon Data
		result += parent->getAbsolutePositionAtUT(uT);
		/*result = getAbsolutePositionFromRelative(
		    parent->getAttachedCoordinateSystemAtUT(uT), result);*/
	}

	return result;
}

Vector3 Orbitable::getRelativeVelocityAtUT(UniversalTime const& uT) const
{
	if(orbit == nullptr)
	{
		return {0.0, 0.0, 0.0};
	}

	return orbit->getVelocityAtUT(uT);
}

Vector3 Orbitable::getAbsoluteVelocityAtUT(UniversalTime const& uT) const
{
	Vector3 result(getRelativeVelocityAtUT(uT));
	if(parent != nullptr)
	{
		result += parent->getAbsoluteVelocityAtUT(uT);
	}
	return result;
}

/*
CoordinateSystem
    Orbitable::getAttachedCoordinateSystemAtUT(UniversalTime const& uT) const
{
    return orbit->getRelativeCoordinateSystemAtUT(uT);
}*/

QJsonObject Orbitable::getJSONRepresentation() const
{
	QJsonObject result;

	if(orbit != nullptr && !orbit->isLoadedFromFile())
	{
		result["orbit"] = orbit->getJSONRepresentation();
	}

	result["name"]   = name.c_str();
	QString dispName = displayName.c_str();
	dispName.replace(QString{'\n'}, QString{"\\n"});
	result["displayName"] = dispName;

	if(!children.empty())
	{
		QJsonArray childrenJSONplanets;
		for(auto const& child : children)
		{
			if(child->type == Type::PLANET)
			{
				childrenJSONplanets.push_back(child->getJSONRepresentation());
			}
		}
		result["planets"] = childrenJSONplanets;

		QJsonArray childrenJSONstars;
		for(auto const& child : children)
		{
			if(child->type == Type::PLANET)
			{
				childrenJSONstars.push_back(child->getJSONRepresentation());
			}
		}
		result["stars"] = childrenJSONstars;

		QJsonArray childrenJSONspacecrafts;
		for(auto const& child : children)
		{
			if(child->type == Type::SPACECRAFT)
			{
				childrenJSONspacecrafts.push_back(
				    child->getJSONRepresentation());
			}
		}
		result["spacecrafts"] = childrenJSONspacecrafts;

		QJsonArray childrenJSONbinaries;
		for(auto const& child : children)
		{
			if(child->type == Type::PLANET)
			{
				childrenJSONbinaries.push_back(child->getJSONRepresentation());
			}
		}
		result["binaries"] = childrenJSONbinaries;
	}

	return result;
}

void Orbitable::update(UniversalTime const& uT)
{
	if(orbit == nullptr)
	{
		return;
	}

	orbit->updateParameters(uT);

	const std::string currentPatch(orbit->getCurrentPatch());

	if(currentPatch.empty() || currentPatch == getParent()->getName())
	{
		return;
	}

	for(auto* orb : system.getAllOrbitablesPointers())
	{
		if(orb->getName() == currentPatch)
		{
			changeParent(orb);
			break;
		}
	}
}

void Orbitable::changeParent(Orbitable* newParent)
{
	std::unique_ptr<Orbitable> thisptr;
	if(parent != nullptr)
	{
		thisptr = parent->removeChild(getName());
	}
	else
	{
		thisptr.reset(this);
	}

	parent = newParent;
	if(newParent != nullptr)
	{
		newParent->addChild(std::move(thisptr));
		if(parent->getOrbitableType() != Type::BINARY)
		{
			auto const* p = dynamic_cast<CelestialBody const*>(parent);
			orbit->setMassiveBodyMass(
			    Orbit::MassiveBodyMass(p->getCelestialBodyParameters().mass));
		}
	}
}

Orbitable const& Orbitable::getCommonAncestor(Orbitable const& orb0,
                                              Orbitable const& orb1)
{
	if(&orb0 == &orb1)
	{
		return orb0;
	}

	unsigned int depth0(0), depth1(0);
	Orbitable const* parent(orb0.getParent());
	while(parent != nullptr)
	{
		++depth0;
		parent = parent->getParent();
	}
	parent = orb1.getParent();
	while(parent != nullptr)
	{
		++depth1;
		parent = parent->getParent();
	}

	auto orb0ptr(&orb0), orb1ptr(&orb1);
	while(depth0 > depth1)
	{
		--depth0;
		orb0ptr = orb0ptr->getParent();
	}
	while(depth1 > depth0)
	{
		--depth1;
		orb1ptr = orb1ptr->getParent();
	}
	while(orb0ptr != orb1ptr)
	{
		orb0ptr = orb0ptr->getParent();
		orb1ptr = orb1ptr->getParent();
	}
	return *orb0ptr;
}

Vector3 Orbitable::getRelativePositionAtUt(Orbitable const& from,
                                           Orbitable const& to,
                                           UniversalTime const& uT)
{
	if(&from == &to)
	{
		return {0.0, 0.0, 0.0};
	}
	if(from.getParent() == to.getParent())
	{
		return to.getRelativePositionAtUT(uT)
		       - from.getRelativePositionAtUT(uT);
	}
	if(from.getParent() == &to)
	{
		return -1.0 * from.getRelativePositionAtUT(uT);
	}
	if(from.getParent() != nullptr && from.getParent()->getParent() == &to)
	{
		return -1.0 * from.getParent()->getRelativePositionAtUT(uT)
		       - from.getRelativePositionAtUT(uT);
	}
	if(to.getParent() == &from)
	{
		return to.getRelativePositionAtUT(uT);
	}

	auto const& commonAncestor(getCommonAncestor(from, to));

	Vector3 result;
	Orbitable const* current(&from);
	while(current != &commonAncestor)
	{
		result -= current->getRelativePositionAtUT(uT);
		current = current->getParent();
	}
	current = &to;
	while(current != &commonAncestor)
	{
		result += current->getRelativePositionAtUT(uT);
		current = current->getParent();
	}
	return result;
}

Vector3 Orbitable::getRelativeVelocityAtUt(Orbitable const& of,
                                           Orbitable const& relativeTo,
                                           UniversalTime const& uT)
{
	if(&of == &relativeTo)
	{
		return {0.0, 0.0, 0.0};
	}
	if(of.getParent() == relativeTo.getParent())
	{
		return of.getRelativeVelocityAtUT(uT)
		       - relativeTo.getRelativeVelocityAtUT(uT);
	}
	if(of.getParent() == &relativeTo)
	{
		return of.getRelativeVelocityAtUT(uT);
	}
	if(of.getParent() != nullptr && of.getParent()->getParent() == &relativeTo)
	{
		return of.getParent()->getRelativeVelocityAtUT(uT)
		       + of.getRelativeVelocityAtUT(uT);
	}
	if(&of == relativeTo.getParent())
	{
		return -1.0 * relativeTo.getRelativeVelocityAtUT(uT);
	}

	auto const& commonAncestor(getCommonAncestor(of, relativeTo));

	Vector3 result;
	Orbitable const* current(&of);
	while(current != &commonAncestor)
	{
		result += current->getRelativeVelocityAtUT(uT);
		current = current->getParent();
	}
	current = &relativeTo;
	while(current != &commonAncestor)
	{
		result -= current->getRelativeVelocityAtUT(uT);
		current = current->getParent();
	}
	return result;
}
