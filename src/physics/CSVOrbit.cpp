/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/CSVOrbit.hpp"

#include "math/constants.hpp"
#include "physics/SimulationTime.hpp"

#include <numbers>

QString& CSVOrbit::currentSystemDir()
{
	static QString currentSystemDir;
	return currentSystemDir;
}

CSVOrbit::CSVOrbit(MassiveBodyMass const& massiveBodyMass,
                   std::string const& bodyName, bool infiniteLifeSpan)
    : Orbit(MassiveBodyMass(massiveBodyMass),
            Parameters({0.0, 0.0, 0.0, 0.0, 1.0, 0.0}))
    , bodyName(bodyName)
    , infiniteLifeSpan(infiniteLifeSpan)
{
	if(bodyName.empty())
	{
		qCritical() << "Trying to load CSV orbit files from an empty name...";
		return;
	}

	dirPath = currentSystemDir() + "/orbital-params/";
	dirPath += bodyName.c_str();

	const QDir csvdir(dirPath);
	if(!csvdir.exists())
	{
		qCritical() << "No CSV orbital params directory related to "
		            << bodyName.c_str();
		return;
	}
	const QStringList csvfilesPaths
	    = csvdir.entryList(QStringList() << "*.csv", QDir::Files);
	for(auto const& csvfilePath : csvfilesPaths)
	{
		QStringList splittedPath(csvfilePath.split('_'));
		const double utMin(splittedPath[splittedPath.size() - 2].toDouble());
		const double utMax(
		    splittedPath[splittedPath.size() - 1].split('.')[0].toDouble());

		csvfiles[utMin] = std::tuple<UniversalTime, QString, bool>(
		    utMax, csvfilePath, false);

		if(utMin < minLoadable)
		{
			minLoadable = utMin;
		}
		if(utMax > maxLoadable)
		{
			maxLoadable = utMax;
		}
	}

	updateParametersNonVirt(SimulationTime::dateTimeToUT(
	    QSettings().value("simulation/starttime").value<QDateTime>()));
}

bool CSVOrbit::isInRange(UniversalTime const& uT) const
{
	return uT >= minLoadable && (uT <= maxLoadable || infiniteLifeSpan);
}

void CSVOrbit::loadFile(QString const& fileName)
{
	QFile file(fileName);
	file.open(QIODevice::ReadOnly);
	file.readLine(); // discard header

	if(file.atEnd())
	{
		return;
	}

	QString firstLine(file.readLine());
	QString parentName("");

	if(firstLine[0] == '#')
	{
		parentName = firstLine.remove(0, 1).simplified();
		firstLine  = file.readLine();
	}

	QStringList entry = firstLine.simplified().split(',');

	UniversalTime u = entry[0].toDouble();
	minLoaded       = u;

	if(!parentName.isEmpty())
	{
		patches[u] = std::string(parentName.toLatin1().data());

		overrides[u] = Parameters({
		    entry[1].toDouble(),
		    entry[2].toDouble(),
		    entry[3].toDouble(),
		    entry[4].toDouble(),
		    entry[5].toDouble(),
		    entry[6].toDouble(),
		});
	}
	else
	{
		parametersHistory[u] = Parameters({
		    entry[1].toDouble(),
		    entry[2].toDouble(),
		    entry[3].toDouble(),
		    entry[4].toDouble(),
		    entry[5].toDouble(),
		    entry[6].toDouble(),
		});
	}

	while(!file.atEnd())
	{
		entry = QString(file.readLine()).simplified().split(',');

		parametersHistory[entry[0].toDouble()] = Parameters({
		    entry[1].toDouble(),
		    entry[2].toDouble(),
		    entry[3].toDouble(),
		    entry[4].toDouble(),
		    entry[5].toDouble(),
		    entry[6].toDouble(),
		});
	}

	u         = entry[0].toDouble();
	maxLoaded = u;
}

void CSVOrbit::loadFromUT(UniversalTime const& uT)
{
	if(uT >= minLoaded && uT <= maxLoaded)
	{
		return;
	}
	if((uT < minLoadable || uT > maxLoadable) && !parametersHistory.empty())
	{
		return;
	}

	// find both surrounding files
	auto it1(--csvfiles.upper_bound(uT)), it2(csvfiles.upper_bound(uT));

	// take the second one if first one is invalid or :
	// - first one ends before today
	// - and second one is valid
	// - and we already loaded the first one before
	auto it(it1);
	if(it2 == csvfiles.begin()
	   || (std::get<0>(it1->second) <= uT && it2 != csvfiles.end()
	       && std::get<2>(it1->second)))
	{
		it = it2;
	}

	// if file is not loaded, load it
	if(!std::get<2>(it->second))
	{
		loadFile(dirPath + "/" + std::get<1>(it->second));
		std::get<2>(it->second) = true;
	}
}

void CSVOrbit::updateParameters(UniversalTime const& uT)
{
	updateParametersNonVirt(uT);
}

void CSVOrbit::updateParametersNonVirt(UniversalTime const& uT)
{
	if(uT == cacheUT)
	{
		return;
	}
	cacheUT = uT;
	loadFromUT(uT);

	// extrapolate if infiniteLifeSpan and beyond last loadable orbit
	if(uT > maxLoadable && infiniteLifeSpan)
	{
		// start from last orbit
		updateParametersNonVirt(maxLoadable);
		// convert mean anomaly to mean anomaly at epoch
		convertMeanAnToMeanAnAtEpoch(maxLoadable);
		// compute current mean anomaly, analytical way
		parameters.meanAnomalyAtEpoch = Orbit::getMeanAnomalyAtUT(uT);
		return;
	}

	if(!patches.empty())
	{
		auto it1(--patches.upper_bound(uT)), it2(patches.upper_bound(uT));

		if(it2 != patches.begin())
		{
			currentPatch = it1->second;
		}
		else
		{
			currentPatch = it2->second;
		}
	}
	auto it1(--parametersHistory.upper_bound(uT)),
	    it2(parametersHistory.upper_bound(uT));

	if(it2 == parametersHistory.begin())
	{
		parameters = it2->second;
		updatePeriod();
	}
	else if(it2 == parametersHistory.end())
	{
		auto it(overrides.find(it1->first));
		if(it != overrides.end())
		{
			it1 = it;
		}

		parameters = it1->second;
		updatePeriod();
	}
	else
	{
		auto it(overrides.find(it1->first));
		if(it != overrides.end())
		{
			it1 = it;
		}

		const UniversalTime beg(it1->first), end(it2->first);
		const UniversalTime intervalUT(end - beg);
		const double interval(intervalUT);
		const UniversalTime fracUT((uT - beg) / interval);
		const double frac(fracUT);
		const Parameters p1(it1->second), p2(it2->second);

		// test if semi-major axis change is not too drastic (rel diff < 3%)
		// if ok, interpolate elements
		const double relDiff(
		    fabs((p2.semiMajorAxis - p1.semiMajorAxis) / p2.semiMajorAxis));
		if(relDiff < 0.03)
		{
			parameters.inclination
			    = interpolateAngle(p1.inclination, p2.inclination, frac);
			parameters.ascendingNodeLongitude = interpolateAngle(
			    p1.ascendingNodeLongitude, p2.ascendingNodeLongitude, frac);

			// Tethys has an orbit pretty much impossible to interpolate :
			// eccentricity is so low that periapsisArgument moves Very fast
			// and meanAnomaly behavior is then changed
			if(bodyName == "Tethys")
			{
				parameters.periapsisArgument = interpolateAngleAlwaysForward(
				    p1.periapsisArgument, p2.periapsisArgument, frac);
			}
			else
			{
				parameters.periapsisArgument = interpolateAngle(
				    p1.periapsisArgument, p2.periapsisArgument, frac);
			}

			parameters.eccentricity
			    = (1.0 - frac) * p1.eccentricity + frac * p2.eccentricity;
			parameters.semiMajorAxis
			    = (1.0 - frac) * p1.semiMajorAxis + frac * p2.semiMajorAxis;

			updatePeriod();

			if(2.0 * interval < getPeriod() || bodyName == "Tethys")
			{
				parameters.meanAnomalyAtEpoch = interpolateAngle(
				    p1.meanAnomalyAtEpoch, p2.meanAnomalyAtEpoch, frac);
			}
			else
			{
				parameters.meanAnomalyAtEpoch = interpolateAngleAlwaysForward(
				    p1.meanAnomalyAtEpoch, p2.meanAnomalyAtEpoch, frac);
			}

			if(getPeriod() < interval)
			{
				parameters.meanAnomalyAtEpoch
				    = p1.meanAnomalyAtEpoch
				      + 2.0 * constant::pi * (frac * interval) / getPeriod();

				parameters.meanAnomalyAtEpoch
				    -= floor(parameters.meanAnomalyAtEpoch
				             / (2.0 * constant::pi))
				       * 2.0 * constant::pi;

				double MAatEnd(p2.meanAnomalyAtEpoch);
				const double MAcomputed(p1.meanAnomalyAtEpoch
				                        + 2.0 * constant::pi * interval
				                              / getPeriod());

				// while MAatEnd < MAcomputed, add 2*pi
				if(MAatEnd < MAcomputed)
				{
					MAatEnd
					    += ceil((MAcomputed - MAatEnd) / (2.0 * constant::pi))
					       * 2.0 * constant::pi;
				}

				double error(MAatEnd - MAcomputed);
				error
				    -= round(error / (2.0 * constant::pi)) * 2.0 * constant::pi;

				parameters.meanAnomalyAtEpoch += frac * error;
				parameters.meanAnomalyAtEpoch
				    -= floor(parameters.meanAnomalyAtEpoch
				             / (2.0 * constant::pi))
				       * 2.0 * constant::pi;
			}
		}
		// if switching between elliptic and hyperbolic,
		// interpolate between pos/vel at uT1 and pos/vel at uT2
		// assuming linear acceleration
		else
		{
			Orbit obt0(MassiveBodyMass(getMassiveBodyMass()), p1),
			    obtf(MassiveBodyMass(getMassiveBodyMass()), p2);
			obt0.convertMeanAnToMeanAnAtEpoch(beg);
			obtf.convertMeanAnToMeanAnAtEpoch(end);

			const Vector3 x0(obt0.getPositionAtUT(beg)),
			    xf(obtf.getPositionAtUT(end)), v0(obt0.getVelocityAtUT(beg)),
			    vf(obtf.getVelocityAtUT(end));

			const Vector3 a0(obt0.getAccelerationAtUT(beg)),
			    af(obtf.getAccelerationAtUT(end));
			/* LINEAR ACCELERATION

			   t0 = 0, tf := interval
			   dotdotx = a*t       + b
			   dotx    = a*t^2 / 2 + b*t       + v0
			   x       = a*t^3 / 6 + b*t^2 / 2 + v0 * t + x0

			   for t=tf :
			   a*tf^3 / 6 + b*tf^2 / 2 = xf - x0 - v0*tf = C
			   a*tf^2 / 2 + b*tf       = vf - v0         = D

			   sol :
			   a = (-12*C + 6*D*tf)/tf^3
			   b = (6 * C - 2*D*tf)/tf^2

			   CUBIC ACCELERATION

			   t0 = 0, tf := interval
			   dotdotx = a*t^3      + b*t^2      + c*t       + a0
			   dotx    = a*t^4 / 4  + b*t^3 / 3  + c*t^2 / 2 + a0*t       + v0
			   x =
			    a*t^5 / 20 + b*t^4 / 12 + c*t^3 / 6 + a0*t^2 / 2 + v0*t + x0

			   for t=tf :
			   a*tf^5 / 20 + b*tf^4 / 12 + c*tf^3 / 6
			   = xf - x0 - v0*tf - a0*tf^2 / 2        = D

			   a*tf^4 / 4  + b*tf^3 / 3  + c*tf^2 / 2
			   = vf - v0 - a0*tf                      = E

			   a*tf^3      + b*tf^2      + c*tf
			   = af - a0                              = F

			   sol :
			   a = (120*D  - 60*E*tf + 10*F*tf^2) / tf^5
			   b = (-180*D + 84*E*tf - 12*F*tf^2) / tf^4
			   c = (60*D   - 24*E*tf + 3*F*tf^2)  / tf^3
			*/

			const double tf(interval);

			/* LIN
			Vector3 C(xf - x0 - v0 * tf);
			Vector3 D(vf - v0);
			Vector3 a((-12 * C + 6 * D * tf)
			          / (tf * tf * tf));
			Vector3 b((6 * C - 2 * D * tf) / (tf * tf));
			*/

			/* CUBIC*/
			const Vector3 D(xf - x0 - v0 * tf - a0 * tf * tf / 2.0);
			const Vector3 E(vf - v0 - a0 * tf);
			const Vector3 F(af - a0);

			const Vector3 a((120 * D - 60 * E * tf + 10 * F * tf * tf)
			                / (tf * tf * tf * tf * tf));
			const Vector3 b((-180 * D + 84 * E * tf - 12 * F * tf * tf)
			                / (tf * tf * tf * tf));
			const Vector3 c((60 * D - 24 * E * tf + 3 * F * tf * tf)
			                / (tf * tf * tf));
			/**/

			const double t(uT - beg);

			/* LIN
			Vector3 x(a * t * t * t / 6.0 + b * t * t / 2.0 + v0 * t + x0),
			    v(a * t * t / 2.0 + b * t + v0);
			*/

			/* CUBIC*/
			Vector3 x(x0);
			Vector3 v(v0);
			double tPowered(t); // t^1
			x += tPowered * v0;
			v += tPowered * a0;
			tPowered *= t; // t^2
			x += tPowered * a0 / 2.0;
			v += tPowered * c / 2.0;
			tPowered *= t; // t^3
			x += tPowered * c / 6.0;
			v += tPowered * b / 3.0;
			tPowered *= t; // t^4
			x += tPowered * (t * a / 20.0 + b / 12.0);
			v += tPowered * a / 4.0;
			/**/

			Orbit obt(MassiveBodyMass(getMassiveBodyMass()), x, v, beg + t);
			parameters                    = obt.getParameters();
			parameters.meanAnomalyAtEpoch = obt.getMeanAnomalyAtUT(beg + t);
			updatePeriod();
		}
	}
}

bool CSVOrbit::csvExistsFor(std::string const& name)
{
	if(name.empty())
	{
		return false;
	}

	QString path(currentSystemDir() + "/orbital-params/");
	path += name.c_str();

	const QDir csvdir(path);
	return csvdir.exists();
}

double CSVOrbit::interpolateAngle(double before, double after, double frac)
{
	while(fabs(after - before) > std::numbers::pi)
	{
		if(after < before)
		{
			after += 2.0 * constant::pi;
		}
		else
		{
			before += 2.0 * constant::pi;
		}
	}

	return (1.0 - frac) * before + frac * after;
}

double CSVOrbit::interpolateAngleAlwaysForward(double before, double after,
                                               double frac)
{
	if(after < before)
	{
		after += 2.0 * constant::pi;
	}

	return (1.0 - frac) * before + frac * after;
}
