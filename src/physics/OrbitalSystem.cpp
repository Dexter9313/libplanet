/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../../include/physics/OrbitalSystem.hpp"

#include <ranges>

OrbitalSystem::OrbitalSystem(std::string name)
    : name(std::move(name))
{
}

OrbitalSystem::OrbitalSystem(std::string name, QJsonObject const& json)
    : name(std::move(name))
{
	declinationTilt = json["declinationTilt"].toDouble();
	distanceToEarth = json["distance"].toDouble(-1);
	rightAscension  = json["rightascension"].toDouble();
	declination     = json["declination"].toDouble();
	if(json.contains("binaries"))
	{
		if(json.contains("stars") || json.contains("planets")
		   || json.contains("spacecrafts"))
		{
			qWarning() << "WARNING: Ill-formed JSON : Several root orbitable "
			              "types declared. Taking binary as root...";
		}

		QJsonArray binArray(json["binaries"].toArray());
		if(binArray.size() > 1)
		{
			qWarning()
			    << "WARNING: Ill-formed JSON : Several binaries declared "
			       "as root orbitable. Taking first binary as root...";
		}
		else if(binArray.empty())
		{
			qCritical()
			    << "ERROR: Ill-formed JSON : Root \"binaries\" array is empty.";
		}
		const QJsonObject jsonBin(binArray[0].toObject());
		rootOrbitable = std::make_unique<Orbitable>(Orbitable::Type::BINARY,
		                                            jsonBin, *this);
		rootOrbitable->parseChildren();
	}
	else if(json.contains("stars"))
	{
		if(json.contains("planets"))
		{
			qWarning() << "WARNING: Ill-formed JSON : Several root orbitable "
			              "types declared. Taking star as root...";
		}

		QJsonArray starArray(json["stars"].toArray());
		if(starArray.size() > 1)
		{
			qWarning() << "WARNING: Ill-formed JSON : Several stars declared "
			              "as root orbitable. Taking first star as root...";
		}
		else if(starArray.empty())
		{
			qCritical()
			    << "ERROR: Ill-formed JSON : Root \"stars\" array is empty.";
		}
		const QJsonObject jsonStar(starArray[0].toObject());
		rootOrbitable = std::make_unique<Star>(jsonStar, *this);
		rootOrbitable->parseChildren();
	}
	else if(json.contains("planets"))
	{
		QJsonArray planetArray(json["planets"].toArray());
		if(planetArray.size() > 1)
		{
			qWarning() << "WARNING: Ill-formed JSON : Several planets declared "
			              "as root orbitable. Taking first planet as root...";
		}
		else if(planetArray.empty())
		{
			qCritical()
			    << "ERROR: Ill-formed JSON : Root \"planets\" array is empty.";
		}
		const QJsonObject jsonPlanet(planetArray[0].toObject());
		rootOrbitable = std::make_unique<Planet>(jsonPlanet, *this);
		rootOrbitable->parseChildren();
	}
	else
	{
		qCritical() << "ERROR: Ill-formed JSON : System is empty or doesn't "
		               "contain a root binary, star or planet (can't find keys "
		               "\"binaries\", \"stars\" or \"planets\" at root level).";
	}
	rootOrbitable->parseOrbit();
	// generate names for orbitables that don't have any
	generateStarsNames();
	rootOrbitable->generateBinariesNames();
	rootOrbitable->generatePlanetsNames();
	indexNewOrbitable(*rootOrbitable);

	for(auto* orbitable : rootOrbitable->getAllDescendants())
	{
		indexNewOrbitable(*orbitable);
	}
}

OrbitalSystem::OrbitalSystem(std::string name,
                             std::unique_ptr<Orbitable>&& rootOrbitable,
                             double declinationTilt)
    : rootOrbitable(std::move(rootOrbitable))
    , declinationTilt(declinationTilt)
    , name(std::move(name))
{
	indexNewOrbitable(*this->rootOrbitable);
	for(auto* orbitable : this->rootOrbitable->getAllDescendants())
	{
		indexNewOrbitable(*orbitable);
	}
}

bool OrbitalSystem::isValid() const
{
	for(auto const* orbitable : getAllOrbitablesPointers())
	{
		if(!orbitable->isValid())
		{
			return false;
		}
	}
	return distanceToEarth >= 0.0;
}

void OrbitalSystem::generateStarsNames()
{
	if(rootOrbitable->getOrbitableType() == Orbitable::Type::STAR)
	{
		rootOrbitable->setName(name);
		return;
	}

	const std::vector<Orbitable*> stars(
	    rootOrbitable->getAllDescendants(Orbitable::Type::STAR));

	// sort by mass
	std::vector<std::pair<double, Orbitable*>> sorted;
	sorted.reserve(stars.size());

	// populate
	for(auto* star : stars)
	{
		sorted.emplace_back(
		    dynamic_cast<Star*>(star)->getCelestialBodyParameters().mass, star);
	}
	// sort by decreasing mass
	std::ranges::sort(sorted, std::greater{},
	                  [](auto const& elem) { return elem.first; });

	// set their name by decreasing mass
	char starLetter('A');
	for(auto& [_, orbitable] : sorted)
	{
		orbitable->setName(name + " " + starLetter);
		++starLetter;
	}
}

CelestialBody* OrbitalSystem::getMainCelestialBody() const
{
	CelestialBody* mainBody = nullptr;
	for(auto* star : getAllStarsPointers())
	{
		if(star->getAbsolutePositionAtUT(0.0).length() == 0.0)
		{
			mainBody = star;
			break;
		}
	}
	// if no star, rogue planet
	if(mainBody == nullptr)
	{
		mainBody = getAllPlanetsPointers()[0];
	}

	return mainBody;
}

std::pair<double, double>
    OrbitalSystem::getRADecFromCarthesian(Vector3 dir) const
{
	std::pair<double, double> result;

	dir.rotateAlongX(declinationTilt);

	result.first  = atan2(dir[1], dir[0]);
	result.second = asin(dir[2]);

	return result;
}

void OrbitalSystem::addChild(std::unique_ptr<Orbitable>&& child,
                             std::string const& parent)
{
	auto* childptr = child.get();
	if(parent.empty())
	{
		rootOrbitable->addChild(std::move(child));
	}
	else
	{
		orbitables[parent]->addChild(std::move(child));
	}

	indexNewOrbitable(*childptr);
}

Orbitable* OrbitalSystem::operator[](std::string const& name)
{
	return orbitables[name];
}

Orbitable const* OrbitalSystem::operator[](std::string const& name) const
{
	return orbitables.at(name);
}

std::vector<std::string> OrbitalSystem::getAllOrbitablesNames() const
{
	std::vector<std::string> result;
	result.reserve(orbitables.size());

	for(auto const& pair : orbitables)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Orbitable*> OrbitalSystem::getAllOrbitablesPointers() const
{
	std::vector<Orbitable*> result;
	result.reserve(orbitables.size());

	for(auto const& pair : orbitables)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllBinariesNames() const
{
	std::vector<std::string> result;
	result.reserve(binaries.size());

	for(auto const& pair : binaries)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Orbitable*> OrbitalSystem::getAllBinariesPointers() const
{
	std::vector<Orbitable*> result;
	result.reserve(binaries.size());

	for(auto const& pair : binaries)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllCelestialBodiesNames() const
{
	std::vector<std::string> result;
	result.reserve(celestialBodies.size());

	for(auto const& pair : celestialBodies)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<CelestialBody*> OrbitalSystem::getAllCelestialBodiesPointers() const
{
	std::vector<CelestialBody*> result;
	result.reserve(celestialBodies.size());

	for(auto const& pair : celestialBodies)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllStarsNames() const
{
	std::vector<std::string> result;
	result.reserve(stars.size());

	for(auto const& pair : stars)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Star*> OrbitalSystem::getAllStarsPointers() const
{
	std::vector<Star*> result;
	result.reserve(stars.size());

	for(auto const& pair : stars)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllSpacecraftsNames() const
{
	std::vector<std::string> result;
	result.reserve(spacecrafts.size());

	for(auto const& pair : spacecrafts)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Spacecraft*> OrbitalSystem::getAllSpacecraftsPointers() const
{
	std::vector<Spacecraft*> result;
	result.reserve(spacecrafts.size());

	for(auto const& pair : spacecrafts)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllPlanetsNames() const
{
	std::vector<std::string> result;
	result.reserve(planets.size());

	for(auto const& pair : planets)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Planet*> OrbitalSystem::getAllPlanetsPointers() const
{
	std::vector<Planet*> result;
	result.reserve(planets.size());

	for(auto const& pair : planets)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllFirstClassPlanetsNames() const
{
	std::vector<std::string> result;
	result.reserve(firstClassPlanets.size());

	for(auto const& pair : firstClassPlanets)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Planet*> OrbitalSystem::getAllFirstClassPlanetsPointers() const
{
	std::vector<Planet*> result;
	result.reserve(firstClassPlanets.size());

	for(auto const& pair : firstClassPlanets)
	{
		result.push_back(pair.second);
	}

	return result;
}

std::vector<std::string> OrbitalSystem::getAllSatellitePlanetsNames() const
{
	std::vector<std::string> result;
	result.reserve(satellitePlanets.size());

	for(auto const& pair : satellitePlanets)
	{
		result.push_back(pair.first);
	}

	return result;
}

std::vector<Planet*> OrbitalSystem::getAllSatellitePlanetsPointers() const
{
	std::vector<Planet*> result;
	result.reserve(satellitePlanets.size());

	for(auto const& pair : satellitePlanets)
	{
		result.push_back(pair.second);
	}

	return result;
}

QJsonObject OrbitalSystem::getJSONRepresentation() const
{
	QJsonObject result;
	result["declinationTilt"] = declinationTilt;

	QJsonArray arr;
	arr.push_back(rootOrbitable->getJSONRepresentation());

	if(rootOrbitable->getOrbitableType() == Orbitable::Type::BINARY)
	{
		result["binaries"] = arr;
	}
	else if(rootOrbitable->getOrbitableType() == Orbitable::Type::STAR)
	{
		result["stars"] = arr;
	}
	else
	{
		result["planets"] = arr;
	}

	return result;
}

void OrbitalSystem::update(UniversalTime const& uT)
{
	for(auto const& pair : orbitables)
	{
		auto* orb = pair.second;
		orb->update(uT);
	}
}

void OrbitalSystem::indexNewOrbitable(Orbitable& orbitable)
{
	orbitables[orbitable.getName()] = &orbitable;
	if(orbitable.getOrbitableType() == Orbitable::Type::BINARY)
	{
		binaries[orbitable.getName()] = &orbitable;
	}
	else
	{
		auto& celestialBody(dynamic_cast<CelestialBody&>(orbitable));
		celestialBodies[orbitable.getName()] = &celestialBody;
		if(celestialBody.getCelestialBodyType() == CelestialBody::Type::STAR)
		{
			auto& star(dynamic_cast<Star&>(celestialBody));
			stars[star.getName()] = &star;
		}
		else if(celestialBody.getCelestialBodyType()
		        == CelestialBody::Type::PLANET)
		{
			auto& planet(dynamic_cast<Planet&>(celestialBody));
			planets[planet.getName()] = &planet;
			if(planet.getParent() == nullptr
			   || planet.getParent()->getOrbitableType()
			          != Orbitable::Type::PLANET)
			{
				firstClassPlanets[planet.getName()] = &planet;
			}
			else
			{
				satellitePlanets[planet.getName()] = &planet;
			}
		}
		else
		{
			auto& spacecraft(dynamic_cast<Spacecraft&>(celestialBody));
			spacecrafts[spacecraft.getName()] = &spacecraft;
		}
	}
}
