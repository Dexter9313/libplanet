/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/Star.hpp"

#include "physics/OrbitalSystem.hpp"
#include "physics/blackbody.hpp"

Star::Star(QJsonObject const& json, OrbitalSystem const& system)
    : CelestialBody(CelestialBody::Type::STAR, json, system)
{
	params.temperature = json["temperature"].toDouble(
	    computeTemperatureFromMass(getCelestialBodyParameters().mass));
	if(!json.contains("mass"))
	{
		if(params.temperature != 0.0)
		{
			CelestialBody::parameters.mass
			    = computeMassFromTemperature(params.temperature);
		}
	}
	if(json.contains("magV"))
	{
		params.absmag = computeAbsMagFromAppMag(json["magV"].toDouble());
	}
	else
	{
		params.absmag = computeAbsMagFromTempAndRadius(params.temperature,
		                                               parameters.radius);
	}
	parameters.color = computeColor();
}

Star::Star(QJsonObject const& json, Orbitable& parent)
    : CelestialBody(CelestialBody::Type::STAR, json, parent)
{
	params.temperature = json["temperature"].toDouble(
	    computeTemperatureFromMass(getCelestialBodyParameters().mass));
	if(json.contains("magV"))
	{
		params.absmag = computeAbsMagFromAppMag(json["magV"].toDouble());
	}
	else
	{
		params.absmag = computeAbsMagFromTempAndRadius(params.temperature,
		                                               parameters.radius);
	}
	parameters.color = computeColor();
}

Star::Star(std::string const& name, CelestialBody::Parameters const& parameters,
           Parameters const& starParameters, Orbitable& parent, Orbit* orbit)
    : CelestialBody(CelestialBody::Type::STAR, name, parameters, parent, orbit)
    , params(starParameters)
{
	this->parameters.color = computeColor();
}

Star::Star(std::string const& name, CelestialBody::Parameters const& parameters,
           Parameters const& starParameters, OrbitalSystem const& system,
           Orbit* orbit)
    : CelestialBody(CelestialBody::Type::STAR, name, parameters, system, orbit)
    , params(starParameters)
{
	this->parameters.color = computeColor();
}

double Star::computeTemperatureFromMass(double mass)
{
	// https://astronomy.stackexchange.com/questions/13104/how-to-calculate-the-temperature-of-a-star
	return 5778.0 * pow(mass * 5.02785e-31, 0.54);
}

double Star::computeMassFromTemperature(double temperature)
{
	// https://astronomy.stackexchange.com/questions/13104/how-to-calculate-the-temperature-of-a-star
	return pow(temperature / 5778.0, 1.0 / 0.54) * 1.98892e30;
}

double Star::computeAbsMagFromAppMag(double apparentMag, double dist)
{
	return apparentMag - 5.0 * (log10(dist) - 1.0);
}

double Star::computeAbsMagFromAppMag(double apparentMag) const
{
	return computeAbsMagFromAppMag(apparentMag,
	                               getSystem().getDistanceToEarth());
}

double Star::computeAbsMagFromTempAndRadius(double temperature, double radius)
{
	const double luminance(blackbody::luminanceFromTemperature(temperature));
	// 3.24078e-18 * 10 pc = 1 m
	const double dist = 1000.0 * radius;
	const double solidAngleAtDist(3.14159e-6);
	const double illumAtDist(luminance * solidAngleAtDist);
	const double apparentMag(-1.0 * (log10(illumAtDist) * 2.5 + 14.0));
	// 3.24078e-17 pc = 1m
	const double res(computeAbsMagFromAppMag(apparentMag, dist * 3.24078e-17));
	return res > 40.0 ? 40.0 : res;
}

Color Star::computeColor() const
{
	Color sRGBcolor(blackbody::colorFromTemperature(params.temperature));
	return sRGBcolor;
}

double Star::getIllumination(Vector3 const& fromRelPos,
                             UniversalTime const& ut) const
{
	return pow(10.0, -0.4 * (getApparentMagnitude(fromRelPos, ut) + 14.0));
	return -2.5 * log10(getApparentMagnitude(fromRelPos, ut)) - 14.0;
}

double Star::getApparentMagnitude(Vector3 const& fromRelPos,
                                  UniversalTime const& /*ut*/) const
{
	// 3.24078e-17 pc = 1m
	return params.absmag
	       + 5.0 * (log10(fromRelPos.length() * 3.24078e-17) - 1.0);
}

QJsonObject Star::getJSONRepresentation() const
{
	QJsonObject result(CelestialBody::getJSONRepresentation());
	result["temperature"] = params.temperature;
	return result;
}
