/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/Orbit.hpp"

#include "math/EccentricAnomalySolver.hpp"
#include "math/MathUtils.hpp"
#include "math/constants.hpp"

Orbit::Orbit(MassiveBodyMass const& massiveBodyMass, QJsonObject const& json)
    : massiveBodyMass(massiveBodyMass.value)
{
	if(json.contains("semiMajorAxis"))
	{
		parameters.semiMajorAxis = json["semiMajorAxis"].toDouble();
	}
	else if(json.contains("period") && this->massiveBodyMass != 0)
	{
		parameters.semiMajorAxis = SMAfromPeriodMass(json["period"].toDouble(),
		                                             this->massiveBodyMass);
	}
	else if(json.contains("separationMeters"))
	{
		parameters.semiMajorAxis = json["separationMeters"].toDouble();
	}
	else
	{
		std::cerr << "Cannot determine orbital elements." << std::endl;
		return;
	}

	if(this->massiveBodyMass == 0)
	{
		if(!json.contains("period"))
		{
			std::cerr << "Cannot determine period." << std::endl;
			return;
		}
		this->massiveBodyMass = massiveBodyMassFromElements(
		    parameters.semiMajorAxis, json["period"].toDouble());
	}

	parameters.inclination = json["inclination"].toDouble();
	parameters.ascendingNodeLongitude
	    = json["ascendingNodeLongitude"].toDouble();
	parameters.periapsisArgument  = json["periapsisArgument"].toDouble();
	parameters.eccentricity       = json["eccentricity"].toDouble();
	parameters.meanAnomalyAtEpoch = json["meanAnomalyAtEpoch"].toDouble();

	updatePeriod();
	valid = true;
}

Orbit::Orbit(MassiveBodyMass const& massiveBodyMass,
             Orbit::Parameters parameters)
    : parameters(parameters)
    , valid(true)
    , massiveBodyMass(massiveBodyMass.value)
{
	updatePeriod();
}

// TODO(florian) compute correctly
Orbit::Orbit(Period const& period, Orbit::Parameters parameters)
    : parameters(parameters)
    , valid(true)
    , massiveBodyMass(period.value)
{
	const double smaCubed = parameters.semiMajorAxis * parameters.semiMajorAxis
	                        * parameters.semiMajorAxis;
	const double mu = constant::G * massiveBodyMass;
	if(parameters.eccentricity < 1)
	{
		this->period = 2 * constant::pi * sqrt(smaCubed / mu);
	}
	else
	{
		this->period = constant::NaN;
	}
}

Orbit::Orbit(MassiveBodyMass const& massiveBodyMass, Vector3 position,
             Vector3 velocity, UniversalTime const& uT)
    : valid(true)
    , massiveBodyMass(massiveBodyMass.value)
{
	updateParameters(position, velocity, uT);
}

Orbit::Orbit(Orbit const& copiedOrbit)
    : parameters(copiedOrbit.parameters)
    , valid(copiedOrbit.valid)
    , massiveBodyMass(copiedOrbit.massiveBodyMass)
    , period(copiedOrbit.period)
    , cacheUT(DBL_MAX)
{
}

void Orbit::updateParameters(Vector3 position, Vector3 velocity,
                             UniversalTime const& uT)
{
	Vector3 north(crossProduct(position.getUnitForm(), velocity.getUnitForm())
	                  .getUnitForm());

	parameters.inclination            = acos(north[2]);
	parameters.ascendingNodeLongitude = atan2(north[0], -north[1]);

	// transform in 2D problem relPos[2] == 0, relVel[2] == 0
	// set new space x:=asc node vector, z:= orbit north, y:= z^x
	Vector3 relPos(position), relVel(velocity);
	relPos.rotateAlongZ(-1.0 * parameters.ascendingNodeLongitude);
	relVel.rotateAlongZ(-1.0 * parameters.ascendingNodeLongitude);
	relPos.rotateAlongX(-1.0 * parameters.inclination);
	relVel.rotateAlongX(-1.0 * parameters.inclination);

	const double distance(relPos.length());

	// http://control.asu.edu/Classes/MAE462/462Lecture07.pdf
	Vector3 eccentricityVector(
	    crossProduct(relVel, crossProduct(relPos, relVel))
	    / (this->massiveBodyMass * constant::G));
	relPos /= distance; // now unit vector
	eccentricityVector -= relPos;

	parameters.eccentricity = eccentricityVector.length();
	if(parameters.eccentricity != 0.0)
	{
		eccentricityVector /= parameters.eccentricity; // now unit vector
	}
	else
	{
		eccentricityVector = Vector3(1.0, 0.0, 0.0);
	}
	parameters.periapsisArgument
	    = atan2(eccentricityVector[1], eccentricityVector[0]);

	// set new space x:= periapsis vector, z:= orbit north y:=z^x
	// velocity isn't used anymore
	relPos.rotateAlongZ(-1.0 * parameters.periapsisArgument);

	const double tAn(atan2(relPos[1], relPos[0]));
	const double e(parameters.eccentricity);

	// see getMassiveBodyDistanceFromTrueAn
	parameters.semiMajorAxis
	    = ((1.0 + e * cos(tAn)) * distance) / (1.0 - e * e);

	const double foo((e + cos(tAn)) / (1.0 + e * cos(tAn)));
	double eccAn(e < 1.0 ? acos(foo) : acosh(foo));
	if(tAn < 0.0)
	{
		eccAn *= -1.0;
	}
	const double meanAn(e < 1.0 ? eccAn - e * sin(eccAn)
	                            : e * sinh(eccAn) - eccAn);
	double smaCubed = parameters.semiMajorAxis * parameters.semiMajorAxis
	                  * parameters.semiMajorAxis;
	smaCubed = fabs(smaCubed);
	const double n(sqrt(constant::G * this->massiveBodyMass / smaCubed));

	updatePeriod();
	const UniversalTime uT2       = e < 1 ? uT % period : uT;
	const auto equivUT            = uT2.convert_to<double>();
	parameters.meanAnomalyAtEpoch = meanAn - (n * equivUT);

	// update cache with what we know
	trueAnomaly    = tAn;
	this->position = position;
	cacheUT        = uT;
}

double Orbit::getMassiveBodyMass() const
{
	return massiveBodyMass;
}

Orbit::Parameters Orbit::getParameters() const
{
	return parameters;
}

double Orbit::getPeriod() const
{
	return period;
}

Vector3 Orbit::getNorth() const
{
	Vector3 north(0.0, 0.0, 1.0);
	north.rotateAlongX(parameters.inclination);
	north.rotateAlongZ(parameters.ascendingNodeLongitude);
	return north;
}

void Orbit::convertMeanAnAtEpochToMeanAn(UniversalTime const& currentUt)
{
	parameters.meanAnomalyAtEpoch = getMeanAnomalyAtUT(currentUt);
}

void Orbit::convertMeanAnToMeanAnAtEpoch(UniversalTime const& currentUt)
{
	const UniversalTime uT2
	    = parameters.eccentricity < 1 ? currentUt % period : currentUt;
	const auto equivUT = uT2.convert_to<double>();

	double smaCubed = parameters.semiMajorAxis * parameters.semiMajorAxis
	                  * parameters.semiMajorAxis;
	if(parameters.eccentricity > 1)
	{
		smaCubed *= -1;
	}
	const double n(sqrt(constant::G * massiveBodyMass / smaCubed));
	parameters.meanAnomalyAtEpoch -= (n * equivUT);
}

double Orbit::getMeanAnomalyAtUT(UniversalTime const& uT)
{
	// TODO(florian) : test assertion
	// to return a sensible meanAnomaly (we don't care about returning > 2*M_PI
	// values as big as we want mathematically but float precision Does care,
	// so we treat potentially huge uTs like this;
	// if uT < period, then the result will be between 0 and 2*M_PI)
	// several loops are done for performance (we don't want one loop which
	// loops millions of times)
	const UniversalTime uT2 = parameters.eccentricity < 1 ? uT % period : uT;
	const auto equivUT      = uT2.convert_to<double>();

	double smaCubed = parameters.semiMajorAxis * parameters.semiMajorAxis
	                  * parameters.semiMajorAxis;
	if(parameters.eccentricity > 1)
	{
		smaCubed *= -1;
	}
	const double n(sqrt(constant::G * massiveBodyMass / smaCubed));
	return (n * equivUT) + parameters.meanAnomalyAtEpoch;
}

double Orbit::getEccentricAnomalyAtUT(UniversalTime const& uT)
{
	if(parameters.eccentricity < 1)
	{
		return EccentricAnomalySolver::solveForEllipticOrbit(
		    getMeanAnomalyAtUT(uT), parameters.eccentricity);
	}
	if(parameters.eccentricity == 1)
	{
		return EccentricAnomalySolver::solveForParabolicOrbit(
		    getMeanAnomalyAtUT(uT));
	}

	return EccentricAnomalySolver::solveForHyperbolicOrbit(
	    getMeanAnomalyAtUT(uT), parameters.eccentricity);
}

double Orbit::getTrueAnomalyAtUT(UniversalTime const& uT)
{
	const double eccentricAnomaly(getEccentricAnomalyAtUT(uT));
	if(parameters.eccentricity < 1)
	{
		const double coeff(sqrt((1 + parameters.eccentricity)
		                        / (1 - parameters.eccentricity)));

		return 2.0 * atan(coeff * tan(eccentricAnomaly / 2.0));
	}

	if(parameters.eccentricity == 1)
	{
		return 2.0 * atan(eccentricAnomaly);
	}

	const double coeff(
	    sqrt((parameters.eccentricity + 1) / (parameters.eccentricity - 1)));

	return 2.0 * atan(coeff * tanh(eccentricAnomaly / 2.0));
}

double Orbit::getMassiveBodyDistanceFromTrueAn(double trueAnomaly) const
{
	// TODO(florian) : solve situation for parabola
	const double e(parameters.eccentricity);
	const double a(parameters.semiMajorAxis);
	const double t(trueAnomaly);

	if(e != 1.0)
	{
		return a * (1.0 - (e * e)) / (1.0 + (e * cos(t)));
	}

	return 2.0 * a / (1.0 + cos(t));
}

double Orbit::getMassiveBodyDistanceAtUT(UniversalTime const& uT)
{
	updateCacheAtUT(uT);
	return getMassiveBodyDistanceFromTrueAn(trueAnomaly);
}

void Orbit::updateCacheAtUT(UniversalTime const& uT)
{
	if(cacheUT != uT)
	{
		trueAnomaly = getTrueAnomalyAtUT(uT);
		position    = getPositionFromTrueAn(trueAnomaly);
		cacheUT     = uT;
	}
}

Vector3 Orbit::getPositionFromTrueAn(double trueAnomaly) const
{
	const double distance(getMassiveBodyDistanceFromTrueAn(trueAnomaly));

	Vector3 result(distance, 0.0, 0.0);
	result.rotateAlongZ(trueAnomaly + parameters.periapsisArgument);
	result.rotateAlongX(parameters.inclination);
	result.rotateAlongZ(parameters.ascendingNodeLongitude);

	return result;
}

Vector3 Orbit::getPositionAtUT(UniversalTime const& uT)
{
	updateCacheAtUT(uT);
	return position;
}

Vector3 Orbit::getVelocityFromTrueAn(double trueAnomaly) const
{
	const double angleOfVel(
	    atan((parameters.eccentricity * sin(trueAnomaly))
	         / (1.0 + parameters.eccentricity * cos(trueAnomaly))));
	const double eSq(parameters.eccentricity * parameters.eccentricity);
	const double p(parameters.semiMajorAxis * (1.0 - eSq));
	const double velNormSq(
	    (massiveBodyMass * constant::G / p)
	    * (1.0 + eSq + 2 * parameters.eccentricity * cos(trueAnomaly)));

	const Vector3 pos(getPositionFromTrueAn(trueAnomaly));
	const Vector3 radOut(pos.getUnitForm());
	const Vector3 forward(crossProduct(getNorth(), radOut).getUnitForm());

	return sqrt(velNormSq)
	       * (forward * cos(angleOfVel) + radOut * sin(angleOfVel))
	             .getUnitForm();
}

Vector3 Orbit::getVelocityFromTrueAn(double trueAnomaly,
                                     Vector3 const& radialOut) const
{
	const double e(parameters.eccentricity);
	const double cost(cos(trueAnomaly));
	const double angleOfVel(atan((e * sin(trueAnomaly)) / (1.0 + e * cost)));
	const double eSq(e * e);
	const double p(parameters.semiMajorAxis * (1.0 - eSq));
	const double velNormSq((massiveBodyMass * constant::G / p)
	                       * (1.0 + eSq + 2 * e * cost));

	const Vector3 forward(crossProduct(getNorth(), radialOut).getUnitForm());

	return sqrt(velNormSq)
	       * (forward * cos(angleOfVel) + radialOut * sin(angleOfVel))
	             .getUnitForm();
}

Vector3 Orbit::getVelocityAtUT(UniversalTime const& uT)
{
	updateCacheAtUT(uT);
	return getVelocityFromTrueAn(trueAnomaly, position.getUnitForm());
}

Vector3 Orbit::getAccelerationFromPosition(Vector3 const& position) const
{
	const double dist(position.length());
	return -1.0 * constant::G * massiveBodyMass * position
	       / (dist * dist * dist);
}

Vector3 Orbit::getAccelerationAtUT(UniversalTime const& uT)
{
	updateCacheAtUT(uT);
	return getAccelerationFromPosition(position);
}

CoordinateSystem Orbit::getRelativeCoordinateSystemAtUT(UniversalTime const& uT)
{
	CoordinateSystem result;
	result.setOrigin(getPositionAtUT(uT));
	result.rotateAlongX(parameters.inclination);
	result.rotateAlongZ(parameters.ascendingNodeLongitude);

	return result;
}

std::ostream& Orbit::displayAsText(std::ostream& stream) const
{
	stream << "[" << std::endl;
	stream << "\tMassiveBodyMass : " << massiveBodyMass << std::endl;
	stream << "\tInclination : " << parameters.inclination << std::endl;
	stream << "\tAscending Node Longitude : "
	       << parameters.ascendingNodeLongitude << std::endl;
	stream << "\tPeriapsis Argument : " << parameters.periapsisArgument
	       << std::endl;
	stream << "\tEccentricity : " << parameters.eccentricity << std::endl;
	stream << "\tSemi Major Axis : " << parameters.semiMajorAxis << std::endl;
	stream << "\tMean Anomaly At Epoch : " << parameters.meanAnomalyAtEpoch
	       << std::endl;
	stream << "]" << std::endl;
	stream << period << std::endl;

	return stream;
}

QJsonObject Orbit::getJSONRepresentation() const
{
	QJsonObject result;

	result["inclination"]            = parameters.inclination;
	result["ascendingNodeLongitude"] = parameters.ascendingNodeLongitude;
	result["periapsisArgument"]      = parameters.periapsisArgument;
	result["eccentricity"]           = parameters.eccentricity;
	result["semiMajorAxis"]          = parameters.semiMajorAxis;
	result["meanAnomalyAtEpoch"]     = parameters.meanAnomalyAtEpoch;

	return result;
}

void Orbit::setMassiveBodyMass(MassiveBodyMass const& massiveBodyMass)
{
	this->massiveBodyMass = massiveBodyMass.value;
	updatePeriod();
}

void Orbit::updatePeriod()
{
	const double smaCubed = parameters.semiMajorAxis * parameters.semiMajorAxis
	                        * parameters.semiMajorAxis;
	const double mu = constant::G * massiveBodyMass;
	if(parameters.eccentricity < 1)
	{
		period = 2 * constant::pi * sqrt(smaCubed / mu);
	}
	else
	{
		period = constant::NaN;
	}
}

double Orbit::SMAfromPeriodMass(double period, double massiveBodyMass)
{
	return cbrt(constant::G * massiveBodyMass * period * period
	            / (4.0 * constant::pi * constant::pi));
}

double Orbit::massiveBodyMassFromElements(double semiMajorAxis, double period)
{
	return semiMajorAxis * semiMajorAxis * semiMajorAxis * 4.0 * constant::pi
	       * constant::pi / (constant::G * period * period);
}

std::ostream& operator<<(std::ostream& stream, Orbit const& orbit)
{
	return orbit.displayAsText(stream);
}
