/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../../include/physics/CelestialBody.hpp"

#include <ranges>

#include "math/MathUtils.hpp"
#include "physics/OrbitalSystem.hpp"

CelestialBody::CelestialBody(Type type, QJsonObject const& json,
                             OrbitalSystem const& system)
    : Orbitable(orbitableType(type), json, system)
    , type(type)
{
	parseJSON(json);
	computeBaseRotation();
}

CelestialBody::CelestialBody(Type type, QJsonObject const& json,
                             Orbitable& parent)
    : Orbitable(orbitableType(type), json, parent)
    , type(type)
{
	parseJSON(json);
	computeBaseRotation();
}

CelestialBody::CelestialBody(Type type, std::string const& name,
                             Parameters parameters, OrbitalSystem const& system,
                             Orbit* orbit)
    : Orbitable(orbitableType(type), name, system, orbit)
    , parameters(parameters)
    , type(type)
{
	computeBaseRotation();
}

CelestialBody::CelestialBody(Type type, std::string const& name,
                             Parameters parameters, Orbitable& parent,
                             Orbit* orbit)
    : Orbitable(orbitableType(type), name, parent, orbit)
    , parameters(parameters)
    , type(type)
{
	computeBaseRotation();
}

void CelestialBody::computeBaseRotation()
{
	Vector3 x(1.0, 0.0, 0.0), y(0.0, 1.0, 0.0);
	x.rotateAlongZ(parameters.northPoleRightAsc);
	y.rotateAlongZ(parameters.northPoleRightAsc);
	Vector3 planetLocalZ // north pole
	    = Matrix4x4(parameters.northPoleDeclination, -1.0 * y) * x;

	planetLocalZ.rotateAlongX(-1.f * getSystem().getDeclinationTilt());
	planetLocalZ = planetLocalZ.getUnitForm();

	Vector3 planetLocalY(
	    crossProduct(planetLocalZ, Vector3(1.0, 0.0, 0.0)).getUnitForm());
	Vector3 planetLocalX(
	    crossProduct(planetLocalY, planetLocalZ).getUnitForm());

	const std::array<std::array<double, 4>, 3> data
	    = {{{planetLocalX[0], planetLocalY[0], planetLocalZ[0], 0.0},
	        {planetLocalX[1], planetLocalY[1], planetLocalZ[1], 0.0},
	        {planetLocalX[2], planetLocalY[2], planetLocalZ[2], 0.f}}};

	baseRotation = Matrix4x4(data);
}

double CelestialBody::getPrimeMeridianSiderealTimeAtUT(UniversalTime uT) const
{
	operator%= <UniversalTime>(uT, parameters.siderealRotationPeriod);

	return parameters.siderealTimeAtEpoch
	       + 2.0 * constant::pi * static_cast<double>(uT)
	             / parameters.siderealRotationPeriod;
}

Matrix4x4 CelestialBody::getProperRotationAtUT(UniversalTime const& uT) const
{
	const float siderealTime = getPrimeMeridianSiderealTimeAtUT(uT);

	const Matrix4x4 siderealRotation(siderealTime, Vector3(0.0, 0.0, 1.0));

	return baseRotation * siderealRotation;
}

double CelestialBody::getEscapeVelocity(double altitude) const
{
	return sqrt(2.0 * constant::G * parameters.mass
	            / (parameters.radius + altitude));
}

double CelestialBody::getSphereOfInfluenceRadius() const
{
	auto const* parent(getParent());
	if(parent == nullptr
	   || parent->getOrbitableType() == Orbitable::Type::BINARY
	   || getOrbit() == nullptr)
	{
		return DBL_MAX;
	}
	return fabs(getOrbit()->getParameters().semiMajorAxis)
	       * pow(parameters.mass
	                 / dynamic_cast<CelestialBody const*>(parent)
	                       ->getCelestialBodyParameters()
	                       .mass,
	             2.0 / 5.0);
}

double CelestialBody::getMaximumRocheLimit() const
{
	// kg
	const double mass(getCelestialBodyParameters().mass);

	// in m
	const double rad(getCelestialBodyParameters().radius);
	Vector3 o(getCelestialBodyParameters().oblateness);
	// in m^3
	const double volume
	    = 4.0 * constant::pi * (rad * o[0] * rad * o[1] * rad * o[2]) / 3.0;

	// in kg/m^3
	const double density(mass / volume);

	// in m
	return 2.422849865 * parameters.radius * pow(density / 500.0, 1.0 / 3.0);
}

Vector3 CelestialBody::getGravitationalField(Vector3 const& atRelPos) const
{
	const double dist = atRelPos.length();
	Vector3 g(-1.0 * atRelPos / dist);
	g *= constant::G * parameters.mass / (dist * dist);
	if(parameters.oblateness != Vector3(1.0, 1.0, 1.0))
	{
		const double f(1.0 - parameters.oblateness[2]);
		const double mu(constant::G * parameters.mass);
		const auto Re = parameters.radius;
		Vector3 surfaceRelPos(getProperRotationAtUT(0.0).inverted() * atRelPos);
		const auto r = surfaceRelPos.length();

		// https://www.vcalc.com/wiki/eng/J2
		// https://en.wikipedia.org/wiki/Geopotential_model
		// PRECOMPUTE
		double J2(f * 2 / 3);
		J2 -= pow(parameters.radius, 3)
		      * pow(2 * constant::pi / parameters.siderealRotationPeriod, 2)
		      / (3 * mu); // actually J2~ for now
		// convert to true J2
		J2 *= mu * Re * Re;

		// https://en.wikipedia.org/wiki/Geopotential_model eq 13
		const double x(surfaceRelPos[0]), y(surfaceRelPos[1]),
		    z(surfaceRelPos[2]);
		const double xSq(x * x), ySq(y * y), zSq(z * z);
		const double threeHalvesXsqPlusYsq = 1.5 * (xSq + ySq);
		const double equatorialTerm
		    = J2 * (6 * zSq - threeHalvesXsqPlusYsq) / pow(r, 7);
		const double polarTerm
		    = J2 * 3 * (zSq - threeHalvesXsqPlusYsq) / pow(r, 7);

		Vector3 perturb(equatorialTerm * x, equatorialTerm * y, polarTerm * z);
		perturb = getProperRotationAtUT(0.0) * perturb;

		g += perturb;

		/* Maybe later ; ! CURRENT VERSION DOESN'T WORK
		const double J3(-2.61913e29);
		const double equatorialTermJ3
		    = J3 * z * (10 * zSq - (15 / 2) * (xSq + ySq)) / pow(r, 9);
		const double polarTermJ3
		    = J3 * (4 * zSq * (zSq - 3 * (xSq + ySq))) / pow(r, 9);

		perturb = Vector3(equatorialTermJ3 * x, equatorialTermJ3 * y,
		polarTermJ3 * z); perturb = getProperRotationAtUT(0.0) * perturb;

		g += perturb;
		*/
	}
	return g;
}

double CelestialBody::getApparentMagnitude(Vector3 const& fromRelPos,
                                           UniversalTime const& ut) const
{
	return -2.5 * log10(getIllumination(fromRelPos, ut)) - 14.0;
}

std::vector<CelestialBody const*>
    CelestialBody::getPotentiallyRelevantNeighbors() const
{
	std::vector<CelestialBody const*> result;
	for(auto const* star : getSystem().getAllStarsPointers())
	{
		result.push_back(star);
	}
	if(getParent() != nullptr
	   && getParent()->getOrbitableType() != Orbitable::Type::BINARY)
	{
		auto const* parent(dynamic_cast<CelestialBody const*>(getParent()));
		if(parent->getOrbitableType() == Orbitable::Type::PLANET)
		{
			result.push_back(parent);
		}
		for(auto const& sibling : parent->getChildren())
		{
			if(sibling.get() != this
			   && sibling->getOrbitableType() != Orbitable::Type::SPACECRAFT)
			{
				result.push_back(
				    dynamic_cast<CelestialBody const*>(sibling.get()));
			}
		}
	}
	for(auto const& child : getChildren())
	{
		if(child->getOrbitableType() != Orbitable::Type::SPACECRAFT)
		{
			result.push_back(dynamic_cast<CelestialBody const*>(child.get()));
		}
	}
	return result;
}

std::vector<std::pair<CelestialBody const*, Vector3>>
    CelestialBody::getSortedRelevantNeighboringOccluders(
        UniversalTime const& uT) const
{
	// map keys = apparent diameter from self
	// map value = occluder+relativePosition pair
	std::vector<std::pair<double, std::pair<CelestialBody const*, Vector3>>>
	    occluders;

	for(auto const* neighbor : getPotentiallyRelevantNeighbors())
	{
		const Vector3 relPos(getRelativePositionAtUt(*this, *neighbor, uT));
		const float rad(neighbor->getCelestialBodyParameters().radius);
		occluders.emplace_back(rad / relPos.length(),
		                       std::make_pair(neighbor, relPos));
	}

	// Sort the vector based on the apparent diameter in descending order
	std::ranges::sort(occluders, std::greater{},
	                  [](auto const& elem) { return elem.first; });

	std::vector<std::pair<CelestialBody const*, Vector3>> result;
	result.reserve(occluders.size());
	for(auto& [_, occluder] : occluders)
	{
		result.emplace_back(occluder);
	}
	return result;
}

std::vector<std::pair<CelestialBody const*, Vector4>>
    CelestialBody::getSortedRelevantNeighboringEmitters(
        UniversalTime const& uT) const
{
	// map keys = apparent magnitude from self
	// map value = occluder+relativePosition pair
	std::map<double, std::pair<CelestialBody const*, Vector4>> sortedEmitters;

	for(auto const* neighbor : getPotentiallyRelevantNeighbors())
	{
		const Vector3 relPos(getRelativePositionAtUt(*this, *neighbor, uT));
		const double mag(neighbor->getApparentMagnitude(-1.0 * relPos, uT));
		sortedEmitters[mag] = std::make_pair(
		    neighbor, Vector4(neighbor->getLightCorrectedRelativePosition(
		                          -1.0 * relPos, uT),
		                      mag));
	}
	std::vector<std::pair<CelestialBody const*, Vector4>> result;
	result.reserve(sortedEmitters.size());
	for(auto const& pair : sortedEmitters)
	{
		result.emplace_back(pair.second.first, pair.second.second);
	}
	return result;
}

QJsonObject CelestialBody::getJSONRepresentation() const
{
	QJsonObject result = Orbitable::getJSONRepresentation();

	result["mass"]       = parameters.mass;
	result["radius"]     = parameters.radius;
	result["oblateness"] = parameters.oblateness.getJSONRepresentation();
	result["color"]      = parameters.color.getJSONRepresentation();
	result["siderealTimeAtEpoch"]    = parameters.siderealTimeAtEpoch;
	result["siderealRotationPeriod"] = parameters.siderealRotationPeriod;
	result["northPoleRightAsc"]      = parameters.northPoleRightAsc;
	result["northPoleDeclination"]   = parameters.northPoleDeclination;

	return result;
}

void CelestialBody::parseJSON(QJsonObject const& json)
{
	parameters.mass = json["mass"].toDouble();
	// TEMP generate radius
	parameters.radius = json["radius"].toDouble(70000000.0);
	parameters.oblateness
	    = Vector3(json["oblateness"].toObject(), Vector3(1.0, 1.0, 1.0));
	parameters.color
	    = Color(json["color"].toObject(), Color(255, 255, 255, 255));
	parameters.siderealTimeAtEpoch = json["siderealTimeAtEpoch"].toDouble();
	parameters.siderealRotationPeriod
	    = json["siderealRotationPeriod"].toDouble(365.25 * 24 * 3600.0);
	parameters.northPoleRightAsc = json["northPoleRightAsc"].toDouble();
	parameters.northPoleDeclination
	    = json["northPoleDeclination"].toDouble(1.5707963705062866);
}

Orbitable::Type CelestialBody::orbitableType(Type celestialBodyType)
{
	return celestialBodyType == Type::STAR
	           ? Orbitable::Type::STAR
	           : (celestialBodyType == Type::PLANET
	                  ? Orbitable::Type::PLANET
	                  : Orbitable::Type::SPACECRAFT);
}
