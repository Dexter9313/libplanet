/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/Spacecraft.hpp"
#include "physics/OrbitalSystem.hpp"

Spacecraft::Spacecraft(QJsonObject const& json, OrbitalSystem const& system)
    : CelestialBody(CelestialBody::Type::SPACECRAFT, json, system)
{
	parameters.antennaDir
	    = Vector3(json["antennaDir"].toObject(), Vector3(0.0, 1.0, 0.0));
	parameters.up = Vector3(json["up"].toObject(), Vector3(0.0, 0.0, 1.0));
	parameters.antennaTarget = json["antennaTarget"].toString();
	parameters.modelPath     = json["modelPath"].toString();
	parameters.modelScale    = json["modelScale"].toDouble(1.0);
}

Spacecraft::Spacecraft(QJsonObject const& json, Orbitable& parent)
    : CelestialBody(CelestialBody::Type::SPACECRAFT, json, parent)
{
	parameters.antennaDir
	    = Vector3(json["antennaDir"].toObject(), Vector3(0.0, 1.0, 0.0));
	parameters.up = Vector3(json["up"].toObject(), Vector3(0.0, 0.0, 1.0));
	parameters.antennaTarget = json["antennaTarget"].toString();
	parameters.modelPath     = json["modelPath"].toString();
	parameters.modelScale    = json["modelScale"].toDouble(1.0);
}

Spacecraft::Spacecraft(std::string const& name,
                       CelestialBody::Parameters const& cbParams,
                       Parameters scParams, Orbitable& parent, Orbit* orbit)
    : CelestialBody(CelestialBody::Type::SPACECRAFT, name, cbParams, parent,
                    orbit)
    , parameters(std::move(scParams))
{
}

Spacecraft::Spacecraft(std::string const& name,
                       CelestialBody::Parameters const& cbParams,
                       Parameters scParams, OrbitalSystem const& system,
                       Orbit* orbit)
    : CelestialBody(CelestialBody::Type::SPACECRAFT, name, cbParams, system,
                    orbit)
    , parameters(std::move(scParams))
{
}

Star const* Spacecraft::getHostStar() const
{
	if(getParent() == nullptr)
	{
		return nullptr;
	}

	if(getParent()->getOrbitableType() == Orbitable::Type::STAR)
	{
		return dynamic_cast<Star const*>(getParent());
	}
	return this->getSystem().getAllStarsPointers()[0];
}

Matrix4x4 Spacecraft::getProperRotationAtUT(UniversalTime const& uT) const
{
	Vector3 spacecraftLocalY(parameters.antennaDir.getUnitForm()),
	    spacecraftLocalZ(parameters.up.getUnitForm());
	Vector3 spacecraftLocalX(
	    crossProduct(spacecraftLocalY, spacecraftLocalZ).getUnitForm());
	spacecraftLocalZ = crossProduct(spacecraftLocalX, spacecraftLocalY);

	// transpose of orthonormal matrix = inverse
	std::array<std::array<double, 4>, 3> data = {
	    {{spacecraftLocalX[0], spacecraftLocalX[1], spacecraftLocalX[2], 0.0},
	     {spacecraftLocalY[0], spacecraftLocalY[1], spacecraftLocalY[2], 0.0},
	     {spacecraftLocalZ[0], spacecraftLocalZ[1], spacecraftLocalZ[2], 0.0}}};

	const Matrix4x4 orientAntennaToYAndUpToZ(data);

	Vector3 x(1.0, 0.0, 0.0), y(0.0, 1.0, 0.0), z(0.0, 0.0, 1.0);
	if(parameters.antennaTarget == "")
	{
		auto RAdec = getSystem().getRADecFromCarthesian(getOrbit()->getNorth());
		x.rotateAlongZ(RAdec.first);
		y.rotateAlongZ(RAdec.first);

		z = Matrix4x4(RAdec.second, -1.0 * y) * x;

		z.rotateAlongX(-1.f * getSystem().getDeclinationTilt());
		z = z.getUnitForm();
	}

	if(parameters.antennaTarget == "")
	{
		y = -1.0 * getRelativePositionAtUT(uT).getUnitForm();
	}
	else
	{
		y = getRelativePositionAtUt(
		        *this, *getSystem()[parameters.antennaTarget.toStdString()], uT)
		        .getUnitForm();
	}

	x = crossProduct(y, z).getUnitForm();
	if(parameters.antennaTarget != "")
	{
		z = crossProduct(x, y).getUnitForm();
	}

	data = {{{x[0], y[0], z[0], 0.0},
	         {x[1], y[1], z[1], 0.0},
	         {x[2], y[2], z[2], 0.0}}};

	return Matrix4x4(data) * orientAntennaToYAndUpToZ;
}

double Spacecraft::getIllumination(Vector3 const& fromRelPos,
                                   UniversalTime const& ut) const
{
	const double dist(fromRelPos.length());
	double illum(0.0);
	for(auto const* star : getSystem().getAllStarsPointers())
	{
		const Vector3 scRelPos(
		    Orbitable::getRelativePositionAtUt(*star, *this, ut));
		const double starApparentMagFromSc(
		    star->getApparentMagnitude(scRelPos, ut));
		const double scIllum(
		    pow(10.0, -0.4 * (starApparentMagFromSc + 14.0))); // lx
		const double scSurface
		    = constant::pi
		      * pow(getCelestialBodyParameters().radius, 2.0); // m^2
		const double bodyReceivedFlux(scIllum * scSurface);    // lm

		// hardcode as ISS albedo, should be enough for most cases...
		const double albedo = 0.81245;

		const double bodyMeanIntensity(bodyReceivedFlux * albedo * 0.25
		                               / constant::pi); // cd
		const double oneReceiverSquareMeterToBodySteradian
		    = 1.0 / pow(dist, 2.0);

		// 4.0 is empirical
		const double alpha(acos(dotProduct(fromRelPos.getUnitForm(),
		                                   -1.0 * scRelPos.getUnitForm())));
		// alpha = constant::pi - alpha;
		// alpha = 0.0;
		const double phaseValue(4.0
		                        * ((1.0 - alpha / constant::pi) * cos(alpha)
		                           + sin(alpha) / constant::pi));

		illum += bodyMeanIntensity * oneReceiverSquareMeterToBodySteradian
		         * phaseValue;
	}
	return illum;
}

QJsonObject Spacecraft::getJSONRepresentation() const
{
	QJsonObject result(CelestialBody::getJSONRepresentation());
	result["antennaDir"]    = parameters.antennaDir.getJSONRepresentation();
	result["up"]            = parameters.up.getJSONRepresentation();
	result["antennaTarget"] = parameters.antennaTarget;
	result["modelPath"]     = parameters.modelPath;
	result["modelScale"]    = parameters.modelScale;
	return result;
}
