/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "physics/Planet.hpp"

#include "physics/OrbitalSystem.hpp"

Planet::Planet(QJsonObject const& json, OrbitalSystem const& system)
    : CelestialBody(CelestialBody::Type::PLANET, json, system)
    , randomGen(getPseudoRandomSeed())
{
	parseJSON(json);
}

Planet::Planet(QJsonObject const& json, Orbitable& parent)
    : CelestialBody(CelestialBody::Type::PLANET, json, parent)
    , randomGen(getPseudoRandomSeed())
{
	parseJSON(json);
}

Planet::Planet(std::string const& name,
               CelestialBody::Parameters const& cbParams,
               Parameters const& planetParams, Orbitable& parent, Orbit* orbit)
    : CelestialBody(CelestialBody::Type::PLANET, name, cbParams, parent, orbit)
    , parameters(planetParams)
    , randomGen(getPseudoRandomSeed())
{
}

Planet::Planet(std::string const& name,
               CelestialBody::Parameters const& cbParams,
               Parameters const& planetParams, OrbitalSystem const& system,
               Orbit* orbit)
    : CelestialBody(CelestialBody::Type::PLANET, name, cbParams, system, orbit)
    , parameters(planetParams)
    , randomGen(getPseudoRandomSeed())
{
}

Star const* Planet::getHostStar() const
{
	if(getParent() == nullptr)
	{
		return nullptr;
	}

	if(getParent()->getOrbitableType() == Orbitable::Type::STAR)
	{
		return dynamic_cast<Star const*>(getParent());
	}
	return this->getSystem().getAllStarsPointers()[0];
}

double Planet::getIllumination(Vector3 const& fromRelPos,
                               UniversalTime const& ut) const
{
	const double dist(fromRelPos.length());
	double illum(0.0);
	const double r(getCelestialBodyParameters().radius);
	for(auto const* star : getSystem().getAllStarsPointers())
	{
		const Vector3 planetRelPos(
		    Orbitable::getRelativePositionAtUt(*star, *this, ut));
		const double starApparentMagFromPlanet(
		    star->getApparentMagnitude(planetRelPos, ut));
		const double planetIllum(
		    pow(10.0, -0.4 * (starApparentMagFromPlanet + 14.0)));  // lx
		const double planetSurface = constant::pi * r * r;          // m^2
		const double bodyReceivedFlux(planetIllum * planetSurface); // lm

		const double bodyMeanIntensity(bodyReceivedFlux * parameters.albedo
		                               * 0.25 / constant::pi); // cd
		const double oneReceiverSquareMeterToBodySteradian
		    = 1.0 / pow(dist, 2.0);

		// 4.0 is empirical
		const double alpha(acos(dotProduct(fromRelPos.getUnitForm(),
		                                   -1.0 * planetRelPos.getUnitForm())));
		// alpha = constant::pi - alpha;
		// alpha = 0.0;
		const double phaseValue(4.0
		                        * ((1.0 - alpha / constant::pi) * cos(alpha)
		                           + sin(alpha) / constant::pi));

		// "infinity" because the fact that the object doesn't see the whole
		// half sphere of the planet is not taken into account yet
		const double illumInfinity(bodyMeanIntensity
		                           * oneReceiverSquareMeterToBodySteradian
		                           * phaseValue);

		// beta := half apparent angle of planet (angle between center, relPos
		// and horizon for object)
		const double beta(asin(r / dist));
		const double x((dist - r) * tan(beta));
		// gamma := signed angle between center, relpos and terminator for
		// object)
		const double gamma(atan(cos(alpha) / ((dist / r) - sin(alpha))));
		double y = NAN;
		// if terminator beyond horizon, take horizon
		if(sin(alpha) > sin(beta))
		{
			y = ((dist - r) * tan(gamma));
		}
		else
		{
			y = gamma >= 0.0 ? x : -x;
		}

		illum += illumInfinity * (x + y) / (r + r * cos(alpha));
	}
	return illum;
}

Vector3 Planet::getLightCorrectedRelativePosition(Vector3 const& fromRelPos,
                                                  UniversalTime const& ut) const
{
	auto const* star(getSystem().getAllStarsPointers()[0]);
	const Vector3 starRelDir(
	    Orbitable::getRelativePositionAtUt(*this, *star, ut).getUnitForm());
	const Vector3 objRelDir(fromRelPos.getUnitForm());
	const double alpha(acos(dotProduct(objRelDir, starRelDir)));

	// normal from objRelDir that goes towards star
	const Vector3 normalTowardsStar(
	    crossProduct(crossProduct(objRelDir, starRelDir), objRelDir));

	const double r(getCelestialBodyParameters().radius);
	const double dist(fromRelPos.length());
	// beta := half apparent angle of planet (angle between center, relPos
	// and horizon for object)
	const double beta(asin(r / dist));
	const double x((dist - r) * tan(beta));
	// gamma := signed angle between center, relpos and terminator for object)
	const double gamma(atan(cos(alpha) / ((dist / r) - sin(alpha))));
	double y = NAN;
	// if terminator beyond horizon, take horizon
	if(sin(alpha) > sin(beta))
	{
		y = ((dist - r) * tan(gamma));
	}
	else
	{
		y = gamma >= 0.0 ? x : -x;
	}

	return -1.0 * fromRelPos + r * objRelDir
	       + normalTowardsStar * (x - y) * 0.5;
}

QJsonObject Planet::getJSONRepresentation() const
{
	QJsonObject result(CelestialBody::getJSONRepresentation());
	result["type"] = typeToStr(parameters.type).c_str();
	if(atmosphere != nullptr)
	{
		result["atmosphere"] = getAtmosphereJSONRepresentation();
	}
	result["innerRing"] = parameters.innerRing;
	result["outerRing"] = parameters.outerRing;
	return result;
}

std::string Planet::typeToStr(Type type)
{
	switch(type)
	{
		case Type::GASGIANT:
			return "gasgiant";
		case Type::TERRESTRIAL:
			return "terrestrial";
		default:
			return "generic";
	}
}

Planet::Type Planet::strToType(std::string const& str)
{
	if(str == "gasgiant")
	{
		return Type::GASGIANT;
	}

	if(str == "terrestrial")
	{
		return Type::TERRESTRIAL;
	}

	return Type::GENERIC;
}

void Planet::parseJSON(QJsonObject const& json)
{
	parameters.type = strToType(
	    json["type"].toString(proceduralTypeStr().c_str()).toStdString());
	if(!json.contains("mass"))
	{
		CelestialBody::parameters.mass = proceduralMass();
	}
	if(!json.contains("color"))
	{
		CelestialBody::parameters.color = proceduralColor();
	}
	if(!json.contains("siderealTimeAtEpoch"))
	{
		CelestialBody::parameters.siderealTimeAtEpoch
		    = proceduralSiderealTimeAtEpoch();
	}
	if(!json.contains("siderealRotationPeriod"))
	{
		CelestialBody::parameters.siderealRotationPeriod
		    = proceduralSiderealRotationPeriod();
	}
	if(!json.contains("northPoleRightAsc"))
	{
		CelestialBody::parameters.northPoleRightAsc
		    = proceduralNorthPoleRightAsc();
	}
	if(!json.contains("northPoleDeclination"))
	{
		CelestialBody::parameters.northPoleDeclination
		    = proceduralNorthPoleDeclination();
	}
	if(!json.contains("oblateness"))
	{
		CelestialBody::parameters.oblateness = proceduralOblateness();
	}
	// if(json.contains("atmosphere"))
	//{
	parseAtmosphereJSON(json["atmosphere"].toObject());
	//}
	parameters.albedo    = json["albedo"].toDouble(proceduralAlbedo());
	parameters.outerRing = json["outerRing"].toDouble(proceduralOuterRings());
	parameters.innerRing = json["innerRing"].toDouble(proceduralInnerRings());
}

QJsonObject Planet::getAtmosphereJSONRepresentation() const
{
	QJsonObject result;
	if(atmosphere == nullptr)
	{
		return result;
	}

	result["HR0"]      = atmosphere->H0R;
	result["HM0"]      = atmosphere->H0M;
	result["O3height"] = atmosphere->O3height;
	result["O3width"]  = atmosphere->O3width;
	result["betaR"]    = atmosphere->betaR.getJSONRepresentation();
	result["betaM"]    = atmosphere->betaM;
	result["betaO3"]   = atmosphere->betaO3.getJSONRepresentation();
	result["clouds"]   = atmosphere->clouds;

	return result;
}

void Planet::parseAtmosphereJSON(QJsonObject const& json)
{
	if(json.isEmpty())
	{
		atmosphere = proceduralAtmosphere();
		// disable oblateness for atmospheres, doesn't work well
		if(atmosphere != nullptr)
		{
			CelestialBody::parameters.oblateness = Vector3(1.0, 1.0, 1.0);
		}
		return;
	}

	if(json["HR0"].toDouble(0.0) == 0.0)
	{
		return;
	}

	atmosphere = std::make_unique<Atmosphere>();

	atmosphere->H0R      = json["HR0"].toDouble();
	atmosphere->H0M      = json["HM0"].toDouble();
	atmosphere->O3height = json["O3height"].toDouble();
	atmosphere->O3width  = json["O3width"].toDouble();
	atmosphere->betaR    = Vector3(json["betaR"].toObject());
	atmosphere->betaM    = json["betaM"].toDouble();
	atmosphere->betaO3   = Vector3(json["betaO3"].toObject());
	atmosphere->clouds   = json["clouds"].toDouble();
}

double Planet::assumedTidalLockingStrengh() const
{
	auto const* orbit(getOrbit());
	if(orbit == nullptr)
	{
		return 0.0;
	}
	const double localAcceleration(
	    constant::G * orbit->getMassiveBodyMass()
	    / pow(orbit->getParameters().semiMajorAxis, 2));
	// assume tidally locked
	if(localAcceleration > 0.1)
	{
		return 1.0;
	}
	// assume close to tidally locked
	if(localAcceleration > 0.01)
	{
		// (0 to 1)
		const double tidalLockingStrenght((localAcceleration - 0.01)
		                                  / (0.1 - 0.01));
		return tidalLockingStrenght;
	}
	return 0.0;
}

double Planet::proceduralMass() const
{
	// g / cm^3
	double density(1.f);
	const float r(randomGen.getRandomNumber(0));
	if(parameters.type == Type::GASGIANT)
	{
		density = 0.5 + r * 1.5;
	}
	else
	{
		density = 3.0 + r * r * 7.0;
	}
	// convert to kg / m^3
	density *= 1000.0;

	// in m
	const double rad(getCelestialBodyParameters().radius);
	const Vector3 o(getCelestialBodyParameters().oblateness);
	// in m^3
	const double volume
	    = 4.0 * constant::pi * (rad * o[0] * rad * o[1] * rad * o[2]) / 3.0;

	// in kg
	return density * volume;
}

Vector3 Planet::proceduralOblateness() const
{
	// empirical (works well with solar system
	return {1.0, 1.0,
	        1.0
	            - exp(-getCelestialBodyParameters().siderealRotationPeriod
	                  / 16400.0)};
}

Color Planet::proceduralColor() const
{
	float r(randomGen.getRandomNumber(1));

	Color c1(97, 142, 232), c2(255, 255, 255), c3(216, 202, 157);
	if(parameters.type != Type::GASGIANT)
	{
		c1 = Color(255, 255, 255);
		c2 = Color(54, 57, 48);
		c3 = Color(231, 125, 17);
	}
	Color result(c1);
	if(r <= 0.6f)
	{
		r /= 0.6f;
		result.r = c1.r * (1.f - r) + c2.r * r;
		result.g = c1.g * (1.f - r) + c2.g * r;
		result.b = c1.b * (1.f - r) + c2.b * r;
	}
	else
	{
		r -= 0.6f;
		r /= 0.4f;
		result.r = c2.r * (1.f - r) + c3.r * r;
		result.g = c2.g * (1.f - r) + c3.g * r;
		result.b = c2.b * (1.f - r) + c3.b * r;
	}
	return result;
}

double Planet::proceduralAlbedo() const
{
	// we want albedo to be tied to color, so get the same random number
	float r(randomGen.getRandomNumber(1));

	double alb1(0.4), alb2(0.9), alb3(0.5);
	if(parameters.type != Type::GASGIANT)
	{
		alb1 = 1.0;
		alb2 = 0.09;
		alb3 = 0.2;
	}
	double result(0.0);
	if(r <= 0.6f)
	{
		r /= 0.6f;
		result = alb1 * (1.f - r) + alb2 * r;
	}
	else
	{
		r -= 0.6f;
		r /= 0.4f;
		result = alb2 * (1.f - r) + alb3 * r;
	}
	return result;
}

double Planet::proceduralSiderealTimeAtEpoch() const
{
	return randomGen.getRandomNumber(2) * 2.0 * constant::pi;
}

double Planet::proceduralSiderealRotationPeriod() const
{
	auto const* orbit(getOrbit());
	const float r = randomGen.getRandomNumber(3);

	double randomRotPeriod = NAN;
	if(parameters.type == Type::GASGIANT)
	{
		randomRotPeriod = (r * 20 + 1) * 3600;
	}
	else
	{
		randomRotPeriod = (r * 20 + 8) * 3600;
	}

	// m/s
	const double randomEquatorSurfaceVel = 2.0 * constant::pi
	                                       * CelestialBody::parameters.radius
	                                       / randomRotPeriod;
	if(randomEquatorSurfaceVel > getEscapeVelocity() / 2.0)
	{
		randomRotPeriod = 4.0 * constant::pi * CelestialBody::parameters.radius
		                  / getEscapeVelocity();
	}

	if(orbit != nullptr)
	{
		const double tidalLockingStrengh(assumedTidalLockingStrengh());
		return orbit->getPeriod() * tidalLockingStrengh
		       + randomRotPeriod * (1.0 - tidalLockingStrengh);
	}
	// not tidally locked
	return randomRotPeriod;
}

double Planet::proceduralNorthPoleRightAsc() const
{
	auto const* orbit(getOrbit());
	const double randomRA(randomGen.getRandomNumber(4) * 2.0 * constant::pi);

	if(orbit != nullptr)
	{
		const Vector3 north(orbit->getNorth());
		const auto RAdec = getSystem().getRADecFromCarthesian(north);
		double tidalLockingStrengh(assumedTidalLockingStrengh());
		tidalLockingStrengh += 0.1;
		tidalLockingStrengh = fmin(1.0, tidalLockingStrengh);
		tidalLockingStrengh = 1.0 - pow((1.0 - tidalLockingStrengh), 8);
		return RAdec.first * tidalLockingStrengh
		       + randomRA * (1.0 - tidalLockingStrengh);
	}
	return randomRA;
}

double Planet::proceduralNorthPoleDeclination() const
{
	auto const* orbit(getOrbit());
	const double randomDec((randomGen.getRandomNumber(5) - 0.5f)
	                       * constant::pi);

	if(orbit != nullptr)
	{
		const Vector3 north(orbit->getNorth());
		const auto RAdec = getSystem().getRADecFromCarthesian(north);
		double tidalLockingStrengh(assumedTidalLockingStrengh());
		tidalLockingStrengh += 0.1;
		tidalLockingStrengh = fmin(1.0, tidalLockingStrengh);
		tidalLockingStrengh = 1.0 - pow((1.0 - tidalLockingStrengh), 8);
		return RAdec.second * tidalLockingStrengh
		       + randomDec * (1.0 - tidalLockingStrengh);
	}
	return randomDec;
}

std::string Planet::proceduralTypeStr() const
{
	if(CelestialBody::parameters.mass != 0.0
	   && CelestialBody::parameters.radius != 0.0)
	{
		// in m
		const double rad(getCelestialBodyParameters().radius);
		const Vector3 o(getCelestialBodyParameters().oblateness);
		// in m^3
		const double volume
		    = 4.0 * constant::pi * (rad * o[0] * rad * o[1] * rad * o[2]) / 3.0;

		const double density(CelestialBody::parameters.mass / volume);
		if(density < 2150)
		{
			return "gasgiant";
		}
		return "terrestrial";
	}
	return randomGen.getRandomNumber(6) < 0.333f ? "terrestrial" : "gasgiant";
}

std::unique_ptr<Planet::Atmosphere> Planet::proceduralAtmosphere() const
{
	float r(randomGen.getRandomNumber(7));
	if(pow(r, 3) < pow(0.5, 3) || getPlanetParameters().type == Type::GASGIANT
	   || getCelestialBodyParameters().oblateness[2] < 0.9)
	{
		return nullptr;
	}
	r *= 2;

	// refractive index,  molecular density
	const double n(1.0 + (r * 1e-4)), Ns(2.504e25 * pow(r, 3));

	const double coeff
	    = 8.0 * pow(constant::pi, 3) * pow(n * n - 1.0, 2) / (3.0 * Ns);

	auto result = std::make_unique<Atmosphere>();

	// factors of radius first
	result->H0R      = 8500.0 / 6371000.0 * fmax(0.1, 2.0 * pow(r, 3));
	result->H0M      = 1500.0 / 6371000.0 * fmax(0.1, 2.0 * pow(1.0 - r, 3));
	result->O3height = 25000.0 / 6371000.0 * fmax(0.1, 2.0 * pow(r, 3));
	result->O3width  = 30000.0 / 6371000.0 * fmax(0.1, 2.0 * pow(r, 3));

	if(result->H0R < 0.0002)
	{
		result->H0R = 0.0002;
	}
	if(result->H0M < 0.0002)
	{
		result->H0M = 0.0002;
	}
	if(result->O3height < 0.0002)
	{
		result->O3height = 0.0002;
	}
	if(result->O3width < 0.0002 * 30 / 25)
	{
		result->O3width = 0.0002 * 30 / 25;
	}

	// multiply radius
	result->H0R *= getCelestialBodyParameters().radius;
	result->H0M *= getCelestialBodyParameters().radius;
	result->O3height *= getCelestialBodyParameters().radius;
	result->O3width *= getCelestialBodyParameters().radius;

	// result->betaR = Vector3(5.19673174e-06, 1.21426979e-05, 2.96452586e-05);
	result->betaR = coeff
	                * Vector3(pow(1.0 / 680e-9, 4), pow(1.0 / 550e-9, 4),
	                          pow(1.0 / 440e-9, 4));
	result->betaM  = (2.0 + r) * result->betaR[2] / 3.0;
	result->betaO3 = coeff * Vector3(0.650e-06, 1.881e-06, 0.085e-06);
	result->clouds = result->H0R;

	return result;
}

double Planet::proceduralOuterRings() const
{
	float probability(0.7f);
	if(parameters.type != Type::GASGIANT)
	{
		probability = 0.05f;
	}
	float r(randomGen.getRandomNumber(8));
	if(r > probability)
	{
		return 0.0;
	}
	r /= probability;
	r *= r;
	double result(CelestialBody::parameters.radius * (2.0 + (10.0 * r)));
	// don't get unreallistically large rings (depends on tides inward and
	// outward)
	const double maxLimit(
	    fmin(getMaximumRocheLimit() * 0.9, getSphereOfInfluenceRadius() * 0.5));
	if(result > maxLimit)
	{
		result = maxLimit;
	}
	if(result < CelestialBody::parameters.radius)
	{
		return 0.0;
	}
	return result;
}

double Planet::proceduralInnerRings() const
{
	const float r(randomGen.getRandomNumber(9));
	const double outerHeight(parameters.outerRing
	                         - getCelestialBodyParameters().radius);
	if(outerHeight < 0.0)
	{
		return 0.0;
	}
	return getCelestialBodyParameters().radius + (1.0 - r * r) * outerHeight;
}
