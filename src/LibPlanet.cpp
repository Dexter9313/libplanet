/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "LibPlanet.hpp"

#include "SettingsWidget.hpp"

#include <QObject>

void LibPlanet::setupSettings(SettingsWidget& settingsWidget)
{
	settingsWidget.insertGroup("simulation", QObject::tr("Simulation"), 0);
	settingsWidget.addDateTimeSetting("starttime",
	                                  QDateTime::currentDateTimeUtc(),
	                                  QObject::tr("Start time (UTC)"));
	settingsWidget.addBoolSetting("lockedrealtime", false,
	                              QObject::tr("Lock to Real Time"));

	settingsWidget.editGroup("graphics");
	settingsWidget.addUIntSetting(
	    "texmaxsize", 8, QObject::tr("Textures max size (x2048)"), 1, 11);
	settingsWidget.addUIntSetting(
	    "gentexload", 1, QObject::tr("Texture generation GPU load"), 1, 4);
	settingsWidget.addUIntSetting(
	    "atmoquality", 1, QObject::tr("Atmosphere rendering quality"), 1, 5);
	settingsWidget.addUIntSetting(
	    "maxlightcasters", 1,
	    QObject::tr("Maximum number of light casters per object"), 1, 2);
}

void LibPlanet::setupPythonAPI()
{
	PythonQtHandler::addWrapper<Vector3Wrapper>();
}
