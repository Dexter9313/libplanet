#version 150 core

in vec3 color;
uniform float mag;

uniform mat4 camera;
uniform float pixelSolidAngle;

out vec4 f_finalcolor;

uniform vec4 lightposradius;

#include <planet/neighborOcclusion.glsl>

uniform NeighborsData neighData;

void main()
{
	gl_Position = camera * vec4(vec3(0.0), 1.0);
	// lm.m-2
	float irradiance = pow(10.0, 0.4 * (-mag - 14.0));

	// NEIGHBORS
	float globalCoeffNeighbor
	    = computeTotalNeighborsOcclusion(vec3(0.0), lightposradius, neighData);
	// END NEIGHBORS

	float mul = globalCoeffNeighbor * irradiance  / pixelSolidAngle;

	f_finalcolor = vec4(mul*color, 1.0);
}
