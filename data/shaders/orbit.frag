#version 150 core

in float multiplier;
uniform vec3 color;
uniform float alpha = 1.0;
uniform float exposure;
uniform float dynamicrange = 10000.0;
out vec4 outColor;

void main()
{
	outColor = vec4(color, clamp(multiplier * alpha, 0.0, 1.0));
	outColor.rgb *= pow(outColor.a, log(dynamicrange) / log(10.0) - 2.0); // ???
	outColor.rgb *= dynamicrange / exposure;
}
