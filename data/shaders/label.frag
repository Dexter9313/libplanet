#version 150 core

in vec2 texCoord;

out vec4 outColor;
uniform sampler2D tex;
uniform float exposure = 1.0;
uniform float dynamicrange = 10000.0;
uniform float alpha = 1.0;

void main()
{
	outColor = texture(tex, texCoord);
	outColor.a *= alpha;
	outColor.rgb *= pow(outColor.a, log(dynamicrange) / log(10.0) - 2.0); // ???
	outColor.rgb *= dynamicrange / exposure;
}
