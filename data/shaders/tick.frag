#version 150 core

uniform vec3 color;
uniform float alpha = 1.0;
uniform float exposure = 1.0;
uniform float dynamicrange = 10000.0;
out vec4 outColor;

void main()
{
	outColor = vec4(color, alpha);
	outColor.rgb *= pow(outColor.a, log(dynamicrange) / log(10.0) - 2.0); // ???
	outColor.rgb *= dynamicrange / exposure;
}
