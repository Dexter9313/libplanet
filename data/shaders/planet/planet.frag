#version 400 core

centroid in vec3 f_position;
in vec4 f_lightrelpos[2];

in vec3 f_tangent;
in vec3 f_normal;

uniform sampler2D diff;
uniform sampler2D norm;
uniform sampler2D specular;
uniform sampler2D night;

uniform vec3 campos;
uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];

uniform sampler2DShadow shadowmap0;
uniform sampler2DShadow shadowmap1;

out vec4 outColor;

#include <planet/ground.glsl>
#include <shadows.glsl>
#include <planet/atmostransforms.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform AtmosphereData atmData;
uniform RingsData ringsData;

void main()
{
	vec3 p    = normalize(f_position);
	float lat = asin(p.z);
	float lon = atan(p.y, p.x);
	vec2 coords = vec2((lon / PI) * 0.5 + 0.5, 1.0 - lat / (PI) -0.5);

	LightingComponents lightComp;
	initLightingComponents(lightComp, coords, diff, norm, specular, night);
	Vertex vertex = Vertex(p, f_normal, f_tangent);
	outColor.rgb = vec3(0.0);
	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		vec4 lightposradius = lightsPosRadius[i];
		vec3 lightillum     = lightsIllum[i];
		vec3 result
		    = getGroundColor(vertex, lightComp, campos, lightposradius, lightillum, 1.0, i == 0, planetData, neighData, atmData, ringsData).rgb;
		if(i == 0)
		{
			result *= computeShadow(f_lightrelpos[0], shadowmap0);
		}
		if(i == 1)
		{
			result *= computeShadow(f_lightrelpos[1], shadowmap1);
		}
		outColor.rgb += result;
	}
	outColor.a   = 1.0;

	if(atmData.H0 != vec2(0.0))
	{
		vec3 pA = campos / planetData.oblateness;
		vec3 pB = p;
		vec2 atmosTexCoord
		    = texCoordFromDir(campos, normalize(pB - pA), planetData.radius, planetData.radius, 0);
		outColor.rgb *= texture(atmData.absorptionLUT, atmosTexCoord).rgb;
		outColor.rgb += max(vec3(0.0), texture(atmData.emissionLUT, atmosTexCoord).rgb);
	}
}
