
vec3 rotateAroundX(float angle, vec3 v)
{
	return vec3(v.x, cos(angle) * v.y - sin(angle) * v.z,
	            cos(angle) * v.z + sin(angle) * v.y);
}

vec3 rotateAroundY(float angle, vec3 v)
{
	return vec3(cos(angle) * v.x + sin(angle) * v.z, v.y,
	            cos(angle) * v.z - sin(angle) * v.x);
}

vec3 rotateAroundZ(float angle, vec3 v)
{
	return vec3(cos(angle) * v.x - sin(angle) * v.y,
	            cos(angle) * v.y + sin(angle) * v.x, v.z);
}

float getMassiveBodyDistanceFromTrueAn(float trueAnomaly, float e, float a)
{
	if(e != 1.0)
	{
		return a * (1.0 - (e * e)) / (1.0 + (e * cos(trueAnomaly)));
	}

	return 2.0 * a / (1.0 + cos(trueAnomaly));
}

float solveForEllipticOrbit(double meanAnomaly, float e)
{
	double eccAn = (e < 0.8 ? meanAnomaly : 3.141592653589793);
	int maxit    = 100;

	for(int i = 0; i < maxit; ++i)
	{
		double f  = eccAn - (e * sin(float(eccAn))) - meanAnomaly;
		double df = 1.0 - (e * cos(float(eccAn)));
		double de = f / df;
		eccAn -= de;
		if(abs(de) < 0.001)
			break;
	}
	return float(eccAn);
}

// scholarius : https://www.shadertoy.com/view/wts3RX
#define NEWTON_ITER 2
#define HALLEY_ITER 1

float cbrt(float x)
{
	float y
	    = sign(x) * uintBitsToFloat(floatBitsToUint(abs(x)) / 3u + 0x2a514067u);

	for(int i = 0; i < NEWTON_ITER; ++i)
		y = (2. * y + x / (y * y)) * .333333333;

	for(int i = 0; i < HALLEY_ITER; ++i)
	{
		float y3 = y * y * y;
		y *= (y3 + 2. * x) / (2. * y3 + x);
	}

	return y;
}

float solveForParabolicOrbit(double meanAnomaly)
{
	float p    = 3.0;
	float q    = -6.0 * float(meanAnomaly);
	float term = sqrt(q * q / 4.0 + p * p * p / 27.0);
	return cbrt(-q / 2.0 + term) + cbrt(-q / 2.0 - term);
}

float solveForHyperbolicOrbit(double meanAnomaly, float e)
{
	double eccAn = 3.141592653589793;
	int maxit    = 100;

	for(int i = 0; i < maxit; ++i)
	{
		double f  = (e * sinh(float(eccAn))) - eccAn - meanAnomaly;
		double df = (e * cosh(float(eccAn))) - 1.0;
		double de = f / df;
		eccAn -= de;
		if(abs(de) < 0.001)
			break;
	}
	return float(eccAn);
}

float getEccentricAnomaly(double meanAnomaly, float e)
{
	if(e < 1.0)
	{
		return solveForEllipticOrbit(meanAnomaly, e);
	}
	if(e == 1.0)
	{
		return solveForParabolicOrbit(meanAnomaly);
	}

	return solveForHyperbolicOrbit(meanAnomaly, e);
}

float getTrueAnomaly(double meanAnomaly, float e)
{
	float eccentricAnomaly = getEccentricAnomaly(meanAnomaly, e);
	if(e < 1.0)
	{
		float coeff = sqrt((1.0 + e) / (1.0 - e));

		return 2.0 * atan(coeff * tan(eccentricAnomaly / 2.0));
	}

	if(e == 1.0)
	{
		return 2.0 * atan(eccentricAnomaly);
	}

	float coeff = sqrt((e + 1.0) / (e - 1.0));

	return 2.0 * atan(coeff * tanh(eccentricAnomaly / 2.0));
}

vec3 getPosFromUT(double ut, float gravParameter, float sma, float ecc,
                  float inc, float anl, float per, float MAepoch)
{
	const double pi = 3.141592653589793;

	double meanAnomaly = MAepoch + ut / sqrt(sma * sma * sma / gravParameter);

	int numberOfOrbits = int(round(meanAnomaly / (2.0 * pi)));

	meanAnomaly -= 2.0 * pi * numberOfOrbits;

	float trueAnomaly = getTrueAnomaly(meanAnomaly, ecc);

	float angle = trueAnomaly + per;

	vec3 dir = vec3(cos(angle), sin(angle), 0.0);
	dir      = rotateAroundX(inc, dir);
	dir      = rotateAroundZ(anl, dir);

	return getMassiveBodyDistanceFromTrueAn(trueAnomaly, ecc, sma) * dir;
}
