#version 400 core

centroid in vec2 f_position;
in vec2 f_camlonlat;
in float f_terrainAngleCoverage;

in vec3 f_tangent;
in vec3 f_normal;

uniform sampler2D diff;
uniform sampler2D norm;
uniform sampler2D specular;
uniform sampler2D night;

uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];
uniform vec3 campos;

out vec4 outColor;

//#define PI 3.14159265359
#include <planet/ground.glsl>
#include <planet/atmostransforms.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform AtmosphereData atmData;
uniform RingsData ringsData;

// computes normalized planet-relative coordinates (0, 0, 1) := north pole; (1, 0, 0) := lon/lat origin
vec3 computePos()
{
	// compute the deltalon/deltalat of this particular fragment from just below camera
	vec2 deltalonlat;

	// if small angles don't compute unstable computations
	if(length(f_position) < 0.08 / f_terrainAngleCoverage
	   || abs(f_position.x) < 0.02 / f_terrainAngleCoverage)
	{
		deltalonlat.y = f_position.y * f_terrainAngleCoverage;
		deltalonlat.x = f_position.x * f_terrainAngleCoverage;
	}
	else
	{
		float angleToCenterRad = length(f_position) * f_terrainAngleCoverage;
		// azimuth := angle above parallel
		float azimuthRad = atan(f_position.y, f_position.x);

		deltalonlat.y = asin(sin(azimuthRad) * sin(angleToCenterRad));
		deltalonlat.x = acos(cos(angleToCenterRad) / cos(deltalonlat.y));

		if(abs(azimuthRad) > PI / 2.0)
		{
			deltalonlat.x *= -1.0;
		}
	}

	// first consider camlonlat = 0.0
	vec3 pos = vec3(cos(deltalonlat.y) * cos(deltalonlat.x),
	                cos(deltalonlat.y) * sin(deltalonlat.x), sin(deltalonlat.y));

	// rotate to match lat
	pos.xz = vec2(cos(f_camlonlat.y) * pos.x - sin(f_camlonlat.y) * pos.z,
	              cos(f_camlonlat.y) * pos.z + sin(f_camlonlat.y) * pos.x);
	// rotate to match lon
	pos.xy = vec2(cos(f_camlonlat.x) * pos.x - sin(f_camlonlat.x) * pos.y,
	              cos(f_camlonlat.x) * pos.y + sin(f_camlonlat.x) * pos.x);

	return pos;
}

void main()
{

	vec3 pos = computePos();
	float lat = asin(pos.z);
	float lon = atan(pos.y, pos.x);
	vec2 coords = vec2((lon / PI) * 0.5 + 0.5, 1.0 - lat / (PI) -0.5);

	LightingComponents lightComp;
	initLightingComponents(lightComp, coords, diff, norm, specular, night);
	Vertex vertex = Vertex(pos, f_normal, f_tangent);
	outColor = vec4(0.0);
	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		outColor += getGroundColor(vertex, lightComp, campos, lightsPosRadius[i],
		                           lightsIllum[i], 0.0, i == 0, planetData, neighData, atmData, ringsData);
	}

	if(atmData.H0 != vec2(0.0))
	{
		vec3 pA = campos / planetData.oblateness;
		vec3 pB = planetData.radius * normalize(pos);
		vec2 atmosTexCoord
		    = texCoordFromDir(campos, normalize(pB - pA), planetData.radius, planetData.radius, 0);
		outColor.rgb *= texture(atmData.absorptionLUT, atmosTexCoord).rgb;
		outColor.rgb
		    += max(vec3(0.0), texture(atmData.emissionLUT, atmosTexCoord).rgb);
	}
}
