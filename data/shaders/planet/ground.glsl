#ifndef GROUND
#define GROUND

#include <planet/neighborOcclusion.glsl>
#include <planet/scattering.glsl>

// example, you can populate lightComp in any other way
void initLightingComponents(out LightingComponents lightComp, vec2 coord, in sampler2D diffuse, in sampler2D normal, in sampler2D specular, in sampler2D emissive)
{
	if(coord.x < 0.001 || coord.x > 0.999 || coord.y < 0.1 || coord.y > 0.9)
	{
		// OR max(0.0, textureQueryLod(diffuse, coord).x - 10.0)
		float lod          = min(3.0, textureQueryLod(diffuse, coord).x);
		lightComp.diffuse  = textureLod(diffuse, coord, lod).rgb;
		lod                = min(3.0, textureQueryLod(normal, coord).x);
		lightComp.normal   = textureLod(normal, coord, lod).xyz;
		lod                = min(3.0, textureQueryLod(specular, coord).x);
		lightComp.specular = textureLod(specular, coord, lod).rgb;
		lod                = min(3.0, textureQueryLod(emissive, coord).x);
		lightComp.emissive = textureLod(emissive, coord, lod).rgb;
	}
	else
	{
		lightComp.diffuse  = texture(diffuse, coord).rgb;
		lightComp.normal   = texture(normal, coord).xyz;
		lightComp.specular = texture(specular, coord).rgb;
		lightComp.emissive = texture(emissive, coord).rgb;
	}
	lightComp.normal    = normalize(lightComp.normal * 2.0 - 1.0); // from [0;1] to [-1;1]
	lightComp.normal.y *= -1.0;
	lightComp.emissive *= 0.5;
}

vec4 getGroundColor(in Vertex vertex, in LightingComponents lightComp, vec3 campos, vec4 lightposradius, vec3 lightillum,
                    float useShadowMapping, bool emitNight, in PlanetData planetData, in NeighborsData neighData, in AtmosphereData atmData, in RingsData ringsData)
{
	vec3 pos           = planetData.radius * normalize(vertex.position) * planetData.oblateness;
	vec3 norm_pos      = normalize(vertex.position / planetData.oblateness);
	vec3 posRelToLight = pos - lightposradius.xyz;

	vec3 lightdir = -1.0 * normalize(posRelToLight);

	vec3 albedo = lightComp.diffuse;
	vec3 normal = lightComp.normal;
	vec3 spec = lightComp.specular;
	vec3 nightcolor = lightComp.emissive;
	albedo *= planetData.albedoCoeff;

	mat3 fromtangentspace;
	fromtangentspace[2] = normalize(vertex.normal);
	if(length(vertex.tangent) == 0.0)
	{
		fromtangentspace[0] = cross(vec3(0.0, 0.0, 1.0), fromtangentspace[2]);
	}
	else
	{
		fromtangentspace[0] = normalize(vertex.tangent);
	}

	// Gram-Schmidt re-orthogonalize T with respect to N
	fromtangentspace[0] = normalize(
	    fromtangentspace[0]
	    - dot(fromtangentspace[0], fromtangentspace[2]) * fromtangentspace[2]);

	fromtangentspace[1] = cross(fromtangentspace[2], fromtangentspace[0]);
	normal = fromtangentspace * normal;

	// 0 or 1
	// if no shadow mapping, avoids lighting stuff behind the planet
	float coeff_pos = mix(dot(lightdir, norm_pos), 1.0, useShadowMapping);
	float coeff     = max(0.0, dot(lightdir, normal));

	// NEIGHBORS
	float globalCoeffNeighbor
	    = computeTotalNeighborsOcclusion(pos, lightposradius, neighData);
	// END NEIGHBORS

	// RINGS SHADOW
	float coeffRings = 1.0;
	if(ringsData.outer > 0.0 && sign(lightdir.z) != sign(pos.z))
	{
		vec3 pointOnRings = pos + lightdir * abs(pos.z / lightdir.z);
		float alt         = length(pointOnRings);
		if(alt >= ringsData.inner && alt <= ringsData.outer)
		{
			float texCoord = (alt - ringsData.inner) / (ringsData.outer - ringsData.inner);
			coeffRings     = 1.0 - texture(ringsData.tex, vec2(texCoord, 0.5)).a;
		}
	}
	// END RINGS SHADOW

	float global_coeff = coeff * min(1.0, max(0.0, coeff_pos + 0.1) * 10.0);
	global_coeff *= coeffRings * globalCoeffNeighbor;
	vec3 result = global_coeff * albedo;

	// Oren-Nayar
	float roughnesssq = planetData.roughness * planetData.roughness;
	float A           = 1.0 - 0.5 * roughnesssq / (roughnesssq + 0.33);
	float B           = 0.45 * roughnesssq / (roughnesssq + 0.09);
	float thetai      = acos(dot(normal, lightdir));
	float thetar      = acos(dot(normal, normalize(campos - pos)));
	float alpha       = max(thetai, thetar);
	float beta        = min(thetai, thetar);
	float cosphiiMinusPhir
	    = dot(normalize(lightdir - normal * dot(lightdir, normal)),
	          normalize(campos - pos - normal * dot(campos - pos, normal)));
	float orenNayarCoeff
	    = (A + (B * max(0.0, cosphiiMinusPhir) * sin(alpha) * tan(beta)));

	result *= orenNayarCoeff * lightillum / 3.1415;

	// specular
	vec3 viewDir    = normalize(campos - pos);
	vec3 reflectDir = reflect(-1.0 * normalize(lightposradius.xyz), normal);
	float s         = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	result += s * lightillum * max(0.0, coeff) * spec * 0.1;

	result *= global_coeff;

	// ATMOSPHERE
	if(atmData.H0 != vec2(0.0))
	{
		vec3 l = normalize(lightposradius.xyz / planetData.oblateness);

		vec3 pA = campos / planetData.oblateness;
		vec3 pB = pos / planetData.oblateness;

		vec3 dir = normalize(pB - pA);

		vec2 res = sphereIntersect(pA, dir, atmData.radius);
		if(res.x > 0.0)
		{
			pA += dir * res.x;
		}

		result *= tPreproc(pB, l, planetData.radius, atmData);
	}

	if(emitNight)
	{
		result += nightcolor; // emissive
	}

	return vec4(result, 1.0);
}
#endif
