#version 150 core

in vec3 position;

uniform mat4 camera;

out vec3 f_position;

#include <planet/structs.glsl>

uniform PlanetData planetData;
uniform AtmosphereData atmData;

void main()
{
	f_position  = normalize(position);

	gl_Position = camera * vec4((atmData.radius / planetData.radius) * normalize(position)*planetData.oblateness, 1.0);
}
