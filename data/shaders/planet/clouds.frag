#version 400 core

centroid in vec3 f_position;

uniform float cloudsHeight;

uniform vec3 campos;
uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];

uniform sampler2D tex;
uniform sampler2D night;

layout(location = 0, index = 0) out vec4 outColor;
layout(location = 0, index = 1) out vec4 absorption;

#include <planet/neighborOcclusion.glsl>
#include <planet/scattering.glsl>
#include <planet/atmostransforms.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform AtmosphereData atmData;

float phaseC(float mu)
{
	const float PI = 3.1415;
	const float g  = 0.2;
	float gg       = g * g;
	return (3.0 * (1.0 - gg) * (1.0 + mu * mu))
	       / (8.0 * PI * (2.0 + gg) * pow(1.0 + gg - 2.0 * g * mu, 1.5));
}

void main()
{
	vec3 pos       = normalize(f_position);
	vec3 cloudsPos = (planetData.radius + cloudsHeight) * pos;

	float lat = asin(pos.z);
	float lon = atan(pos.y, pos.x) + PI;
	if(lon > PI)
	{
		lon -= 2.0 * PI;
	}
	vec2 coords = vec2((lon / PI) * 0.5 + 0.5, 1.0 - lat / (PI) -0.5);

	float alpha;

	if(coords.x < 0.001 || coords.x > 0.999 || coords.y < 0.1 || coords.y > 0.9)
	{
		float lod = min(2.0, textureQueryLod(tex, coords).x);
		alpha     = textureLod(tex, coords, lod).r;
	}
	else
	{
		alpha = texture(tex, coords).r;
	}

	/*
	    // https://openweathermap.org/map_legend
	    // Mercator
	    coords.y = log(tan(PI/4.0 + lat/2.0)) / PI;

	    coords.y = coords.y*0.5 + 0.5;
	    coords.y = 1.0 - coords.y;

	    vec4 c = texture(tex, coords);
	    float alpha = c.a;
	    if(alpha >= 0.5)
	    {
	        if(c.r >= 244.0 / 255.0)
	        {
	            alpha = 0.5 + (alpha-0.5)*0.4;
	        }
	        else
	        {
	            float r = c.r * 255.0;
	            float g = c.g * 255.0;
	            float coeff = (r+g)*0.5;
	            coeff -= 240.0;
	            coeff /= 4.0;
	            coeff = 1.0 - coeff;
	            alpha = 0.7 + 0.3*coeff;
	        }
	    }
	*/
	if(alpha == 0.0)
	{
		discard;
	}

	outColor = vec4(0.0);
	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		vec4 lightposradius = lightsPosRadius[i];
		vec3 lightillum     = lightsIllum[i];

		vec3 l = normalize(lightposradius.xyz / planetData.oblateness);

		vec3 pA = campos / planetData.oblateness;
		vec3 pB = cloudsPos;

		vec3 dir = normalize(pB - pA);

		vec2 res = sphereIntersect(pA, dir, atmData.radius);
		if(res.x > 0.0)
		{
			pA += dir * res.x;
		}

		vec3 result
		    = lightillum
		      * tPreproc(pB, l, planetData.radius, atmData)
		      * computeTotalNeighborsOcclusion(cloudsPos, lightposradius, neighData);

		outColor.rgb += result * abs(dot(normalize(pB), dir)) / 2.0; //* phaseC(dot(l, dir));
	}

	outColor *= alpha;

	vec3 pA = campos / planetData.oblateness;
	vec3 pB = cloudsPos;
	vec2 atmosTexCoord
	    = texCoordFromDir(campos, normalize(pB - pA), planetData.radius, planetData.radius, 0);
	outColor.rgb *= texture(atmData.absorptionLUT, atmosTexCoord).rgb;
	outColor.rgb += max(vec3(0.0), texture(atmData.emissionLUT, atmosTexCoord).rgb) * alpha;

	outColor.rgb += textureLod(night, coords,
	                           textureQueryLod(tex, coords).x + int(alpha * 8))
	                    .rgb
	                * 0.5 * alpha;

	absorption = vec4(1.0 - alpha);
}
