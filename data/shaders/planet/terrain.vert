#version 150 core

in vec2 position;

uniform mat4 camera;

uniform vec3 campos;

out vec2 f_position;
out vec2 f_camlonlat;
out float f_terrainAngleCoverage;

out vec3 f_tangent;
out vec3 f_normal;

#define PI 3.14159265359

#include <planet/structs.glsl>

uniform PlanetData planetData;

// computes normalized planet-relative coordinates (0, 0, 1) := north pole; (1, 0, 0) := lon/lat origin
vec3 computePos()
{
	// compute the deltalon/deltalat of this particular fragment from just below camera
	vec2 deltalonlat;

	// if small angles don't compute unstable computations
	if(length(f_position) < 0.08 / f_terrainAngleCoverage
	   || abs(f_position.x) < 0.02 / f_terrainAngleCoverage)
	{
		deltalonlat.y = f_position.y * f_terrainAngleCoverage;
		deltalonlat.x = f_position.x * f_terrainAngleCoverage;
	}
	else
	{
		float angleToCenterRad = length(f_position) * f_terrainAngleCoverage;
		// azimuth := angle above parallel
		float azimuthRad = atan(f_position.y, f_position.x);

		deltalonlat.y = asin(sin(azimuthRad) * sin(angleToCenterRad));
		deltalonlat.x = acos(cos(angleToCenterRad) / cos(deltalonlat.y));

		if(abs(azimuthRad) > PI / 2.0)
		{
			deltalonlat.x *= -1.0;
		}
	}

	// first consider camlonlat = 0.0
	vec3 pos = vec3(cos(deltalonlat.y) * cos(deltalonlat.x),
	                cos(deltalonlat.y) * sin(deltalonlat.x), sin(deltalonlat.y));

	// rotate to match lat
	pos.xz = vec2(cos(f_camlonlat.y) * pos.x - sin(f_camlonlat.y) * pos.z,
	              cos(f_camlonlat.y) * pos.z + sin(f_camlonlat.y) * pos.x);
	// rotate to match lon
	pos.xy = vec2(cos(f_camlonlat.x) * pos.x - sin(f_camlonlat.x) * pos.y,
	              cos(f_camlonlat.x) * pos.y + sin(f_camlonlat.x) * pos.x);

	return pos;
}

void main()
{
	vec3 ncp  = normalize(campos);
	f_camlonlat.x = atan(ncp.y, ncp.x);
	f_camlonlat.y = asin(ncp.z);

	f_terrainAngleCoverage = 2.0 * acos(min(0.999, planetData.radius / length(campos)));

	f_position = position - 0.5;

	vec3 pos = computePos();

	gl_Position = camera * vec4(pos * planetData.oblateness, 1.0);

	f_normal  = normalize(pos / planetData.oblateness);
	f_tangent   = cross(vec3(0.0, 0.0, 1.0), f_normal);
}
