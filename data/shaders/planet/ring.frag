#version 150 core

in vec2 f_position;

uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];

out vec4 outColor;

#include <planet/neighborOcclusion.glsl>
#include <planet/structs.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform RingsData ringsData;

void main()
{
	// make perfect circles
	if(length(f_position) < ringsData.inner || length(f_position) > ringsData.outer)
		discard;

	float alt      = length(f_position);
	float texCoord = (alt - ringsData.inner) / (ringsData.outer - ringsData.inner);

	vec3 pos = vec3(f_position, 0.0);

	vec4 diffuse = texture(ringsData.tex, vec2(texCoord, 0.5));
	outColor.a   = diffuse.a;
	outColor.rgb = vec3(0.0);

	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		vec4 lightposradius = lightsPosRadius[i];
		vec3 lightillum     = lightsIllum[i];
		vec3 posRelToLight  = pos - lightposradius.xyz;

		vec3 lightdir = -1.0 * normalize(posRelToLight);

		vec3 closestPoint = dot(lightdir, -1 * pos) * lightdir + pos;

		closestPoint /= planetData.oblateness;

		float coeff = 1.0;
		if(dot(lightdir, pos) < 0.0)
		{
			coeff -= getNeighborOcclusionFactor(
			    lightposradius.w * length(pos) / length(posRelToLight),
			    planetData.radius, length(closestPoint));
		}

		// NEIGHBORS
		float globalCoeffNeighbor
		    = computeTotalNeighborsOcclusion(pos, lightposradius, neighData);
		// END NEIGHBORS

		vec3 result = diffuse.rgb;
		result *= coeff * globalCoeffNeighbor;
		result *= lightillum / 3.1415;

		outColor.rgb += result;
	}
}
