#version 330 core

// Conventions From :
// https://developer.nvidia.com/gpugems/GPUGems2/gpugems2_chapter16.html

centroid in vec3 f_position;

uniform vec3 campos;
uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];

uniform sampler2D emissionLUT;
uniform sampler2D absorptionLUT;

layout(location = 0, index = 0) out vec4 outColor;
layout(location = 0, index = 1) out vec4 absorption;

const float PI = 3.14159265359;

#include <planet/atmostransforms.glsl>
#include <planet/structs.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform AtmosphereData atmData;

void main()
{
	vec3 pA = campos / planetData.oblateness;
	vec3 pB = atmData.radius * normalize(f_position);

	vec2 texCoord = texCoordFromDir(campos, normalize(pB - pA), atmData.radius, planetData.radius, 1);

	outColor   = max(vec4(0.0), texture(emissionLUT, texCoord));
	absorption = texture(absorptionLUT, texCoord);
}
