#version 150 core

uniform float opacity = 1.0;
uniform vec3 color = vec3(1.0, 0.0, 0.0);

out vec4 outColor;

void main()
{
	outColor = vec4(20000.0 * color, opacity);
}
