#ifndef SCATTERING
#define SCATTERING

#include <planet/structs.glsl>
#include <planet/neighborOcclusion.glsl>

#ifndef NUM_IN_SCATTER
#define NUM_IN_SCATTER 20
#endif

const float PI = 3.14159265359;

bool isIntersectingSphere(vec3 p, vec3 dir_norm, float radius)
{
	float b_over_2 = dot(p, dir_norm);
	float c        = dot(p, p) - radius * radius;
	float d_over_4 = b_over_2 * b_over_2 - c;

	return d_over_4 >= 0.0 && d_over_4 > b_over_2 * b_over_2 * sign(b_over_2);
}

// returns alpha0 and alpha1 such that alpha0 < alpha1
// and p+alphai*dir_norm is on the sphere of radius radius centered on 0,0,0
// make sure dir_norm is normalized
vec2 sphereIntersect(vec3 p, vec3 dir_norm, float radius)
{
	float b_over_2 = dot(p, dir_norm);
	float c        = dot(p, p) - radius * radius;
	float d_over_4 = b_over_2 * b_over_2 - c;

	float d = sqrt(max(0.0, d_over_4));
	return vec2(-b_over_2 - d, -b_over_2 + d);
}

vec3 densityIntegralPreproc(vec3 p, vec3 lightdir, float planetRadius,
                            float atmRadius, sampler2D preproctex)
{
	vec2 uv;
	uv.x = (length(p) - planetRadius) / (atmRadius - planetRadius);
	uv.y = acos(clamp(dot(normalize(p), lightdir), -1.0, 1.0)) / PI;

	return exp(texture(preproctex, uv).rgb);
}

// opticalDepth(pA->pB) = opticalDepth(pA->Sun) - opticalDepth(pB->Sun)
vec3 subDensityIntegralPreproc(vec3 pA, vec3 pB, float planetRadius,
                               float atmRadius, sampler2D preproctex)
{
	vec3 dir = normalize(pB - pA);
	// revert ray if dir will hit the planet
	float coeff = normalize(dot(normalize(pA), dir));
	vec3 result = densityIntegralPreproc(pA, coeff * dir, planetRadius,
	                                     atmRadius, preproctex);
	result -= densityIntegralPreproc(pB, coeff * dir, planetRadius, atmRadius,
	                                 preproctex);
	return coeff * result;
}

vec3 tPreproc(vec3 p, vec3 lightdir, float planetRadius, in AtmosphereData atmData)
{
	vec3 di = densityIntegralPreproc(p, lightdir, planetRadius, atmData.radius,
	                                 atmData.preproc);
	return exp(-1.0 * (atmData.beta0[0] * di[0] + atmData.beta0[1] * di[1] + atmData.beta0[2] * di[2]));
}

vec3 subTPreproc(vec3 pA, vec3 pB, float planetRadius, float atmRadius,
                 vec3 beta0[3], sampler2D preproctex)
{
	vec3 di = subDensityIntegralPreproc(pA, pB, planetRadius, atmRadius,
	                                    preproctex);
	return exp(-1.0 * (beta0[0] * di[0] + beta0[1] * di[1] + beta0[2] * di[2]));
}

vec2 density(float negalt, vec2 H0)
{
	return exp(negalt / H0);
}

float ozoneDensity(vec2 O3layer, vec3 p, float planetRadius)
{
	return max(
	    0.0, 1.0 - abs(length(p) - planetRadius - O3layer.x) * 2.0 / O3layer.y);
}

void scatteringIntegralPreproc(in AtmosphereData atmData, in PlanetData planetData, in NeighborsData neighData, inout vec3 integral[2], vec3 pA, vec3 pB,
                               vec4 lightposradius)
{
	vec3 lightdir = normalize(lightposradius.xyz / planetData.oblateness);

	vec3 ds = (pB - pA) / float(NUM_IN_SCATTER);
	vec3 v  = pA + ds * 0.5;

	integral = vec3[](vec3(0.0), vec3(0.0));

	// when we compute density integral P(n)->PA, we already know it from
	// P(n-1)->PA so compute only the contribution of P(n)->P(n-1)
	vec3 accumulativeDensityIntegral
	    = subDensityIntegralPreproc(v, pA, planetData.radius, atmData.radius, atmData.preproc);

	vec3 beta0neg[3]
	    = vec3[](-1.0 * atmData.beta0[0], -1.0 * atmData.beta0[1], -1.0 * atmData.beta0[2]);
	for(int i = 0; i < NUM_IN_SCATTER; i++)
	{
		vec3 totalDensInt = densityIntegralPreproc(v, lightdir, planetData.radius,
		                                           atmData.radius, atmData.preproc)
		                    + accumulativeDensityIntegral;

		vec3 coeff1
		    = exp(beta0neg[0] * totalDensInt[0] + beta0neg[1] * totalDensInt[1]
		          + beta0neg[2] * totalDensInt[2]);

		vec2 coeff2 = density(planetData.radius - length(v), atmData.H0)
		              * computeTotalNeighborsOcclusion(v, lightposradius, neighData);

		integral[0] += coeff1 * coeff2[0];
		integral[1] += coeff1 * coeff2[1];

		v += ds;
		accumulativeDensityIntegral
		    += length(ds)
		       * vec3(density(planetData.radius - length(v), atmData.H0),
		              ozoneDensity(atmData.O3layer, v, planetData.radius));
	}

	integral[0] *= length(ds);
	integral[1] *= length(ds);
}

float phaseR(float mu)
{
	return (3.0 + 3.0 * mu * mu) / (16.0 * PI);
}

float phaseM(float mu)
{
	const float g = 0.76;
	float gg      = g * g;
	return (3.0 * (1.0 - gg) * (1.0 + mu * mu))
	       / (8.0 * PI * (2.0 + gg) * pow(1.0 + gg - 2.0 * g * mu, 1.5));
}

vec3 fullScatteringPreproc(in AtmosphereData atmData, in PlanetData planetData, in NeighborsData neighData, vec3 sunLight, vec3 pA, vec3 pB,
                           vec4 lightposradius)
{
	vec3 lightdir = normalize(lightposradius.xyz / planetData.oblateness);

	vec3 integral[2];
	scatteringIntegralPreproc(atmData, planetData, neighData, integral, pA, pB,
	                          lightposradius);
	integral[0]
	    *= sunLight * atmData.beta0[0] * phaseR(dot(lightdir, normalize(pB - pA)));
	integral[1] *= sunLight * 0.5 * atmData.beta0[1]
	               * phaseM(dot(lightdir, normalize(pB - pA)));

	return integral[0] + integral[1];
}
#endif
