#ifndef STRUCTS
#define STRUCTS

struct Vertex
{
	vec3 position;
	vec3 normal;
	vec3 tangent;
};

struct LightingComponents
{
	vec3 diffuse;
	vec3 normal;
	vec3 specular;
	vec3 emissive;
};

struct PlanetData
{
	float radius;
	vec3 oblateness;
	float albedoCoeff;
	float roughness;
};

struct NeighborsData
{
	vec4 posRadius[5];
	vec3 oblateness[5];
};

struct AtmosphereData
{
	float radius;
	vec2 H0;
	vec2 O3layer;
	vec3 beta0[3];

	sampler2D preproc;
	sampler2D emissionLUT;
	sampler2D absorptionLUT;
};

struct RingsData
{
	float inner;
	float outer;
	sampler2D tex;
};

#endif
