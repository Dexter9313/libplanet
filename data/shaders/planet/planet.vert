#version 150 core

in vec3 position;
in vec3 normal;

uniform mat4 camera;
uniform mat4 lightspace0;
uniform mat4 lightspace1;
uniform vec3 oblateness = vec3(1.0, 1.0, 1.0);
uniform float boundingSphereRadius;

out vec3 f_position;
out vec3 f_tangent;
out vec3 f_normal;
out vec4 f_lightrelpos[2];

#include <shadows.glsl>
#include <planet/structs.glsl>

uniform PlanetData planetData;

void main()
{
	f_position = position;

	if(length(normal) == 0.0)
		f_normal = normalize(position / planetData.oblateness);
	else
		f_normal = normal;
	f_tangent = cross(vec3(0.0, 0.0, 1.0), f_normal);

	gl_Position   = camera * vec4(position * planetData.oblateness, 1.0);
	f_lightrelpos[0] = computeLightSpacePosition(lightspace0, position, normal,
	                                          boundingSphereRadius);
	f_lightrelpos[1] = computeLightSpacePosition(lightspace1, position, normal,
	                                          boundingSphereRadius);
}
