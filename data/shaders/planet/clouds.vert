#version 150 core

in vec3 position;

uniform mat4 camera;

uniform float cloudsHeight;

out vec3 f_position;

#include <planet/structs.glsl>

uniform PlanetData planetData;

void main()
{
	f_position = normalize(position);

	gl_Position = camera 
	              * vec4((1.0 + (cloudsHeight / planetData.radius))
	                         * normalize(position) * planetData.oblateness,
	                     1.0);
}
