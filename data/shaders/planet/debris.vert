#version 400 core

in float sma;
in float ecc;
in float inc;
in float anl;
in float per;
in float MAepoch;

uniform mat4 camera;

uniform float high_ut;
uniform float low_ut;
uniform float gravParam;

uniform float planetRadius;

#include <planet/orbit.glsl>

void main()
{
	double ut = high_ut;
	ut *= 1.0e6;
	ut += low_ut;
	vec3 position = getPosFromUT(ut, gravParam, sma, ecc, inc, anl, per, MAepoch) / planetRadius;
	gl_Position = camera * vec4(position, 1.0);
}
