#version 400 core

#include <terrain/terrain.glsl>

#define MAX_LIGHTS 2
uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];
uniform vec3 campos;

out vec4 outColor;

#include <planet/ground.glsl>
#include <planet/atmostransforms.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;
uniform AtmosphereData atmData;
uniform RingsData ringsData;

void main()
{
	LightingComponents lightComp;
	lightComp.diffuse = getTerrainCurrentDiffuseColor();
	lightComp.normal = vec3(0.0, 0.0, 1.0);
	lightComp.specular = vec3(0.0);
	lightComp.emissive = vec3(0.0);

	vec3 pos = getTerrainCurrentPosition();
	Vertex vertex = Vertex(pos, getTerrainCurrentNormal(), getTerrainCurrentTangent());
	outColor = vec4(0.0);
	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		outColor += getGroundColor(vertex, lightComp, campos, lightsPosRadius[i],
		                           lightsIllum[i], 0.0, i == 0, planetData, neighData, atmData, ringsData);
	}

	if(atmData.H0 != vec2(0.0))
	{
		vec3 pA = campos / planetData.oblateness;
		vec3 pB = planetData.radius * normalize(pos);
		vec2 atmosTexCoord
		    = texCoordFromDir(campos, normalize(pB - pA), planetData.radius, planetData.radius, 0);
		outColor.rgb *= texture(atmData.absorptionLUT, atmosTexCoord).rgb;
		outColor.rgb
		    += max(vec3(0.0), texture(atmData.emissionLUT, atmosTexCoord).rgb);
	}
}
