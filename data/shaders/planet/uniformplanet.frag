#version 150 core

in vec3 f_position;

uniform vec3 color;
uniform vec4 lightsPosRadius[MAX_LIGHTS];
uniform vec3 lightsIllum[MAX_LIGHTS];

out vec4 outColor;

#include <planet/neighborOcclusion.glsl>
#include <planet/structs.glsl>

uniform PlanetData planetData;
uniform NeighborsData neighData;

void main()
{
	vec3 pos   = planetData.radius * normalize(f_position) * planetData.oblateness;
	vec3 norm  = normalize(normalize(f_position) / planetData.oblateness);

	outColor   = vec4(0.0);
	outColor.a = 1.0;
	for(int i = 0; i < lightsPosRadius.length() && lightsPosRadius[i].w > 0.0;
	    ++i)
	{
		vec4 lightposradius = lightsPosRadius[i];
		vec3 lightillum     = lightsIllum[i];

		vec3 posRelToLight = pos - lightposradius.xyz;

		vec3 lightdir = -1.0 * normalize(posRelToLight);

		float coeff = max(0.0, dot(lightdir, norm));

		// NEIGHBORS
		float globalCoeffNeighbor
		    = computeTotalNeighborsOcclusion(pos, lightposradius, neighData);
		// END NEIGHBORS
		vec3 result = color;
		result *= coeff * globalCoeffNeighbor;
		result *= lightillum / 3.1415;
		outColor.rgb += result;
	}
}
