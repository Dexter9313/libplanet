

float signed_square(float x)
{
	return sign(x) * x * x;
}

float signed_sqrt(float x)
{
	return sign(x) * sqrt(abs(x));
}

vec3 dirFromTexCoord(vec3 campos, vec2 texCoord, float renderRadius, float planetRadius, int intersectIndex)
{
	float horizonLat
	    = asin(clamp(planetRadius / length(campos), 0.0, 1.0)) - (PI / 2.0);

	float minLat = mix(-PI / 2.0, horizonLat, float(intersectIndex));
	float maxLat
	    = mix(asin(clamp(renderRadius / length(campos), 0.0, 1.0)) - (PI / 2.0),
	          PI / 2.0, float(intersectIndex));

	float horizonYTexCoord = (horizonLat - minLat) / (maxLat - minLat);

	texCoord.y = (signed_square(horizonYTexCoord)
	              + signed_square(texCoord.y - horizonYTexCoord))
	             / (signed_square(1.0 - horizonYTexCoord)
	                + signed_square(horizonYTexCoord));

	vec2 lonlat
	    = vec2(2.0 * PI * texCoord.x, (maxLat - minLat) * texCoord.y + minLat);

	vec3 local_dir
	    = normalize(vec3(cos(lonlat.x) * cos(lonlat.y),
	                     sin(lonlat.x) * cos(lonlat.y), sin(lonlat.y)));

	vec3 localZ = normalize(campos);
	vec3 localY = normalize(cross(localZ, vec3(0.0, 0.0, 1.0)));
	vec3 localX = normalize(cross(localY, localZ));

	mat4 localToGlobal;
	localToGlobal[0] = vec4(localX, 0.0);
	localToGlobal[1] = vec4(localY, 0.0);
	localToGlobal[2] = vec4(localZ, 0.0);
	localToGlobal[3] = vec4(0.0, 0.0, 0.0, 1.0);

	return vec3(localToGlobal * vec4(local_dir, 0.0));
}

vec2 texCoordFromDir(vec3 campos, vec3 dir, float renderRadius, float planetRadius, int intersectIndex)
{
	vec3 localZ = normalize(campos);
	vec3 localY = normalize(cross(localZ, vec3(0.0, 0.0, 1.0)));
	vec3 localX = normalize(cross(localY, localZ));

	mat4 localToGlobal;
	localToGlobal[0] = vec4(localX, 0.0);
	localToGlobal[1] = vec4(localY, 0.0);
	localToGlobal[2] = vec4(localZ, 0.0);
	localToGlobal[3] = vec4(0.0, 0.0, 0.0, 1.0);

	vec3 dir_local = normalize(vec3(inverse(localToGlobal) * vec4(dir, 0.0)));

	vec2 lonlat
	    = vec2(atan(dir_local.y, dir_local.x), 0.5 * PI - acos(dir_local.z));
	if(lonlat.x < 0.0)
		lonlat.x += 2.0 * PI;

	float horizonLat
	    = asin(clamp(planetRadius / length(campos), 0.0, 1.0)) - (PI / 2.0);

	float minLat = mix(-PI / 2.0, horizonLat, float(intersectIndex));
	float maxLat
	    = mix(asin(clamp(renderRadius / length(campos), 0.0, 1.0)) - (PI / 2.0),
	          PI / 2.0, float(intersectIndex));

	float horizonYTexCoord = (horizonLat - minLat) / (maxLat - minLat);

	vec2 texCoord
	    = vec2(lonlat.x * 0.5 / PI, (lonlat.y - minLat) / (maxLat - minLat));

	texCoord.y = horizonYTexCoord
	             + signed_sqrt(texCoord.y
	                               * (signed_square(1.0 - horizonYTexCoord)
	                                  + signed_square(horizonYTexCoord))
	                           - signed_square(horizonYTexCoord));

	return texCoord;
}
