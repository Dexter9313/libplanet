#version 150 core

centroid in vec3 f_position;

uniform float temperature;
uniform float time;
uniform float seed;
uniform float exposure;
uniform float dynamicrange = 10000.0;

// LIMB DARKENING UNIFORMS
uniform vec3 oblateness = vec3(1.0, 1.0, 1.0);
uniform float starRadius;
uniform vec3 campos;

uniform sampler1D blackbody;
// {min, max}
uniform vec2 blackbodyBoundaries;

out vec4 outColor;

#include <planet/gentex/simplex4DNoise.glsl>

//// BEG BLACKBODY
vec4 getTempColorShift(float temperature)
{
	return vec4(temperature * (0.0534 / 255.0) - (43.0 / 255.0),
	            temperature * (0.0628 / 255.0) - (77.0 / 255.0),
	            temperature * (0.0735 / 255.0) - (115.0 / 255.0), 0.0);
}

float efficacy(float T)
{
	// Lampret, V., Peternelj, J., & Krainer, A. (2002). Luminous flux and
	// luminous efficacy of black-body radiation: an analytical approximation.
	// Solar Energy, 73(5), 319–326.
	float alpha[6]
	    = float[6](-5.946, 1.3634e-2, -1.0336e-5, 2.60573e-9, 0.0, 0.0);
	float beta[6] = float[6](0.91051, -3.556e-4, 7.255e-8, 5.415e-12,
	                         -1.4195e-15, 2.7227e-19);

	float a = 0.0;
	float b = 0.0;
	for(int i = 0; i < 6; ++i)
	{
		a += alpha[i] * pow(T, i);
		b += beta[i] * pow(T, i);
	}
	return max(a / b, 0.0);
}

float defficacy(float T)
{
	// Lampret, V., Peternelj, J., & Krainer, A. (2002). Luminous flux and
	// luminous efficacy of black-body radiation: an analytical approximation.
	// Solar Energy, 73(5), 319–326.
	float alpha[6]
	    = float[6](-5.946, 1.3634e-2, -1.0336e-5, 2.60573e-9, 0.0, 0.0);
	float beta[6] = float[6](0.91051, -3.556e-4, 7.255e-8, 5.415e-12,
	                         -1.4195e-15, 2.7227e-19);

	float a  = 0.0;
	float b  = 0.0;
	float da = 0.0;
	float db = 0.0;
	for(int i = 0; i < 6; ++i)
	{
		a += alpha[i] * pow(T, i);
		da += alpha[i] * i * pow(T, i - 1);
		b += beta[i] * pow(T, i);
		db += beta[i] * i * pow(T, i - 1);
	}
	return (b * da - a * db) / (b * b);
}

// Stefan - Boltzmann
float radiance(float T)
{
	return pow(T, 4) * 5.670373e-8 / 3.1415926535;
}

float dradiance(float T)
{
	return pow(T, 3) * 4.0 * 5.670373e-8 / 3.1415926535;
}

float luminance(float T)
{
	return efficacy(T) * radiance(T);
}

float dluminance(float T)
{
	return efficacy(T) * dradiance(T) + defficacy(T) * radiance(T);
}

float tempfromluminance(float L)
{
	// initial guess
	float x0 = L > 565.0e4 ? 5000.0 : 2000.0;
	float l  = luminance(x0);
	int i    = 0;
	// Newton-Raphson method
	while(abs(l - L) / L > 3.0e-2 && i < 7)
	{
		i += 1;
		x0 = x0 - (l - L) / dluminance(x0);
		l  = luminance(x0);
	}
	return x0;
}

//// END BLACKBODY

float max3(vec3 v)
{
	return max(max(v.x, v.y), v.z);
}

float changeRange(float x, float a, float b, float c, float d)
{
	return c + (d - c) / (b - a) * (x - a);
}

// https://en.wikipedia.org/wiki/Limb_darkening
float limb_darkening(float cospsy)
{
	// based on Sun, we don't really have data for other stars
	return 0.3 + 0.93 * cospsy - 0.23 * cospsy * cospsy;
}

void main()
{
	vec3 p    = normalize(f_position);
	float c = (noise(vec4(p, mod(time / 100000.0, 10000.0) / 200.0), 4,
	                 40.0, 0.7)
	           + 1.0)
	          * 0.5;

	// star radius
	float unRadius = 700000.0 * seed;
	// Get worldspace position
	vec4 sPosition = vec4(p, 1.0) * unRadius;

	// Sunspots
	float s         = 0.3;
	float frequency = 0.00001;
	float t1        = snoise(sPosition * frequency) - s;
	float t2        = snoise((sPosition + unRadius) * frequency) - s;
	float ss        = (max(t1, 0.0) * max(t2, 0.0)) * 2.0;

	// Accumulate total noise
	c = c - ss;

	float t = temperature;
	t += (c - 1.0) * (temperature * 1.0 / 3.0);
	t = max(blackbodyBoundaries.x, t);

	// Limb Darkening : https://en.wikipedia.org/wiki/Limb_darkening
	float sinomegasq
	    = starRadius * starRadius
	      / (campos.x * campos.x + campos.y * campos.y + campos.z * campos.z);
	float costheta
	    = dot(normalize(campos), normalize(campos - starRadius * p));
	float sinthetasq = 1.0 - costheta * costheta;
	float cospsy     = sqrt(max(0.0, 1.0 - sinthetasq / sinomegasq));

	float lum = luminance(t);
	lum *= limb_darkening(cospsy);

	float mul = /*exposure */ lum;

	if(mul * exposure < dynamicrange)
	{
		float b = 0.7;
		t       = tempfromluminance(lum);
	}
	else
	{
		t = temperature;
	}

	vec4 color = texture(blackbody,
	                     (t - blackbodyBoundaries.x)
	                         / (blackbodyBoundaries.y - blackbodyBoundaries.x));
	vec3 col   = mul * color.rgb;
	outColor   = vec4(col, 1.0);
}
