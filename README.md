# libplanet

An HydrogenVR library to simulate and render planetary and star systems.

## Settings to put in your project

### simulation/planetsystemdir

User-defined directory containing the planetary/star system.

### simulation/starttime

QDateTime at which to start the simulation.

### graphic/texmaxsize

Integer from 1 to 8 defaulting to 4. Defines texture sizes (not in pixels, internal unit).

### graphics/gentexload

Integer from 1 to 4 defaulting to 3. Defines patches size for generated texture updating (not in pixels, internal unit). The bigger the patches, the more work per frame the GPU has to do but the faster the full texture is loaded.

### graphics/atmoquality

Integer from 1 to 8 defaulting to 1. Defines a number proportional to the number of samples per pixel taken through atmosphere to determine its color.
