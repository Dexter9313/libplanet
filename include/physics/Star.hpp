/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef STAR_HPP
#define STAR_HPP

#include <QImage>
#include <QJsonObject>
#include <physics/Color.hpp>
#include <string>

#include "CelestialBody.hpp"

class Star : public CelestialBody
{
  public:
	struct Parameters
	{
		double temperature = 0.0;
		double absmag      = 0.0;
	};

	// constructors recompute CelestialBody::Parameters' color depending on
	// temperature
	Star(QJsonObject const& json, OrbitalSystem const& system);
	Star(QJsonObject const& json, Orbitable& parent);
	Star(std::string const& name, CelestialBody::Parameters const& parameters,
	     Parameters const& starParameters, Orbitable& parent,
	     Orbit* orbit = nullptr);
	Star(std::string const& name, CelestialBody::Parameters const& parameters,
	     Parameters const& starParameters, OrbitalSystem const& system,
	     Orbit* orbit = nullptr);

	Parameters getStarParameters() const { return params; };
	virtual double getIllumination(Vector3 const& fromRelPos,
	                               UniversalTime const& ut) const override;
	virtual double getApparentMagnitude(Vector3 const& fromRelPos,
	                                    UniversalTime const& ut) const override;
	virtual QJsonObject getJSONRepresentation() const override;

  private:
	Parameters params = {};

	static double computeTemperatureFromMass(double mass);
	static double computeMassFromTemperature(double temperature);
	static double computeAbsMagFromAppMag(double apparentMag, double dist);
	double computeAbsMagFromAppMag(double apparentMag) const;
	static double computeAbsMagFromTempAndRadius(double temperature,
	                                             double radius);
	// sRGB
	Color computeColor() const;
};

#endif // STAR_HPP
