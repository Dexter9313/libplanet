/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COLOR_HPP
#define COLOR_HPP

#include <QJsonObject>
#include <array>
#include <cmath>

/*! \ingroup phys
 * An
 * [ARGB-represented](https://en.wikipedia.org/wiki/RGBA_color_space#ARGB_(word-order))
 * color.
 *
 * See \ref phys group description for conventions and notations.
 */
class Color
{
  public:
	/*! Default constructor
	 *
	 * Constructs opaque black.
	 */
	Color();
	/*! RGB constructor
	 *
	 * Constructs an opaque color from three RGB values.
	 *
	 * \param r Red channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 * \param g Green channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 * \param b Blue channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 */
	Color(unsigned int r, unsigned int g, unsigned int b);

	/*! RGB constructor
	 *
	 * Constructs a color from four ARGB values.
	 *
	 * \param alpha Alpha channel with value from 0 (no contribution) to 255
	 * (max contribution).
	 *
	 * \param r Red channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 * \param g Green channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 * \param b Blue channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 */
	Color(unsigned int alpha, unsigned int r, unsigned int g, unsigned int b);

	Color(QJsonObject const& json,
	      Color const& defaultValue = Color(255, 0, 0, 0));

	QJsonObject getJSONRepresentation() const;

	/*!
	 * Alpha channel with value from 0 (no contribution) to 255
	 * (max contribution).
	 */
	unsigned int alpha;
	/*! Red channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 */
	unsigned int r;
	/*! Green channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 */
	unsigned int g;
	/*! Blue channel with value from 0 (no contribution) to 255 (max
	 * contribution).
	 */
	unsigned int b;

	static std::array<double, 3> rgbtoxyY(double r, double g, double b)
	{
		// Convert from RGB to XYZ
		double X = r * 0.4124 + g * 0.3576 + b * 0.1805;
		double Y = r * 0.2126 + g * 0.7152 + b * 0.0722;
		double Z = r * 0.0193 + g * 0.1192 + b * 0.9505;

		// Convert from XYZ to xyY
		double L = (X + Y + Z);
		double x = X / L;
		double y = Y / L;
		return {{x, y, Y}};
	};

	static std::array<double, 3> xyYtorgb(double x, double y, double Y)
	{
		// Convert from xyY to XYZ
		double X = x * (Y / y);
		double Z = (1.0 - x - y) * (Y / y);

		// Convert from XYZ to RGB
		double R = X * 3.2406 + Y * -1.5372 + Z * -0.4986;
		double G = X * -0.9689 + Y * 1.8758 + Z * 0.0415;
		double B = X * 0.0557 + Y * -0.2040 + Z * 1.0570;

		if(R < 0.0)
		{
			R = 0.0;
		}
		else if(R > 1.0)
		{
			R /= R;
			G /= R;
			B /= R;
		}
		if(G < 0.0)
		{
			G = 0.0;
		}
		else if(G > 1.0)
		{
			R /= G;
			G /= G;
			B /= G;
		}
		if(B < 0.0)
		{
			B = 0.0;
		}
		else if(B > 1.0)
		{
			R /= B;
			G /= B;
			B /= B;
		}

		return {{R, G, B}};
	};

	// illuminance in lux
	static double illuminanceFromMag(double mag)
	{
		return pow(10.0, -0.4 * (mag + 14.0));
	};
	static double magFromIlluminance(double illum)
	{
		return -log10(illum) / 0.4 - 14.0;
	};
};

#endif // COLOR_HPP
