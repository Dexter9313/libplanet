/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@epfl.ch>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef BLACKBODY_H
#define BLACKBODY_H

/*# Blackbody color datafile D58 (bbr_color_D58.html)
# Mitchell Charity
# http://www.vendian.org/mncharity/dir3/blackbody/UnstableURLs/bbr_color.html
# Version 2016-Mar-30
# History:
#   2016-Mar-30  New file using sRGB, but with a nonstandard D58 whitepoint.
#                This data was just a quick kludge, with no quality control.
#   2001-Jun-22  Switched to sRGB from Rec.709.
#   2001-Jun-04  Initial release.
#*/

#include "physics/Color.hpp"
#include <cmath>

namespace blackbody
{

unsigned int min_temp();
unsigned int max_temp();
unsigned int temp_step();
unsigned int arrays_size();
std::array<const unsigned char, (40000 - 1000) / 100 + 1> const& red();
std::array<const unsigned char, (40000 - 1000) / 100 + 1> const& green();
std::array<const unsigned char, (40000 - 1000) / 100 + 1> const& blue();

Color colorFromTemperature(double temp);

// lm/W
double efficacyFromTemperature(double temp);
// W/sr/m^2
double radianceFromTemperature(double temp);
// cd/m^2
double luminanceFromTemperature(double temp);

} // namespace blackbody

#endif // BLACKBODY_H
