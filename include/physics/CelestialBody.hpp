/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CELESTIALBODY_HPP
#define CELESTIALBODY_HPP

#include <QJsonArray>
#include <QJsonObject>
#include <cmath>
#include <vector>

#include "Color.hpp"
#include "Orbit.hpp"
#include "Orbitable.hpp"
#include "math/Vector4.hpp"
#include "math/constants.hpp"

class CelestialBody : public Orbitable
{
  public:
	enum class Type
	{
		PLANET,
		STAR,
		SPACECRAFT,
	};

	struct Parameters
	{
		double mass;   // kg
		double radius; // m
		Vector3 oblateness = Vector3(1.0, 1.0, 1.0);
		Color color;

		/* ROTATION */
		double siderealTimeAtEpoch = 0.0; // angle between FP of Aries and Prime
		                                  // Meridian in radians
		double siderealRotationPeriod = DBL_MAX; // in seconds

		double northPoleRightAsc    = 0.0;                // in rad
		double northPoleDeclination = constant::pi / 2.0; // in rad
	};

	CelestialBody(Type type, QJsonObject const& json,
	              OrbitalSystem const& system);
	CelestialBody(Type type, QJsonObject const& json, Orbitable& parent);
	CelestialBody(Type type, std::string const& name, Parameters parameters,
	              Orbitable& parent, Orbit* orbit = nullptr);
	CelestialBody(Type type, std::string const& name, Parameters parameters,
	              OrbitalSystem const& system, Orbit* orbit = nullptr);

	Type getCelestialBodyType() const { return type; };
	Parameters const& getCelestialBodyParameters() const { return parameters; };
	double getPrimeMeridianSiderealTimeAtUT(UniversalTime uT) const;
	virtual Matrix4x4 getProperRotationAtUT(UniversalTime const& uT) const;
	double getEscapeVelocity(double altitude = 0.0) const;
	double getSphereOfInfluenceRadius() const;
	// Roche limit for a fluid of 500kg/m^3
	// see https://en.wikipedia.org/wiki/Roche_limit
	// rings can be more or less within this limit (above it would form moons)
	double getMaximumRocheLimit() const;
	Vector3 getGravitationalField(Vector3 const& atRelPos) const;
	virtual double getIllumination(Vector3 const& fromRelPos,
	                               UniversalTime const& ut) const
	    = 0;
	virtual double getApparentMagnitude(Vector3 const& fromRelPos,
	                                    UniversalTime const& ut) const;
	// when phase angle of a planet is not 0 illumination doesn't seem to come
	// out of the center but closer and closer from the edge of the disk
	virtual Vector3
	    getLightCorrectedRelativePosition(Vector3 const& fromRelPos,
	                                      UniversalTime const& /*ut*/) const
	{
		// by default, light comes from the center of the celestial body
		// relative to the illuminated object
		return -1.0 * fromRelPos;
	};
	// returns any neighbor that can have a potentially relevant gravitational,
	// occluding or light casting effect
	// ie: stars, parent, planetary children and planetary siblings
	std::vector<CelestialBody const*> getPotentiallyRelevantNeighbors() const;
	// Returns sorted list by decreasing apparent diameter of possible
	// neighboring light occluders. For performance purposes, its relative
	// position at ut is also returned as second element of pair.
	std::vector<std::pair<CelestialBody const*, Vector3>>
	    getSortedRelevantNeighboringOccluders(UniversalTime const& uT) const;
	// Returns sorted list by increasing apparent magnitude of possible
	// neighboring light emitters. For performance purposes, the relative
	// position at ut of the light source (not always neighbor position !)
	// combined with apparent magnitude is also returned as second element of
	// pair
	std::vector<std::pair<CelestialBody const*, Vector4>>
	    getSortedRelevantNeighboringEmitters(UniversalTime const& uT) const;
	virtual ~CelestialBody() {};

	virtual QJsonObject getJSONRepresentation() const override;

  protected:
	Parameters parameters;

  private:
	Type type;

	void computeBaseRotation();
	Matrix4x4 baseRotation; // only align axis, no sideral time

	void parseJSON(QJsonObject const& json);
	static Orbitable::Type orbitableType(Type celestialBodyType);
};

#endif // CELESTIALBODY_HPP
