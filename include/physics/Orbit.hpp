/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ORBIT_HPP
#define ORBIT_HPP

#include <QJsonObject>
#include <cmath>
#include <iostream>

#include "math/CoordinateSystem.hpp"

#include "UniversalTime.hpp"

// mostly based on http://www.bogan.ca/orbits/kepler/orbteqtn.html

class Orbit
{
  public:
	struct Parameters
	{
		double inclination            = 0.0;
		double ascendingNodeLongitude = 0.0;
		double periapsisArgument      = 0.0;
		double eccentricity           = 0.0;
		double semiMajorAxis
		    = 0.0; // or altitude of periapsis if parabolic orbit
		double meanAnomalyAtEpoch = 0.0;
	};

	struct MassiveBodyMass
	{
		explicit MassiveBodyMass(double value)
		    : value(value) {};
		double value;
	};

	struct Period
	{
		explicit Period(double value)
		    : value(value) {};
		double value;
	};

  public:
	Orbit() = delete;
	Orbit(MassiveBodyMass const& massiveBodyMass, QJsonObject const& json);
	Orbit(MassiveBodyMass const& massiveBodyMass, Parameters parameters);
	Orbit(Period const& period, Parameters parameters);
	// orbit from pos+vel at universal time
	Orbit(MassiveBodyMass const& massiveBodyMass, Vector3 position,
	      Vector3 velocity, UniversalTime const& uT);
	Orbit(Orbit const& copiedOrbit);
	bool isValid() const { return valid; };
	virtual bool isInRange(UniversalTime const& /*uT*/) const { return true; };
	virtual std::pair<UniversalTime, UniversalTime> getRange() const
	{
		return {DBL_MIN, DBL_MAX};
	};
	virtual bool isLoadedFromFile() const { return false; };
	virtual void updateParameters(UniversalTime const& uT) { (void) uT; };
	void updateParameters(Vector3 position, Vector3 velocity,
	                      UniversalTime const& uT);
	double getMassiveBodyMass() const;
	Parameters getParameters() const;
	double getPeriod() const;
	// defined as unit vector pointing along the angular momentum of the orbit
	Vector3 getNorth() const;
	void convertMeanAnAtEpochToMeanAn(UniversalTime const& currentUt);
	void convertMeanAnToMeanAnAtEpoch(UniversalTime const& currentUt);
	virtual double getMeanAnomalyAtUT(UniversalTime const& uT);
	double getEccentricAnomalyAtUT(UniversalTime const& uT);
	double getTrueAnomalyAtUT(UniversalTime const& uT);
	double getMassiveBodyDistanceFromTrueAn(double trueAnomaly) const;
	double getMassiveBodyDistanceAtUT(UniversalTime const& uT);
	Vector3 getPositionFromTrueAn(double trueAnomaly) const;
	Vector3 getPositionAtUT(UniversalTime const& uT);
	Vector3 getVelocityFromTrueAn(double trueAnomaly) const;
	// if you know radialOut already ( := position.getUnitForm() ), prevents
	// computing it
	Vector3 getVelocityFromTrueAn(double trueAnomaly,
	                              Vector3 const& radialOut) const;
	Vector3 getVelocityAtUT(UniversalTime const& uT);
	Vector3 getAccelerationFromPosition(Vector3 const& position) const;
	Vector3 getAccelerationAtUT(UniversalTime const& uT);
	CoordinateSystem getRelativeCoordinateSystemAtUT(UniversalTime const& uT);
	std::ostream& displayAsText(std::ostream& stream) const;
	QJsonObject getJSONRepresentation() const;
	std::string getCurrentPatch() const { return currentPatch; };
	void setMassiveBodyMass(MassiveBodyMass const& massiveBodyMass);
	virtual ~Orbit() {};

	static double SMAfromPeriodMass(double period, double massiveBodyMass);
	static double massiveBodyMassFromElements(double semiMajorAxis,
	                                          double period);

  protected:
	void updatePeriod();

	Parameters parameters;
	std::string currentPatch = "";

  private:
	bool valid = false;

	double massiveBodyMass = 0.0;

	// period is precomputed as it is often used and doesn't change
	double period = 0.0;

	// cache
	UniversalTime cacheUT = DBL_MIN;
	double trueAnomaly    = 0.0;
	Vector3 position;
	void updateCacheAtUT(UniversalTime const& uT);
};

std::ostream& operator<<(std::ostream& stream, Orbit const& orbit);

#endif // ORBIT_HPP
