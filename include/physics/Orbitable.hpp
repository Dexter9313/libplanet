/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ORBITABLE_HPP
#define ORBITABLE_HPP

#include <QCryptographicHash>
#include <QJsonArray>
#include <QJsonObject>
#include <QProgressDialog>

#include "physics/Orbit.hpp"

class OrbitalSystem;

/*! \ingroup phys
 * Represents any virtual or real object that has an orbit and/or can be
 * orbited, such as barycenters (or binary barycenters), stars or planets for
 * example.
 *
 * If parent == nullptr, i.e. if
 */
class Orbitable
{
  public:
	enum class Type
	{
		BINARY,
		STAR,
		PLANET,
		SPACECRAFT,
	};

	Orbitable(Type type, QJsonObject const& json, OrbitalSystem const& system);
	Orbitable(Type type, QJsonObject const& json, Orbitable& parent);
	// /!\ will take ownership of orbit ; make sure it doesn't go out of scope
	// later !
	Orbitable(Type type, std::string name, OrbitalSystem const& system,
	          Orbit* orbit = nullptr);
	// /!\ will take ownership of orbit ; make sure it doesn't go out of scope
	// later !
	Orbitable(Type type, std::string name, Orbitable& parent,
	          Orbit* orbit = nullptr);
	virtual bool isValid() const
	{
		if(orbit != nullptr && !orbit->isValid())
		{
			return false;
		}
		return true;
	}

	void generateBinariesNames();
	void generatePlanetsNames();

	// we have to separate children parsing because parsing directly in
	// constructor means children can't dynamic_cast us if they need to
	void parseChildren();
	// we have to separate orbit parsing because we have to get all the
	// masses from the system first
	void parseOrbit();

	OrbitalSystem const& getSystem() const { return system; };
	Type getOrbitableType() const { return type; };
	std::string const& getName() const { return name; };
	std::string const& getDisplayName() const { return displayName; };
	void setName(std::string const& name)
	{
		if(this->name.empty())
		{
			this->name = name;
		}
	};
	float getPseudoRandomSeed() const;
	Orbitable const* getParent() const { return parent; };
	// has to be computed if binary
	double getMass() const;
	Orbit const* getOrbit() const { return orbit.get(); };
	Orbit* getOrbit() { return orbit.get(); };

	std::vector<std::unique_ptr<Orbitable>> const& getChildren() const
	{
		return children;
	};
	std::vector<Orbitable*> getAllDescendants() const;
	// filter by some type
	std::vector<Orbitable*> getAllDescendants(Type type) const;

	void addChild(std::unique_ptr<Orbitable>&& child);
	std::unique_ptr<Orbitable> removeChild(std::string const& name);

	Vector3 getRelativePositionAtUT(UniversalTime const& uT) const;
	Vector3 getAbsolutePositionAtUT(UniversalTime const& uT) const;
	Vector3 getRelativeVelocityAtUT(UniversalTime const& uT) const;
	Vector3 getAbsoluteVelocityAtUT(UniversalTime const& uT) const;
	// CoordinateSystem getAttachedCoordinateSystemAtUT(UniversalTime const& uT)
	// const;

	virtual QJsonObject getJSONRepresentation() const;

	// TODO(florian) : try to merge common code in the two following methods

	static Orbitable const& getCommonAncestor(Orbitable const& orb0,
	                                          Orbitable const& orb1);

	// Will try to get more significant digits than the awful
	// to.absolute - from.absolute
	static Vector3 getRelativePositionAtUt(Orbitable const& from,
	                                       Orbitable const& to,
	                                       UniversalTime const& uT);

	static Vector3 getRelativeVelocityAtUt(Orbitable const& of,
	                                       Orbitable const& relativeTo,
	                                       UniversalTime const& uT);

	void update(UniversalTime const& uT);
	virtual ~Orbitable() = default;

  protected:
	void changeParent(Orbitable* newParent);

  private:
	OrbitalSystem const& system;
	Type type;
	std::string name;
	std::string displayName;
	Orbitable* parent = nullptr;
	std::unique_ptr<Orbit> orbit;
	std::vector<std::unique_ptr<Orbitable>> children;

	/** LOADING BAR **/

	static std::unique_ptr<QProgressDialog>& progress();
	static float& value();
	static float& current();

	std::unique_ptr<QJsonObject> jsonBack;
};

#endif // ORBITABLE_HPP
