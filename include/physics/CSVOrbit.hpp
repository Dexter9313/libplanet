/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CSVORBIT_HPP
#define CSVORBIT_HPP

#include <QDir>
#include <QSettings>
#include <map>
#include <string>
#include <tuple>

#include "Orbit.hpp"

class CSVOrbit : public Orbit
{
  public:
	CSVOrbit(MassiveBodyMass const& massiveBodyMass,
	         std::string const& bodyName, bool infiniteLifeSpan = true);
	virtual bool isInRange(UniversalTime const& uT) const override;
	virtual std::pair<UniversalTime, UniversalTime> getRange() const override
	{
		return {minLoadable, infiniteLifeSpan
		                         ? std::numeric_limits<double>::infinity()
		                         : maxLoadable};
	};
	virtual bool isLoadedFromFile() const override { return true; };
	virtual void updateParameters(UniversalTime const& uT) override;
	virtual double getMeanAnomalyAtUT(UniversalTime const& uT) override
	{
		updateParameters(uT);
		return parameters.meanAnomalyAtEpoch;
	};
	static bool csvExistsFor(std::string const& name);

	static QString& currentSystemDir();

  private:
	void loadFile(QString const& fileName);
	void loadFromUT(UniversalTime const& uT);

	static double interpolateAngle(double before, double after, double frac);
	static double interpolateAngleAlwaysForward(double before, double after,
	                                            double frac);

	void updateParametersNonVirt(UniversalTime const& uT);

	QString dirPath;
	// maps utMin to a tuple of (utMax, csvfilePath, isLoaded)
	std::map<UniversalTime, std::tuple<UniversalTime, QString, bool>> csvfiles;

	std::map<UniversalTime, Orbit::Parameters> parametersHistory;
	std::map<UniversalTime, std::string> patches;
	// parameter is already in history, use overrides as lower bounds for
	// interpolation only
	std::map<UniversalTime, Orbit::Parameters> overrides;
	std::string bodyName;

	// min/max ut of last loaded file, small optimization
	UniversalTime minLoaded = DBL_MAX;
	UniversalTime maxLoaded = DBL_MIN;

	UniversalTime minLoadable = DBL_MAX;
	UniversalTime maxLoadable = DBL_MIN;

	UniversalTime cacheUT = DBL_MIN;

	bool infiniteLifeSpan = true;
};

#endif // CSVORBIT_HPP
