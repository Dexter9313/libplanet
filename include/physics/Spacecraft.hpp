/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPACECRAFT_HPP
#define SPACECRAFT_HPP

#include <QImage>
#include <QJsonObject>

#include "CelestialBody.hpp"
#include "Star.hpp"

class Spacecraft : public CelestialBody
{
  public:
	struct Parameters
	{
		Vector3 antennaDir = Vector3(0.0, 1.0, 0.0);
		Vector3 up         = Vector3(0.0, 0.0, 1.0);

		QString antennaTarget = "";

		QString modelPath = "";
		float modelScale  = 1.f;
	};

	Spacecraft(QJsonObject const& json, OrbitalSystem const& system);
	Spacecraft(QJsonObject const& json, Orbitable& parent);
	Spacecraft(std::string const& name,
	           CelestialBody::Parameters const& cbParams, Parameters scParams,
	           Orbitable& parent, Orbit* orbit = nullptr);
	Spacecraft(std::string const& name,
	           CelestialBody::Parameters const& cbParams, Parameters scParams,
	           OrbitalSystem const& system, Orbit* orbit = nullptr);
	Star const* getHostStar() const;
	Parameters const& getSpacecraftParameters() const { return parameters; };
	virtual Matrix4x4
	    getProperRotationAtUT(UniversalTime const& uT) const override;
	virtual double getIllumination(Vector3 const& fromRelPos,
	                               UniversalTime const& ut) const override;
	virtual QJsonObject getJSONRepresentation() const override;

  private:
	Parameters parameters = {};
};

#endif // SPACECRAFT_HPP
