/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ORBITALSYSTEMCAMERA_H
#define ORBITALSYSTEMCAMERA_H

#include <cmath>

#include "AbstractState.hpp"
#include "camera/BasicCamera.hpp"
#include "physics/OrbitalSystem.hpp"

class OrbitalSystemCamera : public BasicCamera
{
	Q_OBJECT
  public:
	class State : public AbstractState
	{
	  public:
		State()                   = default;
		State(State const& other) = default;
		State(State&& other)      = default;
		virtual void readFromDataStream(QDataStream& stream) override
		{
			stream >> relativePosition[0];
			stream >> relativePosition[1];
			stream >> relativePosition[2];
			stream >> pitch;
			stream >> yaw;
			stream >> targetName;
		};
		virtual void writeInDataStream(QDataStream& stream) override
		{
			stream << relativePosition[0];
			stream << relativePosition[1];
			stream << relativePosition[2];
			stream << pitch;
			stream << yaw;
			stream << targetName;
		};

		Vector3 relativePosition;
		float pitch = 0.f;
		float yaw   = 0.f;
		QString targetName;
	};

	OrbitalSystemCamera(VRHandler const& vrHandler, float const& exposure,
	                    float const& dynamicrange);
	bool isLockedOnRotation() const { return lockedOnRotation; };
	void setLockedOnRotation(bool value, UniversalTime const& uT);
	void toggleLockedOnRotation(UniversalTime const& uT);
	double getActualTargetRadiusBelow() const;
	double getAltitude() const;
	void setAltitude(double altitude);
	Vector3 getHeadShift() const;
	// approximation in steradians
	void updateUT(UniversalTime const& uT);
	static bool shouldBeCulled(QVector3D const& spherePosition, float radius);
	Vector3 getRelativePositionTo(Orbitable const& orbitable,
	                              UniversalTime const& uT) const;

	Vector3 relativePosition = Vector3(10000000.0, 0.0, 0.0);

	float pitch = 0.f;
	float yaw   = 0.f;

	Orbitable const* target = nullptr;

	float const& exposure;
	float const& dynamicrange;

	void readState(AbstractState const& s)
	{
		auto const& state = dynamic_cast<State const&>(s);
		relativePosition  = state.relativePosition;
		pitch             = state.pitch;
		yaw               = state.yaw;
		if(state.targetName != "")
		{
			target = target->getSystem()[state.targetName.toStdString()];
		}
	};
	void writeState(AbstractState& s) const
	{
		auto& state            = dynamic_cast<State&>(s);
		state.relativePosition = relativePosition;
		state.pitch            = pitch;
		state.yaw              = yaw;
		state.targetName       = target->getName().c_str();
	};

  public slots:
	QVector3D getLookDirection() const { return lookDirection; };
	QVector3D getUp() const { return up; };

  private:
	QVector3D lookDirection = {-1.f, 0.f, 0.f};
	QVector3D up            = {0.f, 0.f, 1.f};

	void updateView();

	bool lockedOnRotation = false;

	UniversalTime currentUT;
};

#endif // ORBITALSYSTEMCAMERA_H
