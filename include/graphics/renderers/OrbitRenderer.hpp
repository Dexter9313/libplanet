/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ORBITRENDERER_H
#define ORBITRENDERER_H

#include "graphics/OrbitalSystemCamera.hpp"
#include "physics/Orbitable.hpp"

class OrbitRenderer
{
  public:
	OrbitRenderer(Orbitable const& drawnOrbitable,
	              unsigned int nDivisions = 1000);
	void updateMesh(UniversalTime const& uT, OrbitalSystemCamera const& camera);
	void renderTransparent(BasicCamera const& camera, float alpha = 1.f);

	static double overridenScale;
	static bool forceRedraw;

  private:
	Orbitable const& drawnOrbitable;
	QColor color    = QColor(255, 0, 0);
	bool hyperbolic = false;

	static GLShaderProgram& orbitShader();
	std::unique_ptr<GLMesh> orbitMesh;

	std::vector<float> vertices;

	const unsigned int nDivisions;
	UniversalTime lastUpdateUT;
	unsigned int nextUpdateIndex = 0;

	QVector3D currentCamRelPos;

	// to check if needs redraws
	std::string currentPatch;
	Orbit::Parameters currentParameters;
	unsigned int timeSinceLastRedraw = 0; // in frames
	unsigned int id;

	bool needsRedraw(Orbit::Parameters newParams);

	// used for orbits to not rederender all on the same frame
	static unsigned int nextID;
};

#endif // ORBITRENDERER_H
