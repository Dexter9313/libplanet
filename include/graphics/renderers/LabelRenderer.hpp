/*
    Copyright (C) 2020 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LABELRENDERER_HPP
#define LABELRENDERER_HPP

#include "Text3D.hpp"

class LabelRenderer
{
  public:
	LabelRenderer(QString const& text, QColor const& color,
	              bool renderTick = true);
	void setAlpha(float alpha)
	{
		label.setAlpha(alpha);
		this->alpha = alpha;
	}
	void updateModel(QMatrix4x4 const& model);
	void render(float exposure, float dynamicrange);

  private:
	Text3D label;
	bool renderTick;
	unsigned int numberOfLines;

	QColor color;
	float alpha = 1.f;

	static GLShaderProgram& lineShader();
	GLMesh lineMesh;
	QMatrix4x4 lineModel;

	static bool& fontAdded();
	static QString& fontFamily();
};

#endif // LABELRENDERER_HPP
