/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GENERATEDTEXTURE_HPP
#define GENERATEDTEXTURE_HPP

#include "Primitives.hpp"
#include "gl/GLHandler.hpp"

class GeneratedTexture
{
  public:
	// is only the default color
	GeneratedTexture(QColor const& defaultColor, bool sRGB = true);
	GeneratedTexture(GLShaderProgram&& shader, unsigned int width,
	                 unsigned int height);
	// DON'T CALL WHILE RENDERING SCENE FOR NOW
	void render();
	GLTexture const& getTexture();

  private:
	bool isDefault;

	std::unique_ptr<GLShaderProgram> shader;
	GLMesh mesh;

	unsigned int progress = 0;
	const unsigned int patchsize
	    = 1 << (6 + QSettings().value("graphics/gentexload").toUInt());
	// = 2 ** (6 + gentexload)

	// width or height / currentLvl is current texture resolution
	// divided by two each time a level is fully rendered, until
	// we get to the true target resolution
	unsigned int currentLvl = 1;

	const unsigned int width;
	const unsigned int height;

	GLFramebufferObject framebuffer;
};

#endif
