/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLANETRENDERER_HPP
#define PLANETRENDERER_HPP

#include <QFileInfo>

#include "AtmosphereRenderer.hpp"
#include "CloudsRenderer.hpp"
#include "DetailedPlanetRenderer.hpp"
#include "RingsRenderer.hpp"
#include "graphics/OrbitalSystemCamera.hpp"
#include "physics/Planet.hpp"
#include "physics/Star.hpp"

#include "PlanetShaderHandler.hpp"
#include "graphics/renderers/CelestialBodyRenderer.hpp"
#include "physics/Planet.hpp"

#include "terrain/Terrain.hpp"

class PlanetRenderer : public CelestialBodyRenderer
{
  public:
	PlanetRenderer(Planet const& drawnBody,
	               std::map<QString, Node*>& nodesDict);
	PlanetShaderHandler::EvolvingParameters const& getEvolvingParams() const
	{
		return evolvingParams;
	};
	virtual void updateMesh(UniversalTime const& uT,
	                        OrbitalSystemCamera const& camera) override;

	static QString& currentSystemDir();

  protected:
	virtual void doRender(BasicCamera const& camera,
	                      std::vector<Light const*> const& lights,
	                      GLTexture const& brdfLUT, bool environment) override;
	virtual void doRenderTransparent(BasicCamera const& camera,
	                                 std::vector<Light const*> const& lights,
	                                 GLTexture const& brdfLUT,
	                                 bool environment) override;

  private:
	virtual void
	    setPointShaderParams(UniversalTime const& uT,
	                         OrbitalSystemCamera const& camera,
	                         GLShaderProgram const& pointShader) override;
	void loadPlanet();
	void unloadPlanet(bool waitIfPlanetNotLoaded = false);
	static QColor computePointColor(Planet const& drawnPlanet,
	                                Star const* star);
	void updateNeighborsData(UniversalTime const& uT);

	float boundingSphere;
	bool culled                      = false;
	bool apparentAngleAllowsUnloaded = false;
	bool apparentAngleAllowsDetailed = false;

	std::unique_ptr<DetailedPlanetRenderer> detailedPlanet;
	QString terrainPath;
	int terrainAlbedoCoeffLeftToApply = 0;
	std::unique_ptr<trn::Terrain> terrain;

	std::array<QVector4D, 2> lightsPosRadius;
	std::array<QVector3D, 2> lightsIllum; // lx
	std::array<QVector4D, 5>
	    neighborsPosRadius; // 3D position + radius of 5 closest neighbors
	std::array<QVector3D, 5>
	    neighborsOblateness; // oblateness of 5 closest neighbors

	PlanetShaderHandler::EvolvingParameters evolvingParams;

	std::unique_ptr<RingsRenderer> ringsRenderer;
	std::unique_ptr<AtmosphereRenderer> atmosphereRenderer;
	std::unique_ptr<CloudsRenderer> cloudsRenderer;

	// UNLOADED
	PlanetShaderHandler unloadedShaderHandler;
	GLMesh unloadedMesh;
};

#endif // PLANETRENDERER_HPP
