/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CLOUDSRENDERER_HPP
#define CLOUDSRENDERER_HPP

#include "AsyncTexture.hpp"
#include "Primitives.hpp"
#include "gl/GLHandler.hpp"

#include "AtmosphereRenderer.hpp"
#include "GeneratedTexture.hpp"
#include "PlanetShaderHandler.hpp"
#include "graphics/GraphicsUtils.hpp"
#include "physics/Planet.hpp"

class CloudsRenderer
{
  public:
	CloudsRenderer(Planet const& drawnBody, QString const& texturePath);
	void update();
	void
	    renderTransparent(PlanetShaderHandler::EvolvingParameters const& params,
	                      AtmosphereRenderer const& atmosphereRenderer,
	                      GLTexture const* night);

  private:
	Planet const& drawnBody;

	PlanetShaderHandler shaderHandler;
	GLMesh cloudSphere;

	bool procedural = false;
	std::unique_ptr<AsyncTexture> asyncTex;
	std::unique_ptr<GeneratedTexture> genTex;
};

#endif // CLOUDSRENDERER_HPP
