/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ATMOSPHERERENDERER_HPP
#define ATMOSPHERERENDERER_HPP

#include "Primitives.hpp"
#include "gl/GLHandler.hpp"

#include "PlanetShaderHandler.hpp"
#include "graphics/GraphicsUtils.hpp"
#include "physics/Planet.hpp"

class AtmosphereRenderer
{
  public:
	AtmosphereRenderer(Planet const& drawnBody);
	GLTexture const& getPreprocTexture() const { return preproc; };
	std::array<GLTexture const*, 3>
	    computeLUTs(PlanetShaderHandler::EvolvingParameters const& params,
	                float renderRadius, int intersectIndex) const;
	void renderTransparent(
	    PlanetShaderHandler::EvolvingParameters const& params);

	static float getRadius(float planetRadius,
	                       Planet::Atmosphere const& atmosphere);

  private:
	static GLTexture::Tex2DProperties getTextureConstructor();
	static void setPrecomputedTextureData(GLTexture& tex, float planetRadius,
	                                      Planet::Atmosphere const& atmosphere);
	const unsigned int size
	    = 64 * QSettings().value("graphics/atmoquality").toUInt();
	PlanetShaderHandler shaderHandler;
	GLMesh atmoSphere;

	GLTexture preproc;
	GLTexture emissionLUT;
	GLTexture absorptionLUT;

	GLComputeShader lutShader;
	float atmRadius;

	static QMap<QString, QString> constructDefines();
};

#endif // ATMOSPHERERENDERER_HPP
