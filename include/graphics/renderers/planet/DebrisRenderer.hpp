/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DEBRISRENDERER_HPP
#define DEBRISRENDERER_HPP

#include "PlanetShaderHandler.hpp"

class DebrisRenderer
{
  public:
	DebrisRenderer(CelestialBody const& drawnBody);
	void update(UniversalTime const& uT);
	void render(QMatrix4x4 const& model, float bodyRadius);

	static float opacity;
	static float asteroids;
	static unsigned int size;
	static bool filePathExistsForBody(QString const& bodyName);
	static QStringList getFilePathsForBody(QString const& bodyName);

  private:
	GLShaderProgram shader;
	GLMesh mesh = {};

	QString bodyName;
};

#endif // DEBRISRENDERER_HPP
