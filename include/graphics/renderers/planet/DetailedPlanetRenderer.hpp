/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DETAILEDPLANETRENDERER_H
#define DETAILEDPLANETRENDERER_H

#include <QImageReader>
#include <QtConcurrent>

#include "AsyncMesh.hpp"
#include "AsyncTexture.hpp"
#include "Light.hpp"

#include "AtmosphereRenderer.hpp"
#include "TerrainRenderer.hpp"

#include "GeneratedTexture.hpp"
#include "PlanetShaderHandler.hpp"
#include "physics/Planet.hpp"

class DetailedPlanetRenderer
{
  public:
	DetailedPlanetRenderer(Planet const& drawnPlanet,
	                       QString const& currentSystemDir);

	// returns zero if no custom model
	float getCustomModelBoundingSphere() const
	{
		return asyncMesh->getBoundingSphereRadius();
	}
	GLTexture const& getNightTexture() const;
	AsyncMesh const& getAsyncMesh() const { return *asyncMesh; };
	void update(QMatrix4x4 const& properRotation, QVector3D const& hoststarpos,
	            QVector3D const& neighborLightCasterPos);
	void render(PlanetShaderHandler::EvolvingParameters const& params,
	            GLTexture const* ringsTex                    = nullptr,
	            AtmosphereRenderer const* atmosphereRenderer = nullptr);

  private:
	void initTerrestrial(float seed,
	                     QColor const& color = QColor(255, 255, 255),
	                     float polarLatitude = 90.f);
	void initGasGiant(float seed, QColor const& color = QColor(255, 255, 255));
	void initFromTex(QString const& diffusePath, QString const& normalPath,
	                 QString const& specularPath, QString const& nightPath);

	Planet const& drawnBody;

	std::unique_ptr<PlanetShaderHandler> shaderHandler;

	bool procedural;
	// diffuse, normal, specular, night
	std::array<std::unique_ptr<AsyncTexture>, 4> asyncTexs;
	std::array<std::unique_ptr<GeneratedTexture>, 4> genTexs;

	std::unique_ptr<AsyncMesh> asyncMesh;

	TerrainRenderer terrain;

	std::array<std::unique_ptr<Light>, 2> light;
	GLTexture shadowmap;
};

#endif // DETAILEDPLANETRENDERER_H
