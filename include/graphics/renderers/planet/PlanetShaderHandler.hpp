/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLANETSHADERHANDLER_HPP
#define PLANETSHADERHANDLER_HPP

#include "gl/GLHandler.hpp"

#include "graphics/GraphicsUtils.hpp"
#include "physics/Planet.hpp"

class PlanetShaderHandler
{
  public:
	struct EvolvingParameters
	{
		QMatrix4x4 model;
		QMatrix4x4 properRotation;
		QVector3D campos;

		// light casters
		std::array<QVector4D, 2> lightsPosRadius;
		std::array<QVector3D, 2> lightsIllum;

		// light occluders
		std::array<QVector4D, 5> neighborsPosRadius;
		std::array<QVector3D, 5> neighborsOblateness;

		float exposure;
		float dynamicrange;
	};

	PlanetShaderHandler(QString const& vertPath, QString const& fragPath,
	                    Planet const& drawnBody, bool normal = false);
	PlanetShaderHandler(QString const& shaderPath, Planet const& drawnBody,
	                    bool normal = false);
	GLShaderProgram const& getShader() const { return shader; };
	void update(EvolvingParameters const& params);

	static void setConstantUniforms(GLShaderProgram const& shader,
	                                Planet const& drawnBody);
	static void update(GLShaderProgram const& shader,
	                   EvolvingParameters const& params,
	                   unsigned int maxlights);

  private:
	GLShaderProgram shader;
	static QMap<QString, QString> constructDefines(bool normal,
	                                               std::string const& bodyName);

	const unsigned int maxlights
	    = QSettings().value("graphics/maxlightcasters").toUInt();
};

#endif // PLANETSHADERHANDLER_HPP
