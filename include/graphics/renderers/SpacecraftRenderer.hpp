/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef SPACECRAFTRENDERER_HPP
#define SPACECRAFTRENDERER_HPP

#include "graphics/OrbitalSystemCamera.hpp"
#include "graphics/renderers/CelestialBodyRenderer.hpp"
#include "physics/Spacecraft.hpp"
#include "scene/Node.hpp"

class SpacecraftRenderer : public CelestialBodyRenderer
{
  public:
	SpacecraftRenderer(Spacecraft const& drawnSpacecraft,
	                   std::map<QString, Node*>& nodesDict);
	virtual void updateMesh(UniversalTime const& uT,
	                        OrbitalSystemCamera const& camera) override;

	static bool globalIllumination;

  protected:
	virtual void doRender(BasicCamera const& camera,
	                      std::vector<Light const*> const& lights,
	                      GLTexture const& brdfLUT, bool environment) override;
	virtual void doRenderTransparent(BasicCamera const& camera,
	                                 std::vector<Light const*> const& lights,
	                                 GLTexture const& brdfLUT,
	                                 bool environment) override;

  private:
	virtual void
	    setPointShaderParams(UniversalTime const& uT,
	                         OrbitalSystemCamera const& camera,
	                         GLShaderProgram const& pointShader) override;
	void updateNeighborsData(UniversalTime const& uT);

	void constructModel3D();
	void destructModel3D();

	std::array<QVector4D, 5>
	    neighborsPosRadius; // 3D position + radius of 5 closest neighbors
	std::array<QVector3D, 5>
	    neighborsOblateness; // oblateness of 5 closest neighbors

	std::array<std::unique_ptr<Light>, 2> lights;
	std::array<QVector4D, 2> lightsPosRadius;
	std::array<QVector3D, 2> lightsIllum;

	bool culled = false;

	const unsigned int maxlights
	    = QSettings().value("graphics/maxlightcasters").toUInt();
};

#endif // SPACECRAFTRENDERER_HPP
