/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CELESTIALBODYRENDERER_H
#define CELESTIALBODYRENDERER_H

#include <QMatrix4x4>

#include "graphics/OrbitalSystemCamera.hpp"
#include "graphics/renderers/LabelRenderer.hpp"
#include "graphics/renderers/OrbitRenderer.hpp"
#include "physics/CelestialBody.hpp"
#include "planet/DebrisRenderer.hpp"
#include "scene/Node.hpp"

class CelestialBodyRenderer : public Node
{
  public:
	CelestialBodyRenderer(CelestialBody const& drawnBody,
	                      QColor const& pointColor,
	                      std::map<QString, Node*>& nodesDict);
	virtual std::vector<std::pair<GLMesh const&, QMatrix4x4>>
	    getShadowCastingMeshes() const override
	{
		return {};
	};
	virtual void updateMesh(UniversalTime const& uT,
	                        OrbitalSystemCamera const& camera);
	virtual bool usesGlobalIllumination() const override { return false; };
	CelestialBody const& getDrawnBody() const { return drawnBody; };
	virtual ~CelestialBodyRenderer() = default;

	static double& overridenScale();
	static float& renderLabels();
	static float& renderOrbits();

	static QStringList& renderLabelsOrbitsOnly();

  protected:
	virtual void doRender(BasicCamera const& camera,
	                      std::vector<Light const*> const& lights,
	                      GLTexture const& brdfLUT, bool environment) override;
	virtual void doRenderTransparent(BasicCamera const& camera,
	                                 std::vector<Light const*> const& lights,
	                                 GLTexture const& brdfLUT,
	                                 bool environment) override;

  protected:
	virtual void setPointShaderParams(UniversalTime const& /*uT*/,
	                                  OrbitalSystemCamera const& /*camera*/,
	                                  GLShaderProgram const& /*pointShader*/) {
	};
	bool depthWillBeCleared() const { return clearDepthBuffer; };
	void handleDepth() const;

	CelestialBody const& drawnBody;

	QMatrix4x4 model;
	QVector3D position;
	Vector3 camRelPos;
	double scale              = 0.0;
	double apparentAngle      = 0.0;
	double orbitApparentAngle = 0.0;

	QMatrix4x4 properRotation; // full rotation, sideral time included

	std::unique_ptr<OrbitRenderer> orbitRenderer;

	bool transparencyRendered = false;

  private:
	// POINT
	static GLShaderProgram& pointShader();
	GLMesh pointMesh;

	bool clearDepthBuffer = false;

	UniversalTime lastUpdateUt;
	void renderPoint(OrbitalSystemCamera const& camera);

	LabelRenderer label;

	std::unique_ptr<DebrisRenderer> debrisRenderer;
};

#endif // CELESTIALBODYRENDERER_H
