/*
    Copyright (C) 2019 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LIBPLANET_HPP
#define LIBPLANET_HPP

#include "AbstractLibrary.hpp"
#include "PythonQtHandler.hpp"

#include "math/Vector3.hpp"

class LibPlanet : public AbstractLibrary
{
  public:
	LibPlanet() = default;
	virtual void setupSettings(SettingsWidget& settingsWidget) override;
	virtual void setupPythonAPI() override;
};

#endif // LIBPLANET_HPP
