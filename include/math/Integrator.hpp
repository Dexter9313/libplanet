/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INTEGRATOR_HPP
#define INTEGRATOR_HPP

#include "math/Vector3.hpp"

struct StateVectors
{
	Vector3 v;
	Vector3 x;
	StateVectors& operator+=(StateVectors const& sv);
};

StateVectors operator+(StateVectors const& sv0, StateVectors const& sv1);
StateVectors operator*(StateVectors const& sv, double scalar);
StateVectors operator*(double scalar, StateVectors const& sv);
StateVectors operator/(StateVectors const& sv, double scalar);

class Integrator
{
  private:
	Integrator() = default;

  public:
	typedef std::function<StateVectors(StateVectors const&)> Derivative;

	// f : v,x -> dv, dx
	// methods from : https://www.embeddedrelated.com/showarticle/474.php
	// v2, x2
	static StateVectors euler(double dt, StateVectors sv0,
	                          Derivative const& df);
	static StateVectors trapezoidal(double dt, StateVectors sv0,
	                                Derivative const& df);
	static StateVectors midpoint(double dt, StateVectors sv0,
	                             Derivative const& df);
	static StateVectors rungekutta4(double dt, StateVectors sv0,
	                                Derivative const& df);
	static StateVectors verlet(double dt, StateVectors sv0,
	                           Derivative const& df);
	static StateVectors forestruth(double dt, StateVectors sv0,
	                               Derivative const& df);
	static StateVectors princedormand8(double dt, StateVectors sv0,
	                                   Derivative const& df);
};

#endif // INTEGRATOR_HPP
